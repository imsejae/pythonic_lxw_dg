#-------------------------------------------------------------------------------
# SMOOTH SOLUTION EXAMPLE
#-------------------------------------------------------------------------------

#------------------- Initial Conditions: Primitive variables -------------------
def rho(x,AppParams):
    from numpy import sin,pi;
    return 1.0 + 0.5*sin(3.0*pi*x);
def u(x,AppParams):
    return 0.5+0.0*x;
def p(x,AppParams):
    return 0.75+0.0*x;
def q(x,AppParams):
    return 0.5+0.0*x;
def r(x,AppParams):
    return 3.0+0.0*x;
#-------------------------------------------------------------------------------

#------------------- Initial Conditions: Conserved variables -------------------
def q0(x,AppParams):
    return rho(x,AppParams);
def q1(x,AppParams):
    return rho(x,AppParams)*u(x,AppParams);
def q2(x,AppParams):
    return rho(x,AppParams)*u(x,AppParams)**2 \
           + p(x,AppParams);
def q3(x,AppParams):
    return rho(x,AppParams)*u(x,AppParams)**3 \
           + 3*p(x,AppParams)*u(x,AppParams) + q(x,AppParams);
def q4(x,AppParams):
    return rho(x,AppParams)*u(x,AppParams)**4 \
    + 6*p(x,AppParams)*u(x,AppParams)**2 \
    + 4*q(x,AppParams)*u(x,AppParams) + r(x,AppParams);
#-------------------------------------------------------------------------------

#---------------------------- Source Term Functions ----------------------------
def s0(t,x,AppParams):
    return 0.0
def s1(t,x,AppParams):
    return 0.0
def s2(t,x,AppParams):
    return 0.0
def s3(t,x,AppParams):
    return 0.0
def s4(t,x,AppParams):
    return 0.0
#-------------------------------------------------------------------------------

#---------------------------- Primitive Equations Source Term Functions --------
def s0_prim(t,x,AppParams):
    return 0.0
def s1_prim(t,x,AppParams):
    return 0.0
def s2_prim(t,x,AppParams):
    return 0.0
def s3_prim(t,x,AppParams):
    return 0.0
def s4_prim(t,x,AppParams):
    return 0.0
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
if __name__ == "__main__":

    # -----------------------------
    # GRAB SOME IMPORTANT FUNCTIONS
    # -----------------------------
    # parameter function (this sits in the current local directory)
    from parameters import parameters_init;
    # set path to main library for DG code
    import sys; sys.path.insert(0,'../../lib');
    # main Lax-Wendroff code (this sits in the main library directory)
    from LaxWendroffDG import LaxWendDG;

    # --------------
    # SET PARAMETERS
    # --------------
    from os import system;
    output_dir = "output";
    system("rm -f -r " + output_dir);
    system("mkdir " + output_dir);
    AppParams,DimParams,MeshParams,TimeParams,PredParams \
        = parameters_init(output_dir);

    # --------------------------------
    # QUICK SANITY CHECK OF PARAMETERS
    # --------------------------------
    assert(DimParams['numEqns']==5);
    assert(AppParams['gamma']>1.0);

    # ------------------------------
    # EXECUTE LAX-WENDROFF DG METHOD
    # ------------------------------
    Q = LaxWendDG([q0,q1,q2,q3,q4],                # initial condition function
                  [s0,s1,s2,s3,s4],                # source term function for conservative vars
                  [s0_prim,s1_prim,s2_prim,s3_prim,s4_prim], # source term function for primitive vars
                  AppParams,                 # application-specific parameters
                  DimParams,                 # parameters determining dimensions of arrays
                  MeshParams,                # mesh-specific parameters
                  TimeParams,                # time-stepping-specific parameters
                  PredParams,                # prediction-step-specific parameters
                  output_dir,                # directory to where output is written
                  verbosity=False);          # Turn-off printing runtime info to screen

    # Compute relative L2 error and print to file "error.dat"
    from L2error import L2error;
    L2error(MeshParams['xlow'],
            MeshParams['spacestep'],
            MeshParams['numGridCells'],
            DimParams['numOrder'],
            TimeParams['endTime'],
            Q,
            AppParams);

#-------------------------------------------------------------------------------
