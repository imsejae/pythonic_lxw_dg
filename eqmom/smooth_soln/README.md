# Compressible Euler equations: smooth solution example #

### Files:
* `errortable.py`    - (main) executes a full convergence study of method
* `run_example.py`   - (main) runs Lax-Wendroff DG method
* `plot_example.py`  - (main) plots solution from Lax-Wendroff DG method
* `parameters.py`    - (main) contains parameter values for simulation
* `errorhelper.py`   - (helper) routines for full convergence study of method
* `L2error.py`       - (helper) computes L2-error of solution

### Use the Makefile to run code:
* `make help`     : this will list all possible targets
* `make run`      : this will execute the DG code
* `make plot`     : this will plot the DG solution
* `make clean`    : this will remove all `*.pyc` and `output/*` files
* `make cleanrun` : this will clean and run
* `make all`      : this will clean, run, and plot
* `make error`    : this will carry out a convergence study

### Initial conditions:
* `rho(t=0,x) = 1.0 + 0.5*sin(pi*x)`
* `u(t=0,x) = 0.5`
* `p(t=0,x) = 0.75`

### Exact solutions:
* `rho(t,x) = 1.0 + 0.5*sin(pi*(x-0.5*t))`
* `u(t,x) = 0.5`
* `p(t,x) = 0.75`

### Source terms:
* `s0(t,x) = 0.0`
* `s1(t,x) = 0.0`
* `s2(t,x) = 0.0`
