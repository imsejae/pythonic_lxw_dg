#Lay down Gauss Hermite Points and Calculate the Weights

def m_star(moments,x,t):
    from numpy import zeros,cos,pi,sqrt

    #print("moments is", moments)
    new_mom = zeros(5);
    #for i in range(0,4):
    #    new_mom[i] = moments[i+1];
    rhoEx = -(sqrt(pi)*(-2.0 + cos(2.0*pi*t - 2.0*x)))/2.0
    new_mom[0] = moments[1];
    new_mom[1] = moments[2];
    new_mom[2] = moments[3];
    new_mom[3] = moments[4];
    new_mom[4] = 81.0*rhoEx/2048.0;
    #new_mom[4] = 0.0

    exact_mom = zeros(5);
    #exact_mom[0] = rhoEx;
    exact_mom[0] = rhoEx/4.0;
    exact_mom[1] = 3.0*rhoEx/16;
    exact_mom[2] = 7.0*rhoEx/64.0;
    exact_mom[3] = 25.0*rhoEx/256.0;
    exact_mom[4] = 81.0*rhoEx/2048.0;

    #print("new_mom is", new_mom)
    #print("exact moments is", exact_mom)

    return(new_mom)


###################################################
def m_starExact(moments,x,t):
    from numpy import zeros,cos,pi,sqrt

    new_mom = zeros(5);
    #for i in range(0,4):
    #    new_mom[i] = moments[i+1];
    rhoEx = -(sqrt(pi)*(-2.0 + cos(2.0*pi*t - 2.0*x)))/2.0
    new_mom[0] = rhoEx/4.0;
    new_mom[1] = 3.0*rhoEx/16;
    new_mom[2] = 7.0*rhoEx/64.0;
    new_mom[3] = 25.0*rhoEx/256.0;
    new_mom[4] = 81.0*rhoEx/2048.0;
    #new_mom[4] = 0.0

    return(new_mom)


#def m_star(moments,n):

    from numpy import zeros,array,exp,dot,diag,transpose,shape;
    from numpy.linalg import inv,norm;
    import math;

    #print("moments is", moments);

    m = len(moments);

    scaled_mom = zeros(len(moments));
    for k in range(0,len(moments)):
        tem = 0.0;
        for l in range(0,k+1):
            tem = tem + math.factorial(k)/(math.factorial(l)*math.factorial(k-l)) \
                *(-moments[1]/moments[0])**(k-l)*moments[l];
        scaled_mom[k] = (1/moments[0])*((moments[2]-moments[1]**2/moments[0])/moments[0])**(-k/2)*tem;

    #print("moments is", scaled_mom);

    roots = array([[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [1.0,-1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 1.73205080756887729, -1.73205080756887729, 0.0, 0.0, \
                      0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.741963784302725858, -0.741963784302725858, 2.33441421833897724, \
                      -2.33441421833897724, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 1.35562617997426587, -1.35562617997426587, 2.85697001387280565, \
                      -2.85697001387280565, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.616706590192594152, -0.616706590192594152, 1.88917587775371068, \
                      -1.88917587775371068, 3.32425743355211895, -3.32425743355211895, 0.0, \
                      0.0, 0.0, 0.0],
                    [0.0, 1.154405394739968127, -1.154405394739968127, 2.36675941073454129, \
                      -2.36675941073454129, 3.75043971772574226, -3.75043971772574226, 0.0, 0.0, 0.0],
                    [0.539079811351375108, -0.539079811351375108, 1.63651904243510800, \
                      -1.63651904243510800, 2.80248586128754170, -2.80248586128754170, \
                      4.14454718612589433, -4.14454718612589433, 0.0, 0.0],
                    [0.0, 1.023255663789132525, -1.023255663789132525, 2.07684797867783011, \
                      -2.07684797867783011, 3.20542900285646994, -3.20542900285646994, \
                      4.51274586339978267, -4.51274586339978267, 0.0],
                    [0.484935707515497653, -0.484935707515497653, 1.46598909439115818, \
                      -1.46598909439115818, 2.48432584163895458, -2.48432584163895458, \
                      3.58182348355192692, -3.58182348355192692, 4.85946282833231215, \
                      -4.85946282833231215]]);

    #print("roots", roots[n-1,:])

    A = zeros((m,n));
    for i in range(0,m):
        for j in range(0,n):
            #create matrix A of Hermite roots
            A[i,j] = roots[n-1,j]**(i+1);
    #print("A is", A)
    #print("lamb is", lamb)
    lamb = zeros(m); #Guess lambda vector


###Calculate omega values
    omega = zeros(n);
    a_lam = 0.0;
    for i in range(0,n):
        a_lam = dot(A[:,i],lamb);
        omega[i] = exp(-1.0-a_lam);
    #print("omega is", omega)

###Calculate gradient of g
    grad_g = scaled_mom - dot(A,omega);

    #print("grad_g is", grad_g);

    diag_omega = zeros((n,n));
    check = 0;
    count = 0;
    while check == 0:
        for i in range(0,n):
            diag_omega[i,i] = omega[i];

        tranA = transpose(A);
        tempH = dot(diag_omega,tranA);
        H = dot(A,tempH);
        #if count==0:
        #    print("H is", H)

        Hinv = inv(H);
        #if count==0:
        #    print("Hinv is", Hinv)

        del_lambda = -1.0*dot(Hinv,grad_g);
        #if count==0:
        #    print("del_lambda is", del_lambda)

        lamb = lamb+del_lambda;
        #if count==0:
        #    print("lambda is", lamb)

        a_lam2 = 0.0;
        for i in range(0,n):
            a_lam2 = dot(A[:,i],lamb);

            #if math.isnan(a_lam2):
            #print("count is", count)
            #    raise
            #print("a_lam2 is", a_lam2);
            omega[i] = exp(-1.0-a_lam2);

        if count==1:
            print("omega is", omega)

        grad_g = scaled_mom - dot(A,omega);
        if count==1:
            print("grad_g is", grad_g)

        #print("lambda is", lamb)

        if norm(del_lambda)<1.0e-12 and norm(grad_g)<1.0e-12:
            check = 1;


        count = count+1;
        if count == 500:
            check = 1;

    print("count=", count)
    if count == 500:
        raise

    approx_mom = zeros(m+1);
    for j in range(0,m+1):
        for i in range(0,n):
            approx_mom[j] = approx_mom[j] + omega[i]*roots[n-1,i]**j;
    #print("approx_mom is", approx_mom)


    new_mom = zeros(m+1);
    for k in range(0,m+1):
        tem = 0.0;
        for l in range(0,k+1):
            tem = tem + math.factorial(k)/(math.factorial(l)*math.factorial(k-l)) \
                *((moments[2]-moments[1]**2/moments[0])/moments[0])**(l/2)*(moments[1]/moments[0])**(k-l)*approx_mom[l];
        new_mom[k] = moments[0]*tem;

    #print("lambda is", lamb);
    return(new_mom[1:]);


    #######################################
