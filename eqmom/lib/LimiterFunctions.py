#------------------------------------------------------------------------------
def LimitAverageFlux(nu,
                     Q_bar,
                     Q_LxF,
                     dFlux_lft,
                     dFlux_rgt,
                     NumFlux_lft,
                     NumFlux_rgt,
                     AppParams,
                     epsilon):

    from numpy import min,abs,isnan;
    from FluxFunctions import primvars;

    #test for negatives
    Prim_LxF = primvars(Q_LxF[0],Q_LxF[1],Q_LxF[2],Q_LxF[3],Q_LxF[4]);
    if Prim_LxF[0] < epsilon or Prim_LxF[2] < epsilon or Prim_LxF[4] < epsilon \
        or isnan(Prim_LxF[0]) or isnan(Prim_LxF[2]) or isnan(Prim_LxF[4]):
        print("Error: Limit Average Flux")
        print("rho",Prim_LxF[0],"p",Prim_LxF[2],"rstar",Prim_LxF[4]);
        raise;

    ###########################################
    # PART I: density
    ###########################################
    Gamma = (Q_LxF[0]-epsilon)/nu;

    if dFlux_lft[0]<0.0 and dFlux_rgt[0]<0.0:

        Lambda_lft = min([1.0,Gamma/(abs(dFlux_lft[0])+abs(dFlux_rgt[0]))]);
        Lambda_rgt = 0.0+Lambda_lft;

    elif dFlux_lft[0]<0.0:

        Lambda_lft = min([1.0,Gamma/abs(dFlux_lft[0])]);
        Lambda_rgt = 1.0;

    elif dFlux_rgt[0]<0.0:

        Lambda_lft = 1.0;
        Lambda_rgt = min([1.0,Gamma/abs(dFlux_rgt[0])]);

    else:

        Lambda_lft = 1.0;
        Lambda_rgt = 1.0;

    ###########################################
    # PART II: pressure
    ###########################################

    p_LxF = Q_LxF[2]-(Q_LxF[1]**2)/Q_LxF[0];
    mu_11 = 1.0;
    mu_10 = 1.0;
    mu_01 = 1.0;

    # case 1:  alpha[0] = 1   and    alpha[1] = 1
    qc = Q_bar - nu*(Lambda_rgt*NumFlux_rgt - Lambda_lft*NumFlux_lft);
    pc = primvars(qc[0],qc[1],qc[2],qc[3],qc[4])[2];
    #pc = qc[2]-(qc[1]**2)/qc[0];
    if pc<=epsilon:
        mu_11 = (p_LxF-epsilon)/(p_LxF-pc);

    # case 2:  alpha[0] = 1   and    alpha[1] = 0
    qc = Q_bar + nu*(Lambda_lft*NumFlux_lft);
    pc = primvars(qc[0],qc[1],qc[2],qc[3],qc[4])[2];
    if pc<=epsilon:
        mu_10 = (p_LxF-epsilon)/(p_LxF-pc);

    # case 3:  alpha[0] = 0   and    alpha[1] = 1
    qc = Q_bar - nu*(Lambda_rgt*NumFlux_rgt);
    pc = primvars(qc[0],qc[1],qc[2],qc[3],qc[4])[2];
    if pc<=epsilon:
        mu_01 = (p_LxF-epsilon)/(p_LxF-pc);

    # take minimum mu
    mu = min([mu_11,mu_10,mu_01]);

    # rescale Lambdas
    Lambda_lft *= mu;
    Lambda_rgt *= mu;



    ###########################################
    # PART III: rstar
    ###########################################

    rstar_LxF = primvars(Q_LxF[0],Q_LxF[1],Q_LxF[2],Q_LxF[3],Q_LxF[4])[4];
    mu_11 = 1.0;
    mu_10 = 1.0;
    mu_01 = 1.0;

    # case 1:  alpha[0] = 1   and    alpha[1] = 1
    qc = Q_bar - nu*(Lambda_rgt*NumFlux_rgt - Lambda_lft*NumFlux_lft);
    rstarc = primvars(qc[0],qc[1],qc[2],qc[3],qc[4])[4];
    if rstarc<=epsilon:
        mu_11 = (rstar_LxF-epsilon)/(rstar_LxF-rstarc);

    # case 2:  alpha[0] = 1   and    alpha[1] = 0
    qc = Q_bar + nu*(Lambda_lft*NumFlux_lft);
    rstarc = primvars(qc[0],qc[1],qc[2],qc[3],qc[4])[4];
    if rstarc<=epsilon:
        mu_10 = (rstar_LxF-epsilon)/(rstar_LxF-rstarc);

    # case 3:  alpha[0] = 0   and    alpha[1] = 1
    qc = Q_bar - nu*(Lambda_rgt*NumFlux_rgt);
    rstarc = primvars(qc[0],qc[1],qc[2],qc[3],qc[4])[4];
    if rstarc<=epsilon:
        mu_01 = (rstar_LxF-epsilon)/(rstar_LxF-rstarc);

    # take minimum mu
    mu = min([mu_11,mu_10,mu_01]);

    # rescale Lambdas
    Lambda_lft *= mu;
    Lambda_rgt *= mu;

    return Lambda_lft,Lambda_rgt;
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def PredictionStep_Positivity_Limiter(Win,
                                      numGridCells,
                                      numOrder,
                                      numPredBasis,
                                      numEqns,
                                      epsilon,
                                      AppParams,
                                      QuadData):

    #num_psiAtPositivityPts = QuadData['num_psiAtPositivityPts'];
    #psiAtPositivityPts     = QuadData['psiAtPositivityPts'    ];
    num_psiAtPositivityPts = QuadData['num_psiAtPosPts'];
    psiAtPositivityPts     = QuadData['psiAtPosPts'    ];

    #print(num_psiAtPositivityPts)
    #print(psiAtPositivityPts.shape);
    #print(Win.shape);

    from numpy import min,copy,shape,isnan;

    Wout = copy(Win);

    for i in range (0,numGridCells):

        # --------------------------------------------------------------------
        # PART I: DENSITY
        rho_ave = Win[0,i];
        rho_min = 0.0+rho_ave;

        if rho_ave < epsilon or isnan(rho_ave):
            print ("ERROR: average density in PREDICTION is less than epsilon")
            print ("i = %i, rho_ave = %24.16e\n" % (i,rho_ave));
            raise

        for nq in range(0,num_psiAtPositivityPts):
            rho_val = 0.0+rho_ave;
            for ell in range (1,numPredBasis):
                rho_val += psiAtPositivityPts[ell,nq] * Win[ell,i];
            rho_min = min([rho_min,rho_val]);
        # --------------------------------------------------------------------

        # --------------------------------------------------------------------
        # PART II: PRESSURE
        p_ave = Win[2*numPredBasis,i];
        p_min = 0.0+p_ave;

        if p_ave < epsilon or isnan(p_ave):
            print ("ERROR: average pressure in PREDICTION is less than epsilon")
            print ("i = %i, p_ave = %24.16e\n" % (i,p_ave));
            raise

        for nq in range(0,num_psiAtPositivityPts):
            p_val = 0.0+p_ave;
            ell_mod = 1;
            for ell in range (2*numPredBasis+1,3*numPredBasis):
                p_val += psiAtPositivityPts[ell_mod,nq] * Win[ell,i];
                ell_mod += 1;
            p_min = min([p_min,p_val]);
        # --------------------------------------------------------------------

        # --------------------------------------------------------------------
        # PART III: RSTAR
        rstar_ave = Win[4*numPredBasis,i];
        rstar_min = 0.0+rstar_ave;

        if rstar_ave < epsilon or isnan(rstar_ave):
            print ("ERROR: average rstar in PREDICTION is less than epsilon")
            print ("i = %i, rstar_ave = %24.16e\n" % (i,rstar_ave));
            raise

        for nq in range(0,num_psiAtPositivityPts):
            rstar_val = 0.0+rstar_ave;
            ell_mod = 1;
            for ell in range (4*numPredBasis+1,5*numPredBasis):
                rstar_val += psiAtPositivityPts[ell_mod,nq] * Win[ell,i];
                ell_mod += 1;
            rstar_min = min([rstar_min,rstar_val]);
        # --------------------------------------------------------------------

        # --------------------------------------------------------------------
        # FINALLY, LIMIT ALL VARIABLES
        if rho_min>=epsilon and p_min>=epsilon and rstar_min>=epsilon:
            theta = 1.0;
            #print("theta is", theta);
        else:
            theta_rho = max([0.0,min([1.0, (rho_ave-epsilon)/(rho_ave-rho_min)])]);
            theta_p   = max([0.0,min([1.0, (p_ave-epsilon)/(p_ave-p_min)])]);
            theta_k   = max([0.0,min([1.0, (rstar_ave-epsilon)/(rstar_ave-rstar_min)])]);

            for ell in range (1,numPredBasis):
                Wout[ell,i] *= theta_rho;
            for ell in range (2*numPredBasis+1,3*numPredBasis):
                Wout[ell,i] *= theta_p;
            for ell in range (4*numPredBasis+1,5*numPredBasis):
                Wout[ell,i] *= theta_k;
            #theta = min([1.0, \
            #             (rho_ave-epsilon)/(rho_ave-rho_min), \
            #             (p_ave-epsilon)/(p_ave-p_min), \
            #             (rstar_ave-epsilon)/(rstar_ave-rstar_min)]);

            #for ell in range (1,numPredBasis):
            #    Wout[ell,i] *= theta;
            #for ell in range (numPredBasis+1,2*numPredBasis):
            #    Wout[ell,i] *= theta;
            #for ell in range (2*numPredBasis+1,3*numPredBasis):
            #    Wout[ell,i] *= theta;
            #for ell in range (3*numPredBasis+1,4*numPredBasis):
            #    Wout[ell,i] *= theta;
            #for ell in range (4*numPredBasis+1,5*numPredBasis):
            #    Wout[ell,i] *= theta;
        # --------------------------------------------------------------------

    return Wout;
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def CorrectionStep_Positivity_Limiter(Qin,
                                      numGridCells,
                                      numOrder,
                                      numEqns,
                                      epsilon,
                                      AppParams,
                                      num_phiAtPosPts,
                                      phiAtPosPts):

    from numpy import copy,isnan;
    from FluxFunctions import primvars;
    
    Qout = copy(Qin);

    for i in range (0,numGridCells):

        prim_ave  = primvars(Qin[0,i,0],Qin[1,i,0],Qin[2,i,0],Qin[3,i,0],Qin[4,i,0]);
        rho_ave   = prim_ave[0]
        p_ave     = prim_ave[2];
        rstar_ave = prim_ave[4];

        rho_min   = rho_ave;
        p_min     = p_ave;
        rstar_min = rstar_ave;

        if rho_ave < epsilon or isnan(rho_ave):
            print ("ERROR: average density in CORRECTION is less than epsilon")
            print ("i = %i, rho_ave = %24.16e\n" % (i,rho_ave));
            raise

        if p_ave < epsilon or isnan(p_ave):
            print ("ERROR: average pressure in CORRECTION is less than epsilon")
            print ("i = %i, p_ave = %24.16e\n" % (i,p_ave));
            raise

        if rstar_ave < epsilon or isnan(rstar_ave):
            print ("ERROR: average rstar in CORRECTION is less than epsilon")
            print ("i = %i, rstar_ave = %24.16e\n" % (i,rstar_ave));
            raise

        # LIMIT TO GET POSITIVE DENSITY
        for ell in range (0,num_phiAtPosPts):
            rho_val = 0.0+rho_ave;

            for k in range (1,numOrder):
                rho_val += Qin[0,i,k] * phiAtPosPts[k,ell];

            rho_min = min([rho_min,rho_val]);

        if rho_min>epsilon:
            theta = 1.0;
        else:
            theta = min([1.0, (rho_ave - epsilon)/(rho_ave - rho_min)]);
            for m in range(0,numEqns):
                for ell in range (1,numOrder):
                    Qout[m,i,ell] *= theta;

        # LIMIT TO GET POSITIVE PRESSURE
        for ell in range (0,num_phiAtPosPts):
            rho_val = Qout[0,i,0];
            mom_val = Qout[1,i,0];
            nrg_val = Qout[2,i,0];
            for k in range (1,numOrder):
                rho_val += Qout[0,i,k] * phiAtPosPts[k,ell];
                mom_val += Qout[1,i,k] * phiAtPosPts[k,ell];
                nrg_val += Qout[2,i,k] * phiAtPosPts[k,ell];

            p_val = nrg_val - (mom_val**2)/rho_val;
            p_min = min([p_min,p_val]);

        if p_min>epsilon:
            theta = 1.0;
        else:
            theta = min([1.0, (p_ave - epsilon)/(p_ave - p_min)]);
            for m in range(0,numEqns):
                for ell in range (1,numOrder):
                    Qout[m,i,ell] *= theta;


        # LIMIT TO GET POSITIVE RSTAR
        for ell in range (0,num_phiAtPosPts):
            rho_val  = Qout[0,i,0];
            mom_val  = Qout[1,i,0];
            nrg_val  = Qout[2,i,0];
            con4_val = Qout[3,i,0];
            con5_val = Qout[4,i,0];
            for k in range (1,numOrder):
                rho_val  += Qout[0,i,k] * phiAtPosPts[k,ell];
                mom_val  += Qout[1,i,k] * phiAtPosPts[k,ell];
                nrg_val  += Qout[2,i,k] * phiAtPosPts[k,ell];
                con4_val += Qout[3,i,k] * phiAtPosPts[k,ell];
                con5_val += Qout[4,i,k] * phiAtPosPts[k,ell];

            rstar_val  = primvars(rho_val,mom_val,nrg_val,con4_val,con5_val)[4];
            rstar_min = min([rstar_min,rstar_val]);

        if rstar_min>epsilon:
            theta = 1.0;
        else:
            theta = min([1.0, (rstar_ave - epsilon)/(rstar_ave - rstar_min)]);
            for m in range(0,numEqns):
                for ell in range (1,numOrder):
                    Qout[m,i,ell] *= theta;

    return Qout;
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def CorrectionStep_Moment_Limiter(Q,
                                  numOrder,
                                  numEqns,
                                  MeshParams,
                                  AppParams,
                                  epsilon,
                                  num_LimPts,
                                  phiAtLimPts):

    from numpy import max,min,zeros,dot,shape,swapaxes;
    from FluxFunctions import GetPrimFromCons,primvars,consvars;
    numGridCells = MeshParams['numGridCells'];
    spacestep    = MeshParams['spacestep'];
    boundary     = MeshParams['boundary'];

    Max_Prim    = zeros([numGridCells,5]);
    Min_Prim    = zeros([numGridCells,5]);
    Ave_Prim    = zeros([numGridCells,5]);
    M           = zeros([numGridCells,5]);
    m           = zeros([numGridCells,5]);
    tempTheta_M = zeros([numGridCells,5]);
    tempTheta_m = zeros([numGridCells,5]);
    theta_M     = zeros(numGridCells);
    theta_m     = zeros(numGridCells);
    theta       = zeros(numGridCells);
    troubled_cell = zeros(numGridCells,dtype=int);

    #LIMITING OVER ONLY RHO, P, RSTAR

    for i in range(0,numGridCells): #Step 1
        Q_at_LimPts = dot(Q[:,i,:],phiAtLimPts);

        Prim_at_LimPts = GetPrimFromCons(Q_at_LimPts,numEqns,numOrder+2,AppParams);
        #for j in range(0,numEqns):

        for j in range(0,5):
            #Max_Prim[i,j] = max(Prim_at_LimPts[2*j,:]);
            #Min_Prim[i,j] = min(Prim_at_LimPts[2*j,:]);
            Max_Prim[i,j] = max(Prim_at_LimPts[j,:]);
            Min_Prim[i,j] = min(Prim_at_LimPts[j,:]);
        Ave_Prim[i,:] = primvars(Q[0,i,0],Q[1,i,0],Q[2,i,0],Q[3,i,0],Q[4,i,0]);
        #temp = primvars(Q[0,i,0],Q[1,i,0],Q[2,i,0],Q[3,i,0],Q[4,i,0]);
        #Ave_Prim[i,0] = temp[0];
        #Ave_Prim[i,1] = temp[2];
        #Ave_Prim[i,2] = temp[4];

    #alpha = 1.0*spacestep**1.5;
    alpha = 0.01*spacestep**1.5;

    if boundary == 'extrapolation':
        for j in range(0,5):
        #for j in range(0,numEqns):
            M[0,j] = max([Ave_Prim[0,j]+alpha,Max_Prim[1,j]]);
            m[0,j] = min([Ave_Prim[0,j]-alpha,Min_Prim[1,j]]);
            M[numGridCells-1,j] = max([Ave_Prim[numGridCells-1,j]+alpha,Max_Prim[numGridCells-2,j]]);
            m[numGridCells-1,j] = min([Ave_Prim[numGridCells-1,j]-alpha,Min_Prim[numGridCells-2,j]]);

    if boundary == 'periodic':
        for j in range(0,5):
        #for j in range(0,numEqns):
            M[0,j] = max([Ave_Prim[0,j]+alpha,Max_Prim[1,j],Max_Prim[numGridCells-1,j]]);
            m[0,j] = min([Ave_Prim[0,j]-alpha,Min_Prim[1,j],Min_Prim[numGridCells-1,j]]);
            M[numGridCells-1,j] = max([Ave_Prim[numGridCells-1,j]+alpha,Max_Prim[numGridCells-2,j], \
            Max_Prim[0,j]]);
            m[numGridCells-1,j] = min([Ave_Prim[numGridCells-1,j]-alpha,Min_Prim[numGridCells-2,j], \
            Min_Prim[0,j]]);

    for i in range(1,numGridCells-1):  #Step 2
        for j in range(0,5):
        #for j in range(0,numEqns):
            M[i,j] = max([Ave_Prim[i,j]+alpha,Max_Prim[i-1,j],Max_Prim[i+1,j]]);
            m[i,j] = min([Ave_Prim[i,j]-alpha,Min_Prim[i-1,j],Min_Prim[i+1,j]]);

    #print("M is",M[2,2]);
    #print("m is",m[2,2]);
    for i in range(0,numGridCells):  #Step 3
        for j in range(0,5):
        #for j in range(0,numEqns):
            tempTheta_M[i,j] = min([(M[i,j]-Ave_Prim[i,j])/(1.1*(Max_Prim[i,j]-Ave_Prim[i,j]+1.0e-10)),1]);
            tempTheta_m[i,j] = min([(Ave_Prim[i,j]-m[i,j])/(1.1*(Ave_Prim[i,j]-Min_Prim[i,j]+1.0e-10)),1]);
        theta_M[i] = min(tempTheta_M[i,:]);
        theta_m[i] = min(tempTheta_m[i,:]);

    for i in range(0,numGridCells):  #Step 4
        theta[i] = max([0.0,min([1.0,theta_m[i],theta_M[i]])]);
        #if theta[i] != 1:
        #theta[i] = 0.8*theta[i]



    #print("theta is",theta[2])
    #swapQ = swapaxes(Q,0,1);

    for i in range(0,numGridCells):  #Step 5
        for k in range(1,numOrder):
            #q_tilde[i,:,k] = consvars(Ave_Prim[i,0],Ave_Prim[i,1],Ave_Prim[i,2],Ave_Prim[i,3],Ave_Prim[i,4]) \
            #    + theta[i]*(swapQ[i,:,k] - consvars(Ave_Prim[i,0],Ave_Prim[i,1],Ave_Prim[i,2],Ave_Prim[i,3],Ave_Prim[i,4]));
            Q[:,i,k] = theta[i]*Q[:,i,k];
        #if theta[i] <= .99:
        #    troubled_cell[i] = 1;


    return Q#,troubled_cell;#q_tilde

#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def convert_to_char_vars(W,
                         Q,
                         numGridCells,
                         numOrder,
                         numPredBasis,
                         numEqns,
                         boundary,
                         AppParams):

    from numpy import zeros,arange,sqrt,array,dot,abs,max;
    gamma = AppParams['gamma'];

    # Characteristic variable storage
    C_cent   = zeros([numEqns,numGridCells,numOrder-1]);
    dC_right = zeros([numEqns,numGridCells,numOrder-1]);
    dC_left  = zeros([numEqns,numGridCells,numOrder-1]);

    ip1_vec = arange(numGridCells)+1;
    im1_vec = arange(numGridCells)-1;
    if boundary=='periodic':
        ip1_vec[numGridCells-1] = 0;
        im1_vec[0] = numGridCells-1;
    elif boundary=='extrapolation':
        ip1_vec[numGridCells-1] = numGridCells-1;
        im1_vec[0] = 0;

    sc = zeros(numOrder-1);
    for ell in range(0,numOrder-1):
        sc[ell] = sqrt((2.0*ell+1.0)/(2.0*ell+3.0));

    for i in range(0,numGridCells):

        ip1 = ip1_vec[i];
        im1 = im1_vec[i];

        rho = max([1.0e-6,Q[0,i,0]]);
        u   = Q[1,i,0]/rho;
        p   = max([1.0e-6,(gamma-1.0)*(Q[2,i,0]-0.5*rho*u**2)]);
        cs  = sqrt(gamma*p/rho);

        Rinv = array([[(2.0*gamma*p + rho*u**2 - gamma*rho*u**2)/(2.0*gamma*p),
                       ((gamma-1.0)*rho*u)/(gamma*p),
                       (((1.0 - gamma)*rho)/(gamma*p))],
                      [(u*(2.0*gamma*p - cs*rho*u + cs*gamma*rho*u))/(4.0*cs*gamma*p),
                       -((gamma*p - cs*rho*u + cs*gamma*rho*u)/(2.0*cs*gamma*p)),
                       ((gamma-1.0)*rho)/(2.0*gamma*p)],
                      [(u*(-2.0*gamma*p - cs*rho*u + cs*gamma*rho*u))/(4.0*cs*gamma*p),
                       -((-gamma*p - cs*rho*u + cs*gamma*rho*u)/(2.0*cs*gamma*p)),
                       ((-1.0 + gamma)*rho)/(2.0*gamma*p)]]);

        for ell in range(0,numOrder-1):
            C_cent[:,i,ell]   =         dot(Rinv, Q[:,i,ell+1]                );
            dC_left[:,i,ell]  = sc[ell]*dot(Rinv, Q[:,i,  ell] - Q[:,im1,ell] );
            dC_right[:,i,ell] = sc[ell]*dot(Rinv, Q[:,ip1,ell] - Q[:,i,  ell] );

    return dC_right,dC_left,C_cent
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def convert_to_cons_vars(Cin,
                         Win,
                         Qin,
                         numGridCells,
                         numOrder,
                         numPredBasis,
                         numEqns,
                         AppParams):

    from numpy import copy,array,dot,sqrt,max;
    gamma = AppParams['gamma'];

    # Conservative variable storage
    Qout = copy(Qin);

    for i in range(0,numGridCells):

        rho = Qin[0,i,0]; #max([1.0e-6,Qin[0,i,0]]);
        u   = Qin[1,i,0]/rho;
        p   = (gamma-1.0)*(Qin[2,i,0]-0.5*rho*u**2); #max([1.0e-6,(gamma-1.0)*(Qin[2,i,0]-0.5*rho*u**2)]);
        cs  = sqrt(gamma*p/rho);

        R = array([[1.0, 1.0,  1.0],
                   [u,   u-cs, u+cs],
                   [0.5*u**2,
                    p*gamma/((gamma-1.0)*rho) + 0.5*u**2 - u*cs,
                    p*gamma/((gamma-1.0)*rho) + 0.5*u**2 + u*cs]]);

        for ell in range(1,numOrder):
            Qout[:,i,ell] = dot(R, Cin[:,i,ell-1]);

    return Qout;
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def minmod(a,b,c):
    from numpy import sign,min;

    if a*b<0.0 or b*c<0.0 or a*c<0.0:
        return 0.0

    return sign(a)*min([abs(a),abs(b),abs(c)]);
#------------------------------------------------------------------------------
