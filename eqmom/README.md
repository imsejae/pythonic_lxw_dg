# Quadrature-based moment-closure using 3 delta functions #

### Sub-directories:
* `lib/`            : library files relevant to all examples
* `shocktube/`      : Riemann problem showing five waves
* `vacuum/`         : Riemann problem with vacuum state

### Variables:
* `t`               : time
* `x`               : spatial coordinate
* `rho(t,x)`        : macroscopic fluid density
* `u(t,x)`          : macroscopic fluid velocity
* `p(t,x)`          : thermal pressure
* `q(t,x)`          : heat flux
* `rstar(t,x)`      : kurtosis
* `q1(t,x) = rho`   : macroscopic fluid density
* `q2(t,x) = rho*u` : macroscopic fluid momentum density
* `q3(t,x) = rho*u^2 + p`   : macroscopic fluid energy density
* `q4(t,x) = rho*u^3 + 3*p*u + q`   : q4
* `q5(t,x) = rho*u^4 + 6*p*u^2 + 4*q*u + rstar + p^2/rho + q^2/p`   : q5
* `s0(t,x), ..., s4(t,x)`         : source term functions

### Equations:
* `(rho)_{,t} + (rho*u)_{,x}       = s0`      : conservation of mass
* `(rho*u)_{,t} + (rho*u^2 + p)_{,x} = s1`      : conservation of momentum
* `(rho*u^2 + p)_{,t} + (rho*u^3 + 3*p*u + q)_{,x} = s2`      : conservation of energy
* `(rho*u^3 + 3*p*u + q)_{,t} + (rho*u^4 + 6*p*u^2 + 4*q*u + rstar + p^2/rho + q^2/p)_{,x} = s3`      : conservation of q4
* `(rho*u^4 + 6*p*u^2 + 4*q*u + rstar + p^2/rho + q^2/p)_{,t} + (rho*u^5 + 10*p*u^3 + 10*q*u^2 + (rstar + p^2/rho + q^2/p)*(5*u + 2*q/p) - q^3/p^2)_{,x} = s4`      : conservation of q5

### Primitive Equation Jacobian:
* `A = array([[u, rho, 0, 0, 0], [0, u, (1/rho), 0, 0], [0, 3*p, u, 1, 0], [-(p^2/rho^2), 4*q, -(q^2/p^2) - p/rho, (2*q)/p+u, 1], [0, 5*rstar, -((2*q*rstar)/p^2), ((2*rstar)/p), u]]);`

### Eigenvalues of Primitive Equation Jacobian:
* `lambda1 = u + q/(2*p) - sqrt(tmp1 + 4.0*sqrt(tmp2))/(2.0*(p**2)*(rho**2))`
* `lambda2 = u + q/(2*p) - sqrt(tmp1 - 4.0*sqrt(tmp2))/(2.0*(p**2)*(rho**2))`
* `lambda3 = u`
* `lambda4 = u + q/(2*p) + sqrt(tmp1 - 4.0*sqrt(tmp2))/(2.0*(p**2)*(rho**2))`
* `lambda5 = u + q/(2*p) + sqrt(tmp1 + 4.0*sqrt(tmp2))/(2.0*(p**2)*(rho**2))`
* `tmp1 = (p**2)*(rho**3)*(4.0*(p**3) + (q**2)*rho + 4.0*p*rho*rstar);`
* `tmp2 = (p**6)*(rho**7)*rstar*(p**2 + rho*rstar);`
