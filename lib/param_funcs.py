# ------------------------------------------------------------------------------
def create_lookup_table(numOrder,
                        numEqns,
                        numPredBasis,
                        xlow,
                        xhigh,
                        numGridCells,
                        spacestep,
                        boundary,
                        CFL,
                        CFL_max_allowed,
                        initialTime,
                        endTime,
                        numFrames,
                        timestepInitial,
                        limiters_minmod,
                        limiters_positv,
                        epsilon,
                        use_default_iter,
                        custom_iter):

    DimParams  = {'numOrder'         : numOrder,
                  'numEqns'          : numEqns,
                  'numPredBasis'     : numPredBasis};

    MeshParams = {'xlow'             : xlow,
                  'xhigh'            : xhigh,
                  'numGridCells'     : numGridCells,
                  'spacestep'        : spacestep,
                  'boundary'         : boundary};

    TimeParams = {'CFL'              : CFL,
                  'CFL_max_allowed'  : CFL_max_allowed,
                  'initialTime'      : initialTime,
                  'endTime'          : endTime,
                  'numFrames'        : numFrames,
                  'timestepInitial'  : timestepInitial,
                  'limiters_minmod'  : limiters_minmod,
                  'limiters_positv'  : limiters_positv,
                  'epsilon'          : epsilon};

    PredParams = {'use_default_iter' : use_default_iter,
                  'custom_iter'      : custom_iter};

    return DimParams,MeshParams,TimeParams,PredParams;
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
def derived_parameters(numOrder,numGridCells,xlow,xhigh):

    numPredBasis = int(((numOrder+1)*numOrder)/2); # basis for prediction step
    spacestep    = (xhigh-xlow)/numGridCells;      # spatial grid spacing

    return numPredBasis,spacestep;
# ------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def set_recommended_cfl(numOrder):

    # ******************************* #
    # The recommended CFL numbers are #
    #     0.90  for  numOrder = 1     #
    #     0.30  for  numOrder = 2     #
    #     0.14  for  numOrder = 3     #
    #     0.10  for  numOrder = 4     #
    #     0.06  for  numOrder = 5     #
    # ******************************* #

    assert(numOrder>=1);
    assert(numOrder<=5);

    if numOrder==1:
        return 0.90;
    elif numOrder==2:
        return 0.30;
    elif numOrder==3:
        return 0.13;
    elif numOrder==4:
        return 0.10;
    elif numOrder==5:
        return 0.06;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def set_recommended_cfl_max_allowed(numOrder):

    # *********************************** #
    # The recommended max CFL numbers are #
    #     0.92  for  numOrder = 1         #
    #     0.32  for  numOrder = 2         #
    #     0.16  for  numOrder = 3         #
    #     0.12  for  numOrder = 4         #
    #     0.08  for  numOrder = 5         #
    # *********************************** #

    assert(numOrder>=1);
    assert(numOrder<=5);

    if numOrder==1:
        return 0.92;
    elif numOrder==2:
        return 0.32;
    elif numOrder==3:
        return 0.15;
    elif numOrder==4:
        return 0.12;
    elif numOrder==5:
        return 0.08;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def check1(DimParams):
    assert(DimParams['numOrder']>=1);
    assert(DimParams['numOrder']<=5);
    assert(DimParams['numEqns']>=1);
    assert(DimParams['numPredBasis']==int((DimParams['numOrder']*(DimParams['numOrder']+1))/2));
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def check2(MeshParams):
    assert(MeshParams['xhigh']>MeshParams['xlow']);
    assert(MeshParams['numGridCells']>=1);
    assert(MeshParams['spacestep']==((MeshParams['xhigh']-MeshParams['xlow'])/MeshParams['numGridCells']));
    assert((MeshParams['boundary']=='extrapolation' or MeshParams['boundary']=='periodic'));
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def check3(TimeParams):
    assert(TimeParams['CFL']>=0.0);
    assert(TimeParams['CFL_max_allowed']>TimeParams['CFL']);
    assert(TimeParams['endTime']>=TimeParams['initialTime']);
    assert(TimeParams['numFrames']>=1);
    assert(TimeParams['timestepInitial']>0.0);
    assert((TimeParams['limiters_minmod']==False \
         or TimeParams['limiters_minmod']==True));
    assert((TimeParams['limiters_positv']==False \
        or TimeParams['limiters_positv']==True));
    assert(TimeParams['epsilon']>0.0);
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def check4(PredParams):
    assert(PredParams['use_default_iter']==False \
        or PredParams['use_default_iter']==True);
    assert(PredParams['custom_iter']>=1);
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def parameters_write_to_file(outputdir,
                             write_app_params,
                             AppParams,
                             DimParams,
                             MeshParams,
                             TimeParams,
                             PredParams):

    import os;

    check1(DimParams);
    check2(MeshParams);
    check3(TimeParams);
    check4(PredParams);

    filename = outputdir+ "/parameters.txt";
    file = open(filename,'w');

    file.write("\n");
    write_app_params(file,AppParams);

    file.write("DimParams  = {numOrder         :  %i,\n"        % DimParams['numOrder']);
    file.write("              numEqns          :  %i,\n"        % DimParams['numEqns']);
    file.write("              numPredBasis     :  %i};\n\n"     % DimParams['numPredBasis']);

    file.write("MeshParams = {xlow             : %22.15e,\n"    % MeshParams['xlow']);
    file.write("              xhigh            : %22.15e,\n"    % MeshParams['xhigh']);
    file.write("              numGridCells     :  %i,\n"        % MeshParams['numGridCells']);
    file.write("              spacestep        : %22.15e,\n"    % MeshParams['spacestep']);
    file.write("              boundary         :  %s};\n\n"     % MeshParams['boundary']);

    file.write("TimeParams = {CFL              : %22.15e,\n"    % TimeParams['CFL']);
    file.write("              CFL_max_allowed  : %22.15e,\n"    % TimeParams['CFL_max_allowed']);
    file.write("              initialTime      : %22.15e,\n"    % TimeParams['initialTime']);
    file.write("              endTime          : %22.15e,\n"    % TimeParams['endTime']);
    file.write("              numFrames        :  %i,\n"        % TimeParams['numFrames']);
    file.write("              timestepInitial  : %22.15e,\n"    % TimeParams['timestepInitial']);
    file.write("              limiters_minmod  :  %r,\n"        % TimeParams['limiters_minmod']);
    file.write("              limiters_positv  :  %r,\n"        % TimeParams['limiters_positv']);
    file.write("              epsilon          : %22.15e};\n\n" % TimeParams['epsilon']);

    file.write("PredParams = {use_default_iter :  %r,\n"        % PredParams['use_default_iter']);
    file.write("              custom_iter      :  %i};\n\n"     % PredParams['custom_iter']);

    file.close();
#-------------------------------------------------------------------------------
