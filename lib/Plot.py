#-------------------------------------------------------------------------------
def readParamFile(outputDir):

    from numpy import zeros;

    file = open(outputDir+'/plot_helper.txt','r');

    numFrames    =   int(file.readline());
    numOrder     =   int(file.readline());
    numGridCells =   int(file.readline());
    xlow         = float(file.readline());
    xhigh        = float(file.readline());
    numEqns      =   int(file.readline());
    timePerFrame = float(file.readline());

    return numFrames, numOrder, numGridCells, xlow, xhigh, numEqns, timePerFrame;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def readQdataFile(outputDir,
                  frame,
                  numEqns,
                  numGridCells,
                  numOrder,
                  fileprefix="/out"):

    from numpy import zeros;
    import os.path

    Q = zeros([numEqns, numGridCells, numOrder]);

    filename = outputDir+fileprefix + str(frame)+".txt";

    if os.path.isfile(filename)==False:
        print (" ");
        print (" Error in Plot.py, could not find file = ",filename)
        print (" ")
        raise;

    file = open(filename,'r');

    for k in range (0,numEqns):
        for i in range (0,numGridCells):
            linestring = file.readline()
            linelist = str.split(linestring)
            for j in range (0,numOrder):
                Q[k,i,j]= float(linelist[j])

    return Q;
#-------------------------------------------------------------------------------
###Added the below function to plot the electric field***Right now only works for 1st order
#-------------------------------------------------------------------------------
def readEdataFile(outputDir,
                  frame,
                  numEqns,
                  numGridCells,
                  numOrder):

    from numpy import zeros;
    import os.path

    E = zeros([numGridCells]);

    filename = outputDir+"/outelectric" + str(frame)+".txt";

    if os.path.isfile(filename)==False:
        print (" ");
        print (" Error in Plot.py, could not find file = ",filename)
        print (" ")
        raise;

    file = open(filename,'r');

    for k in range(0,numGridCells):
        linestring = file.readline()
        linelist = str.split(linestring)
        E[k] = float(linelist)

    return E;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def findy(x, Q, xlow, xhigh):

    from numpy import floor;
    from legendre_poly import phi;

    numGridCells, numOrder = Q.shape;
    spacestep = (xhigh-xlow)/numGridCells;

    if x == xhigh:
        i=numGridCells-1;
    else:
        i = int(floor(float(x-xlow)/spacestep));

    xc = float(xlow) +(float(i)+0.5)*spacestep;
    total = 0.0;
    xi = (2.0/spacestep)*(x-xc);

    for k in range(0,numOrder):
        total= total+Q[i,k]*phi(k,xi);

    return total
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def plotFrames(outputDir,
               AppParams,
               plot):

    import matplotlib.pylab as plt;
    from numpy import linspace;

    numFrames, numOrder, numGridCells, xlow, xhigh, numEqns, timePerFrame\
        = readParamFile(outputDir);

    print(" ");
    print("   ----------------------");
    print("   |  Plotting routine  |");
    print("   ----------------------");
    print(" ");

    pointsPerGridCell = input('Plot how many points per element?\n (press <Enter> to get default value 1): ');
    if pointsPerGridCell=='':
        pointsPerGridCell = 1;
    pointsPerGridCell = int(pointsPerGridCell)

    x = linspace(xlow, xhigh, numGridCells*pointsPerGridCell);

    plt.ion()

    print(" ");
    cnt = -1;
    while True:
        frame = input('Plot which frame? 0-' + str(numFrames) + '\n (Press <Enter> to continue to next frame, type "q" to quit): ')

        if frame=='':
            cnt = cnt+1;
            frame = cnt;
        elif frame=='q' or frame=='quit':
            break

        frame = int(frame)
        if frame > numFrames:
            frame = numFrames;
            print("< WARNING: Have reached the end of frames, reset frame to frame = %i >" % frame);

        variables = range(0,numEqns);

        cnt = frame;

        Q = readQdataFile(outputDir,
                          frame,
                          numEqns,
                          numGridCells,
                          numOrder);

        E = readQdataFile(outputDir, ###Added to plot electric field
                            frame,
                            1,
                            numGridCells,
                            numOrder,
                            "/outelectric");


        plot(x,
             Q,
             E, ###added to plot electric field
             xlow,
             xhigh,
             variables,
             frame*timePerFrame,
             AppParams);

        print(" ");

    print(" ");

    plt.ioff()
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def plotFrames_comparison(outputDir1,
                          outputDir2,
                          AppParams,
                          plot):

    import matplotlib.pylab as plt;
    from numpy import linspace;

    numFrames, numOrder1, numGridCells1, xlow, xhigh, numEqns, timePerFrame\
        = readParamFile(outputDir1);

    numFrames, numOrder2, numGridCells2, xlow, xhigh, numEqns, timePerFrame\
        = readParamFile(outputDir2);

    print(" ");
    print("   ----------------------");
    print("   |  Plotting routine  |");
    print("   ----------------------");
    print(" ");

    pointsPerGridCell = input('Plot how many points per element?\n (press <Enter> to get default value 1): ');
    if pointsPerGridCell=='':
        pointsPerGridCell = 1;
    pointsPerGridCell = int(pointsPerGridCell)

    x1 = linspace(xlow, xhigh, numGridCells1*pointsPerGridCell);
    x2 = linspace(xlow, xhigh, numGridCells2*pointsPerGridCell);

    plt.ion()

    print(" ");
    cnt = -1;
    while True:
        frame = input('Plot which frame? 0-' + str(numFrames) + '\n (Press <Enter> to continue to next frame, type "q" to quit): ')

        if frame=='':
            cnt = cnt+1;
            frame = cnt;
        elif frame=='q' or frame=='quit':
            break

        frame = int(frame)
        if frame > numFrames:
            frame = numFrames;
            print("< WARNING: Have reached the end of frames, reset frame to frame = %i >" % frame);

        variables = range(0,numEqns);

        cnt = frame;

        Q1 = readQdataFile(outputDir1,
                          frame,
                          numEqns,
                          numGridCells1,
                          numOrder1);

        Q2 = readQdataFile(outputDir2,
                          frame,
                          numEqns,
                          numGridCells2,
                          numOrder2);

        plot(x1,
             x2,
             Q1,
             Q2,
             xlow,
             xhigh,
             variables,
             frame*timePerFrame,
             AppParams);

        print(" ");

    print(" ");

    plt.ioff()
#-------------------------------------------------------------------------------
