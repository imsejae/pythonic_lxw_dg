#-------------------------------------------------------------------------------
def computeCoefficients(q0,
                        xlow,
                        spacestep,
                        numGridCells,
                        numOrder_local,
                        AppParams):

    from numpy import zeros;
    from gauss_quad_rules import gauss;
    from legendre_poly import phi;

    xi,omega=gauss(numOrder_local);
    Q = zeros((numGridCells, numOrder_local));
    for i in range(0,numGridCells): #iterating over all grid cells
        xc = xlow + (float(i)+0.5)*spacestep;
        for k in range (0,numOrder_local): #iterates over all coefficents(slope,curvature..)
            f = lambda xi : q0(xc+0.5*spacestep*xi,AppParams)*phi(k,xi);
            Q[i,k] = 0.5*GaussQuadrature(f,-1.0,1.0,numOrder_local);

    return Q
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def phiRecursive(k,xi):

    if k==0:
        return 1.0;
    if k==1:
        return np.sqrt(3.0)*xi;
    kk= k-1;
    ak = np.sqrt((1.0+2.0*kk)*(3.0+2.0*kk))/(1.0+kk);
    bk = np.sqrt((2.0*kk+3.0)/(2.0*kk-1.0)) * (1.0*kk)/(kk+1.0);

    return ak*xi*phi(k-1,xi)-bk*phi(k-2,xi)
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def phiDerRecursive(k,xi):

    from legendre_poly import phiDer;
    from numpy import sqrt;

    if k==0:
        return 0.0;
    if k==1:
        return sqrt(3.0);
    kk = k-1;
    ak = sqrt((1.0+2.0*kk)*(3.0+2.0*kk))/(1.0+kk);
    bk = sqrt((2.0*kk+3.0)/(2.0*kk-1.0)) * (1.0*kk)/(kk+1.0);

    return ak*phi(k-1,xi)+ ak*xi*phiDer(k-1,xi)-bk*phiDer(k-2,xi)
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def findInd(k):

    numOrder = 0;
    indices = 0;
    while indices<=k :
        indices += numOrder+1;
        numOrder += 1;
    indices -= numOrder;
    numOrder -= 1;
    a = numOrder - (k-indices);
    b = numOrder-a;

    return a,b
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def psi(k,xi,tau):

    from legendre_poly import phi;

    # k is the index of psi
    a, b = findInd(k);
    return phi(a,xi)*phi(b,tau);
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def psiderxi(k,xi,tau):

    from legendre_poly import phi;
    from legendre_poly import phiDer;

    # k is the index of psider
    a,b = findInd(k);

    return phiDer(a,xi)*phi(b,tau);
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def psidertau(k,xi,tau):

    from legendre_poly import phi;
    from legendre_poly import phiDer;

    a,b = findInd(k);

    return phi(a,xi)*phiDer(b,tau);
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def GaussQuadrature(f,xMinus,xPlus,numOrder):

    from gauss_quad_rules import gauss;

    xi, omega = gauss(numOrder);
    xc = 0.5*(xMinus + xPlus);
    deltaX= xPlus - xMinus;
    total = 0.0;
    for m in range (0, numOrder):
        total += omega[m]*f(xc+0.5*deltaX*xi[m]);

    return total;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def L2Project_GaussQuadrature(f,
                              xlow,
                              dx,
                              numGridCells,
                              numQuadOrder,
                              numBasisCmpts):

    from gauss_quad_rules import gauss;
    from numpy import copy,zeros;
    from legendre_poly import phi;

    numQuadPoints = numQuadOrder+0;

    xi,omega = gauss(numQuadPoints);
    phi_at_xi = zeros([numBasisCmpts,numQuadPoints]);
    for k in range(0,numBasisCmpts):
        for m in range (0,numQuadPoints):
            phi_at_xi[k,m] = phi(k,xi[m]);

    total = zeros([numGridCells,numBasisCmpts]);
    for i in range(0,numGridCells):
        xc = xlow + (i+0.5)*dx;
        for k in range(0,numBasisCmpts):
            for m in range (0,numQuadPoints):
                total[i,k] += 0.5*omega[m]*phi_at_xi[k,m]*f(xc+0.5*dx*xi[m]);

    return total;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def createQuadPoints(numOrder,
                     numPredBasis):

    from numpy import zeros;
    from numpy import copy;
    from gauss_quad_rules import gauss;
    from legendre_poly import phi;
    from legendre_poly import phiDer;

    numPredBasis_tmp    = int(((numOrder+1)*numOrder)/2);
    assert(numPredBasis==numPredBasis_tmp);
    psiAtQuadP      = zeros([numPredBasis,numOrder,numOrder]);
    psiderxiAtQuad  = zeros([numPredBasis,numOrder,numOrder]);
    psidertauAtQuad = zeros([numPredBasis,numOrder,numOrder]);
    xi, omega = gauss(numOrder);
    tau = copy(xi);

    for l in range(0,numPredBasis):
        for i in range(0,numOrder):
            for j in range(0,numOrder):
                psiAtQuadP[l,i,j]      = psi(l,xi[i],tau[j]);
                psiderxiAtQuad[l,i,j]  = psiderxi(l,xi[i],tau[j]);
                psidertauAtQuad[l,i,j] = psidertau(l,xi[i],tau[j]);

    psiAtQuadLeft   = zeros([numPredBasis,numOrder]);
    psiAtQuadBottom = zeros([numPredBasis,numOrder]);
    psiAtQuadRight  = zeros([numPredBasis,numOrder]);
    phiDerAtQuadP   = zeros([numOrder,numOrder]);
    phiAtQuad       = zeros([numOrder,numOrder]);
    phiAtLimPts     = zeros([numOrder,numOrder+2]);

    for l in range(0,numPredBasis):
        for i in range(0,numOrder):
            psiAtQuadLeft[l,i]   = psi(l,-1.0,tau[i]);
            psiAtQuadBottom[l,i] = psi(l,xi[i],-1.0);
            psiAtQuadRight[l,i]  = psi(l,1.0,tau[i]);

    for k in range(0,numOrder):
        for i in range (0,numOrder):
            phiDerAtQuadP[k,i] = phiDer(k,xi[i]);
            phiAtQuad[k,i] = phi(k,xi[i]);

    xi_mod = zeros(numOrder+2);
    xi_mod[0] = -1.0;
    xi_mod[numOrder+1] = 1.0;
    xi_mod[1:numOrder+1] = xi;

    num_phiAtLimPts = numOrder+2;
    for k in range(0,numOrder):
        for i in range(0,numOrder+2):    #for i in range(0,numOrder+2):
            phiAtLimPts[k,i] = phi(k,xi_mod[i]);

    if numOrder>1:
        phiatGLPts,psiatGlPts = createGLQuadPts(numOrder,numPredBasis);
    else:
        phiatGLPts = 0.0;
        psiatGlPts = 0.0;

    num_phiAtPosPts,phiAtPosPts = \
        phi_at_positivity_points(numOrder);

    num_psiAtPosPts,psiAtPosPts = \
        psi_at_positivity_points(numOrder,numPredBasis);

    QuadData = {            "psiAtQuadP": psiAtQuadP,
                        "psiderxiAtQuad": psiderxiAtQuad,
                       "psidertauAtQuad": psidertauAtQuad,
                        "psiAtQuadRight": psiAtQuadRight,
                         "psiAtQuadLeft": psiAtQuadLeft,
                       "psiAtQuadBottom": psiAtQuadBottom,
                         "phiDerAtQuadP": phiDerAtQuadP,
                             "phiAtQuad": phiAtQuad,
                                    "xi": xi,
                                 "omega": omega,
                            "phiatGLPts": phiatGLPts,
                            "psiatGlPts": psiatGlPts,
                       "num_phiAtPosPts": num_phiAtPosPts,
                           "phiAtPosPts": phiAtPosPts,
                       "num_psiAtPosPts": num_psiAtPosPts,
                           "psiAtPosPts": psiAtPosPts,
                       "num_phiAtLimPts": num_phiAtLimPts,
                           "phiAtLimPts": phiAtLimPts     };

    return QuadData;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def createGLQuadPts(numOrder,numPredBasis):

    from numpy import zeros;
    from numpy import copy;
    from gauss_lobatto_points import gausslobatto;
    from legendre_poly import phi;

    phiatGLPts = zeros([numOrder,numOrder]);
    psiatGlPts = zeros([numPredBasis,numOrder,numOrder]);
    xi = gausslobatto(numOrder);
    tau = copy(xi);

    for i in range (0,numOrder):
        for j in range (0,numOrder):
            phiatGLPts[i,j] = phi(i, xi[j]);

    for i in range(0, numPredBasis):
        for j in range(0,numOrder):
            for k in range(0,numOrder):
                psiatGlPts[i,j,k] = psi(i, xi[j],tau[k]);

    return phiatGLPts, psiatGlPts
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def FindPrimitiveVar(W, indexTau, indexXi, i, numPredBasis, psiAtQuadP):
    total = 0.0;
    for n in range(0,numPredBasis):
        total += (W[n,i] * psiAtQuadP[n, indexXi, indexTau])

    return total #returns primitive variable
#-------------------------------------------------------------------------------

#------------------- Convert from Conservative to Primitive Variables ----------
def ConvertConsToPrim(Q,
                      numGridCells,
                      numEqns,
                      numOrder,
                      phiAtQuad,
                      omega,
                      AppParams,
                      t,
                      xlow,
                      spacestep,
                      xi):

    from FluxFunctions import GetPrimFromCons;
    from numpy import zeros,sqrt,cos,pi;

    numQuadPts = numOrder+0;

    Q_at_QuadPts    = zeros([numEqns,
                             numQuadPts]);
    Prim            = zeros([numEqns,
                             numGridCells,
                             numOrder]);
    Prim_at_QuadPts    = zeros([numEqns,
                             numQuadPts]);

    for i in range(0,numGridCells):

        for m in range(0,numEqns):
            for nq in range(0,numQuadPts):
                Q_at_QuadPts[m,nq] = 0.0;
                for k in range(0,numOrder):
                    Q_at_QuadPts[m,nq] += Q[m,i,k] * phiAtQuad[k,nq];

        Prim_at_QuadPts = GetPrimFromCons(Q_at_QuadPts,
                                          numEqns,
                                          numQuadPts,
                                          AppParams);
        #xc = xlow + (i+0.5)*spacestep;

        #for nq in range(0,numQuadPts):
        #    x = xc + 0.5*spacestep*xi[nq]
        #    rhoEx = -(sqrt(pi)*(-2.0 + cos(2.0*pi*t - 2.0*x)))/2.0
        #    Prim_at_QuadPts[0,nq] = rhoEx;
        #    Prim_at_QuadPts[1,nq] = rhoEx/4.0;
        #    Prim_at_QuadPts[2,nq] = 3.0*rhoEx/16;
        #    Prim_at_QuadPts[3,nq] = 7.0*rhoEx/64.0;
        #    Prim_at_QuadPts[4,nq] = 25.0*rhoEx/256.0;

        for m in range(0,numEqns):
            for nq in range(0,numQuadPts):
                for k in range(0,numOrder):
                    Prim[m,i,k] +=  0.5*omega[nq]\
                                       *phiAtQuad[k,nq]\
                                       *Prim_at_QuadPts[m,nq];

    return Prim;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def phi_at_positivity_points(numOrder):

    from numpy import zeros,concatenate;
    from gauss_quad_rules import gauss;
    from gauss_lobatto_points import gausslobatto;
    from legendre_poly import phi;

    num_phiAtPosPts = numOrder+2;#+numOrder; # gauss quad pts + gauss lobatto

    gauss_pts_tmp,omega_tmp = gauss(numOrder);
    xi_pos                  = concatenate((gauss_pts_tmp,[-1.0,1.0]));
    #gauss_lob_pts_tmp       = gausslobatto(numOrder);
    #xi_pos                  = concatenate((gauss_pts_tmp,gauss_lob_pts_tmp));
    #xi_pos = gauss_lob_pts_tmp;
    assert(len(xi_pos)==num_phiAtPosPts);

    phiAtPosPts = zeros([numOrder,num_phiAtPosPts]);
    for nb in range(0,numOrder):
        for nq in range(0,num_phiAtPosPts):
            phiAtPosPts[nb,nq] = phi(nb,xi_pos[nq]);

    return num_phiAtPosPts,phiAtPosPts;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def psi_at_positivity_points(numOrder,numPredBasis):

    from numpy import zeros;
    from gauss_quad_rules import gauss;

    gauss_pts_tmp,omega_tmp = gauss(numOrder);

    num_psiAtPosPts = 0;
    if numOrder==1:
        num_psiAtPosPts = 1;
    elif numOrder==2:
        num_psiAtPosPts = 4;
    else:
        num_psiAtPosPts = 4 + 4*numOrder + numOrder**2;

    xi_pos  = zeros(num_psiAtPosPts);
    tau_pos = zeros(num_psiAtPosPts);

    if numOrder==1:
        k = 0;
        xi_pos[0]  =  0.0;
        tau_pos[0] =  0.0;
    elif numOrder==2:
        k = 0;
        xi_pos[k]  = -1.0;
        tau_pos[k] = -1.0;

        k = 1;
        xi_pos[k]  = -1.0;
        tau_pos[k] =  1.0;

        k = 2;
        xi_pos[k]  =  1.0;
        tau_pos[k] = -1.0;

        k = 3;
        xi_pos[k]  =  1.0;
        tau_pos[k] =  1.0;
    else:
        k = 0;
        xi_pos[k]  = -1.0;
        tau_pos[k] = -1.0;

        k = 1;
        xi_pos[k]  = -1.0;
        tau_pos[k] =  1.0;

        k = 2;
        xi_pos[k]  =  1.0;
        tau_pos[k] = -1.0;

        k = 3;
        xi_pos[k]  =  1.0;
        tau_pos[k] =  1.0;

        for nq in range(0,numOrder):
            k = k+1;
            xi_pos[k]  = -1.0;
            tau_pos[k] =  gauss_pts_tmp[nq];

        for nq in range(0,numOrder):
            k = k+1;
            xi_pos[k]  =  1.0;
            tau_pos[k] =  gauss_pts_tmp[nq];

        for nq in range(0,numOrder):
            k = k+1;
            xi_pos[k]  =  gauss_pts_tmp[nq];
            tau_pos[k] = -1.0;

        for nq in range(0,numOrder):
            k = k+1;
            xi_pos[k]  =  gauss_pts_tmp[nq];
            tau_pos[k] =  1.0;

        for nq1 in range(0,numOrder):
            for nq2 in range(0,numOrder):
                k = k+1;
                xi_pos[k]  =  gauss_pts_tmp[nq1];
                tau_pos[k] =  gauss_pts_tmp[nq2];

    assert(num_psiAtPosPts==(k+1));

    psiAtPosPts = zeros([numPredBasis,num_psiAtPosPts]);
    for nb in range(0,numPredBasis):
        for nq in range(0,num_psiAtPosPts):
            psiAtPosPts[nb,nq] = psi(nb,xi_pos[nq],tau_pos[nq]);

    return num_psiAtPosPts,psiAtPosPts;
#-------------------------------------------------------------------------------
