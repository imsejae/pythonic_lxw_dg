#-------------------------------------------------------------------------------
def phi(k, xi):
    if k==0:
        return 1.0
    elif k==1:
        return 1.73205080757*xi
    elif k==2:
        return -1.11803398875 + 3.35410196625*xi**2
    elif k==3:
        return -3.9686269666*xi + 6.61437827766*xi**3
    elif k==4:
        return 1.125 + -11.25*xi**2 + 13.125*xi**4
    elif k==5:
        return 6.21867148192*xi + -29.0204669156*xi**3 + 26.118420224*xi**5
    elif k==6:
        return -1.12673477358 + 23.6614302452*xi**2 + -70.9842907357*xi**4 + 52.0551465395*xi**6
    elif k==7:
        return -8.47215106983*xi + 76.2493596285*xi**3 + -167.748591183*xi**5 + 103.84436597*xi**7
    elif k==8:
        return 1.1274116945 + -40.5868210022*xi**2 + 223.227515512*xi**4 + -386.927693554*xi**6 + 207.282692975*xi**8
    elif k==9:
        return 10.7269778689*xi + -157.329008743*xi**3 + 613.583134099*xi**5 + -876.547334428*xi**7 + 413.925130146*xi**9
    else:
        print('Error: Legendre polynomial above order 7 not implemented')
        raise
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def phiDer(k, xi):
    if k==0:
        return 0.0
    elif k==1:
        return 1.73205080757
    elif k==2:
        return 2*3.35410196625*xi
    elif k==3:
        return -3.9686269666 + 3*6.61437827766*xi**2
    elif k==4:
        return 2*-11.25*xi + 4*13.125*xi**3
    elif k==5:
        return 6.21867148192 + 3*-29.0204669156*xi**2 + 5*26.118420224*xi**4
    elif k==6:
        return 2*23.6614302452*xi + 4*-70.9842907357*xi**3 + 6*52.0551465395*xi**5
    elif k==7:
        return -8.47215106983 + 3*76.2493596285*xi**2 + 5*-167.748591183*xi**4 + 7*103.84436597*xi**6
    elif k==8:
        return 2*-40.5868210022*xi + 4*223.227515512*xi**3 + 6*-386.927693554*xi**5 + 8*207.282692975*xi**7
    elif k==9:
        return 10.7269778689 + 3*-157.329008743*xi**2 + 5*613.583134099*xi**4 + 7*-876.547334428*xi**6 + 9*413.925130146*xi**8
    else:
        print('Error: Legendre polynomial above order 9 not implemented')
        raise
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def printLegendrePolynomial(order):
    coefficients = np.zeros([order, order])
    for k in range(order):
        if k == 0:
            coefficients[k,0] = 1.0
        elif k == 1:
            coefficients[k,1] = np.sqrt(3.0)
        else:
            kk = k - 1
            ak = np.sqrt((1.0+2.0*kk)*(3.0+2.0*kk))/(1.0+kk)
            bk = np.sqrt((2.0*kk+3.0)/(2.0*kk-1.0)) * (1.0*kk)/(kk+1.0)
            coefficients[k,:] = ak*np.insert(coefficients[k-1,:-1], 0, 0.0) - bk*coefficients[k-2,:]

    print('def phi(k, xi):')
    for i in range(order):
        if i == 0:
            print('    if k==' + str(i) + ':')
        else:
            print('    elif k==' + str(i) + ':')
        s = '';
        for j in range(order):
            if coefficients[i, j] != 0:
                if j == 0:
                    s = s + str(coefficients[i, j]) + ' + '
                elif j == 1:
                    s = s + str(coefficients[i, j]) + '*xi + '
                else:
                    s = s + str(coefficients[i, j]) + '*xi**' + str(j) + ' + '

        print('        return ' + s[:-3])
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def printLegendrePolynomialDer(order):
    coefficients = np.zeros([order, order])
    for k in range(order):
        if k == 0:
            coefficients[k,0] = 1.0
        elif k == 1:
            coefficients[k,1] = np.sqrt(3.0)
        else:
            kk = k - 1
            ak = np.sqrt((1.0+2.0*kk)*(3.0+2.0*kk))/(1.0+kk)
            bk = np.sqrt((2.0*kk+3.0)/(2.0*kk-1.0)) * (1.0*kk)/(kk+1.0)
            coefficients[k,:] = ak*np.insert(coefficients[k-1,:-1], 0, 0.0) - bk*coefficients[k-2,:]

    print('def phider(k, xi):')
    for i in range(order):
        if i == 0:
            print('    if k==' + str(i) + ':')
            print('        return 0.0')
        else:
            print('    elif k==' + str(i) + ':')
            s = '';
            for j in range(1,order):
                if coefficients[i, j] != 0:
                    if j == 1:
                        s = s + str(coefficients[i, j]) + ' + '
                    elif j == 2:
                        s = s + str(j) + '*' + str(coefficients[i, j]) + '*xi' + ' + '
                    else:
                        s = s + str(j) + '*' + str(coefficients[i, j]) + '*xi**' + str(j-1) + ' + '

            print('        return ' + s[:-3])
#-------------------------------------------------------------------------------
