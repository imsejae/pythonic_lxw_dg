#-------------------------------------------------------------------------------
def gausslobatto(numOrder=-1):

    from numpy import array,sqrt;

    assert(numOrder>=2 or numOrder<=7);

    if numOrder ==2:
        return array([-1.0,1.0])
    if numOrder ==3:
        return array([-1.0, 0.0, 1.0])
    if numOrder ==4:
        return array([-1.0,-1.0/5.0*sqrt(5.0), 1.0/5.0*sqrt(5.0), 1.0])
    if numOrder==5:
        return array([-1.0,-sqrt(3.0/7.0),0, sqrt(3.0/7.0), 1.0])
    if numOrder==6:
        return array([-1.0, -sqrt(1.0/3.0+2.0*sqrt(7.0)/21.0), -sqrt(1.0/3.0-2.0*sqrt(7.0)/21.0), \
            sqrt(1.0/3.0-2.0*sqrt(7.0)/21.0), sqrt(1.0/3.0+2.0*sqrt(7.0)/21.0), 1.0])
    if numOrder==7:
        return array([-1.0, -sqrt(5.0/11.0+2.0/11.0*sqrt(5.0/3.0)), -sqrt(5.0/11.0-2.0/11.0*sqrt(5.0/3.0)), 0, \
            sqrt(5.0/11.0-2.0/11.0*sqrt(5.0/3.0)), sqrt(5.0/11.0+2.0/11.0*sqrt(5.0/3.0)),1.0])
#-------------------------------------------------------------------------------
