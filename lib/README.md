# Code main library files #

### Files:
* `LaxWendroffDG.py`         - (main) discontinous Galerkin Lax-Wendroff method
* `Plot.py`                  - (main) plotting routine
* `param_funcs.py`           - (helper) routines to process simulation parameters
* `basis_data.py`            - (helper) routines to evaluate solution/basis and perform L2-projections
* `gauss_lobatto_points.py`  - (helper) Gauss-Lobatto point information
* `gauss_quad_rules.py`      - (helper) Gaussian quadrature rules for integration
* `legendre_poly.py`         - (helper) routines to evaluate Legendre polynomials and derivatives
