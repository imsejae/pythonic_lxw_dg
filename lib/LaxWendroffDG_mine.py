#-------------------------------------------------------------------------------
def LaxWendDG(q,                   # initial condition function
              source_conservative, # source term function for conservative vars
              source_primitive,    # source term function for primitive vars
              AppParams,           # application-specific parameters
              DimParams,           # parameters determining dimensions of arrays
              MeshParams,          # mesh-specific parameters
              TimeParams,          # time-stepping-specific parameters
              PredParams,          # prediction-step-specific parameters
              outputDir,           # which directory to output
              verbosity=True):     # print runtime information to screen

    from numpy import zeros,isnan;
    from basis_data import computeCoefficients;
    from basis_data import createQuadPoints;

    # Print welcome message
    if verbosity==True:
        printWelcome();

    # creating the time vector to iterate over
    times = zeros(TimeParams['numFrames']+1);
    for v in range(0,TimeParams['numFrames']+1):
        times[v] = TimeParams['endTime']*v/TimeParams['numFrames'];

    timePerFrame = TimeParams['endTime']/TimeParams['numFrames'];
    createPlotHelper(outputDir,
                     TimeParams['numFrames'],
                     DimParams['numOrder'],
                     MeshParams['numGridCells'],
                     MeshParams['xlow'],
                     MeshParams['xhigh'],
                     DimParams['numEqns'],
                     timePerFrame);

    # create quadrature information
    QuadData = createQuadPoints(DimParams['numOrder'],
                                DimParams['numPredBasis']);

    # initial conditions for all equations
    Q = zeros([DimParams['numEqns'],
               MeshParams['numGridCells'],
               DimParams['numOrder']]);
    for k in range (0,DimParams['numEqns']):
        Q[k,:,:] = computeCoefficients(q[k],
                                       MeshParams['xlow'],
                                       MeshParams['spacestep'],
                                       MeshParams['numGridCells'],
                                       DimParams['numOrder'],
                                       AppParams);

    # output solution Q
    outputQatEndOfFrame(Q,
                        0,
                        DimParams['numEqns'],
                        MeshParams['numGridCells'],
                        DimParams['numOrder'],
                        outputDir);

    # loop over each time frame
    for n in range(0, TimeParams['numFrames']):
        # take several time steps to complete one frame
        Q,numIterations = TimeStepOverOneFrame(n,
                                               AppParams,
                                               DimParams,
                                               MeshParams,
                                               TimeParams,
                                               PredParams,
                                               Q,
                                               times[n],
                                               times[n+1],
                                               source_conservative,
                                               source_primitive,
                                               QuadData,
                                               verbosity);

        # output solution Q
        outputQatEndOfFrame(Q,
                            n+1,
                            DimParams['numEqns'],
                            MeshParams['numGridCells'],
                            DimParams['numOrder'],
                            outputDir);

        # output number of iterations required
        outputNumIterations(MeshParams['numGridCells'],
                            numIterations,
                            outputDir,
                            n);

    if verbosity==True:
        print(" ");

    return Q
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def TimeStepOverOneFrame(FRAME_NUMBER,
                         AppParams,
                         DimParams,
                         MeshParams,
                         TimeParams,
                         PredParams,
                         Q,
                         tInitial,
                         tFinal,
                         source_conservative,
                         source_primitive,
                         QuadData,
                         verbosity):

    from numpy import dot,zeros,copy,min,isnan;

    t = tInitial;
    timestep = TimeParams['timestepInitial'];
    W = zeros([DimParams['numPredBasis']*DimParams['numEqns'],\
               MeshParams['numGridCells']]);
    troubled_cell = zeros(MeshParams['numGridCells'],dtype=int);

    if verbosity==True:
        print(" ");
        print("Starting frame number %i/%i at time t = %9.3e" % \
            (FRAME_NUMBER+1,TimeParams['numFrames'],t));

    step_number = 0;
    if (t>=tFinal):
        numIterations = zeros(MeshParams['numGridCells']);

    while(t<tFinal):
        step_number = step_number + 1;

        accept_time_step = False

        while(accept_time_step==False):

            tsave = t+0.0;
            Qsave = copy(Q);
            Wsave = copy(W);


            # ---------------
            # PREDICTION STEP
            # ---------------
            W,numIterations = predictionStep(DimParams,
                                             MeshParams,
                                             TimeParams,
                                             PredParams,
                                             AppParams,
                                             QuadData,
                                             source_primitive,
                                             timestep,
                                             t,
                                             Q,
                                             troubled_cell); # initial condition for prediction step

            # ---------------
            # CORRECTION STEP
            # ---------------
            Q,CFL_actual,speed_MaxGlobal = correctionStep(DimParams,
                                                          MeshParams,
                                                          TimeParams,
                                                          AppParams,
                                                          QuadData,
                                                          source_conservative,
                                                          timestep,
                                                          t,
                                                          Q,  # initial condition
                                                          W); # predicted solution

            if verbosity==True:
                print("   Step %4i:  CFL = %5f, dt = %9.3e, [t, t+dt] = [%9.3e, %9.3e]" \
                    % (step_number,CFL_actual,timestep,t,t+timestep));

            if CFL_actual<=TimeParams['CFL_max_allowed']:
                accept_time_step = True;
                t += timestep;
                timestep = min([TimeParams['CFL']*MeshParams['spacestep']/speed_MaxGlobal,tFinal-t]);
            else:
                timestep = MeshParams['spacestep']*TimeParams['CFL']/speed_MaxGlobal;
                t = tsave+0.0;
                Q = copy(Qsave);
                W = copy(Wsave);
                if verbosity==True:
                    print("        WARNING: timestep rejected, CFL = %7f, new dt = %9.3e" % (CFL_actual,timestep));

            if (timestep<1.0e-14) and ((tFinal-t)>1.0e-14):
                print(" ");
                print("    ERROR:  timestep is too small, dt = %9.3e, ENDING SIMULATION." % timestep);
                print(" ");
                raise

    if verbosity==True:
        print("Done with frame %i/%i at time t = %9.3e" % \
            (FRAME_NUMBER+1,TimeParams['numFrames'],t));

    return Q,numIterations
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def predictionStep(DimParams,
                   MeshParams,
                   TimeParams,
                   PredParams,
                   AppParams,
                   QuadData,
                   source_primitive,
                   timestep,
                   time,
                   Qinit,
                   troubled_cell):   # initial condition for prediction step

    from numpy import zeros,copy;
    from basis_data import ConvertConsToPrim;
    import sys; sys.path.insert(0,'../lib/');
    from LimiterFunctions import PredictionStep_Positivity_Limiter
    #print(troubled_cell);

    # store some parameters
    xlow             = MeshParams['xlow'            ];
    spacestep        = MeshParams['spacestep'       ];
    numGridCells     = MeshParams['numGridCells'    ];
    numEqns          =  DimParams['numEqns'         ];
    numOrder         =  DimParams['numOrder'        ];
    numPredBasis     =  DimParams['numPredBasis'    ];
    phiAtQuad        =   QuadData['phiAtQuad'       ];
    psiAtQuadP       =   QuadData['psiAtQuadP'      ];
    psiderxiAtQuad   =   QuadData['psiderxiAtQuad'  ];
    omega            =   QuadData['omega'           ];
    xi               =   QuadData['xi'              ];
    epsilon          = TimeParams['epsilon'         ];
    limiters_positv  = TimeParams['limiters_positv' ];
    use_default_iter = PredParams['use_default_iter'];
    custom_iter      = PredParams['custom_iter'     ];

    # *************************************************************************
    # STEP 1: Convert initial data on conserved variables into
    #         initial data on primitive variables.
    # *************************************************************************
    Prim = ConvertConsToPrim(Qinit,
                             numGridCells,
                             numEqns,
                             numOrder,
                             phiAtQuad,
                             omega,
                             AppParams);
    # *************************************************************************

    # *************************************************************************
    # STEP 2: Create some matrices that are the same on each
    #         spacetime element.
    # *************************************************************************
    psiHatAtQuadP,Wstar,Wconstant = CreatePredictionData(numOrder,
                                                         numPredBasis,
                                                         numGridCells,
                                                         numEqns,
                                                         Prim,
                                                         QuadData);
    # *************************************************************************

    # *************************************************************************
    # STEP 3: Picard iteration element-by-element.
    # *************************************************************************
    NumIterations = numOrder+0;
    if use_default_iter==False:
        NumIterations = custom_iter+0;
    numIterations_list = zeros(numGridCells)+NumIterations;

    Wnew = zeros([numEqns*numPredBasis,numGridCells]);
    t_center = time + 0.5*timestep;

    for iter in range(0,NumIterations):

        for i in range(0,numGridCells):
            x_center = xlow + (i+0.5)*spacestep;
            BigTheta = CreateBigTheta(numEqns,
                                      numOrder,
                                      numPredBasis,
                                      t_center,
                                      x_center,
                                      timestep,
                                      spacestep,
                                      psiAtQuadP,
                                      psiderxiAtQuad,
                                      xi,
                                      Wstar[:,i],
                                      source_primitive,
                                      AppParams);

            for m in range(0,numEqns):
                tmp = zeros(numPredBasis);
                for a in range(0,numOrder):
                    for b in range(0,numOrder):
                        tmp += psiHatAtQuadP[:,a,b]*BigTheta[m,a,b];
                Wnew[m*numPredBasis:(m+1)*numPredBasis,i] = \
                    Wconstant[m*numPredBasis:(m+1)*numPredBasis,i] + tmp;

    #for i in range(0,numGridCells):
    #    if troubled_cell[i] == 1:
    #        for m in range(0,numEqns):
    #            Wnew[m*numPredBasis+1:(m+1)*numPredBasis,i] = 0.0;

    for iter in range(0,NumIterations):
        if numPredBasis>1 and limiters_positv==True:
            Wnew = PredictionStep_Positivity_Limiter(Wnew,
                                                     numGridCells,
                                                     numOrder,
                                                     numPredBasis,
                                                     numEqns,
                                                     epsilon,
                                                     AppParams,
                                                     QuadData);

        Wstar = copy(Wnew);
    # *************************************************************************

    # DONE WITH PREDICTION STEP
    return Wnew,numIterations_list;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def CreatePredictionData(numOrder,
                         numPredBasis,
                         numGridCells,
                         numEqns,
                         Prim,
                         QuadData):

    from numpy import array,dot,zeros,sqrt;
    from numpy.linalg import inv;

    sq3 = sqrt(3.0);
    sq5 = sqrt(5.0);
    sq7 = sqrt(7.0);
    Lmat = array([[0.5, 0.0, 0.5*sq3, 0.0, 0.0, 0.5*sq5, 0.0, 0.0, 0.0, 0.5*sq7, 0.0, 0.0, 0.0, 0.0, 1.5],
                  [0.0, 0.5, 0.0, 0.0, 0.5*sq3, 0.0, 0.0, 0.0, 0.5*sq5, 0.0, 0.0, 0.0, 0.0, 0.5*sq7, 0.0],
                  [-0.5*sq3, 0.0, 1.5, 0.0, 0.0, 0.5*sq3*sq5, 0.0, 0.0, 0.0, 0.5*sq3*sq7, 0.0, 0.0, 0.0, 0.0, 1.5*sq3],
                  [0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.5*sq3, 0.0, 0.0, 0.0, 0.0, 0.5*sq5, 0.0, 0.0],
                  [0.0, -0.5*sq3, 0.0, 0.0, 1.5, 0.0, 0.0, 0.0, 0.5*sq3*sq5, 0.0, 0.0, 0.0, 0.0, 0.5*sq3*sq7, 0.0],
                  [0.5*sq5, 0.0, -0.5*sq3*sq5, 0.0, 0.0, 2.5, 0.0, 0.0, 0.0, 0.5*sq5*sq7, 0.0, 0.0, 0.0, 0.0, 1.5*sq5],
                  [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.5*sq3, 0.0, 0.0, 0.0],
                  [0.0, 0.0, 0.0, -0.5*sq3, 0.0, 0.0, 0.0, 1.5, 0.0, 0.0, 0.0, 0.0, 0.5*sq3*sq5, 0.0, 0.0],
                  [0.0, 0.5*sq5, 0.0, 0.0, -0.5*sq3*sq5, 0.0, 0.0, 0.0, 2.5, 0.0, 0.0, 0.0, 0.0, 0.5*sq5*sq7, 0.0],
                  [-0.5*sq7, 0.0, 0.5*sq3*sq7, 0.0, 0.0, -0.5*sq5*sq7, 0.0, 0.0, 0.0, 3.5, 0.0, 0.0, 0.0, 0.0, 1.5*sq7],
                  [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.0],
                  [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.5*sq3, 0.0, 0.0, 0.0, 0.0, 1.5, 0.0, 0.0, 0.0],
                  [0.0, 0.0, 0.0, 0.5*sq5, 0.0, 0.0, 0.0, -0.5*sq3*sq5, 0.0, 0.0, 0.0, 0.0, 2.5, 0.0, 0.0],
                  [0.0, -0.5*sq7, 0.0, 0.0, 0.5*sq3*sq7, 0.0, 0.0, 0.0, -0.5*sq5*sq7, 0.0, 0.0, 0.0, 0.0, 3.5, 0.0],
                  [1.5, 0.0, -1.5*sq3, 0.0, 0.0, 1.5*sq5, 0.0, 0.0, 0.0, -1.5*sq7, 0.0, 0.0, 0.0, 0.0, 4.5]]);

    Tmat = array([[0.5, 0.0, 0.0, 0.0, 0.0],
                  [0.0, 0.5, 0.0, 0.0, 0.0],
                  [-0.5*sq3, 0.0, 0.0, 0.0, 0.0],
                  [0.0, 0.0, 0.5, 0.0, 0.0],
                  [0.0, -0.5*sq3, 0.0, 0.0, 0.0],
                  [0.5*sq5, 0.0, 0.0, 0.0, 0.0],
                  [0.0, 0.0, 0.0, 0.5, 0.0],
                  [0.0, 0.0, -0.5*sq3, 0.0, 0.0],
                  [0.0, 0.5*sq5, 0.0, 0.0, 0.0],
                  [-0.5*sq7, 0.0, 0.0, 0.0, 0.0],
                  [0.0, 0.0, 0.0, 0.0, 0.5],
                  [0.0, 0.0, 0.0, -0.5*sq3, 0.0],
                  [0.0, 0.0, 0.5*sq5, 0.0, 0.0],
                  [0.0, -0.5*sq7, 0.0, 0.0, 0.0],
                  [1.5, 0.0, 0.0, 0.0, 0.0]]);

    Xmat = array([[1.0, 0.0, 0.0, 0.0, 0.0],
                  [0.0, 1.0, 0.0, 0.0, 0.0],
                  [0.0, 0.0, 0.0, 0.0, 0.0],
                  [0.0, 0.0, 1.0, 0.0, 0.0],
                  [0.0, 0.0, 0.0, 0.0, 0.0],
                  [0.0, 0.0, 0.0, 0.0, 0.0],
                  [0.0, 0.0, 0.0, 1.0, 0.0],
                  [0.0, 0.0, 0.0, 0.0, 0.0],
                  [0.0, 0.0, 0.0, 0.0, 0.0],
                  [0.0, 0.0, 0.0, 0.0, 0.0],
                  [0.0, 0.0, 0.0, 0.0, 1.0],
                  [0.0, 0.0, 0.0, 0.0, 0.0],
                  [0.0, 0.0, 0.0, 0.0, 0.0],
                  [0.0, 0.0, 0.0, 0.0, 0.0],
                  [0.0, 0.0, 0.0, 0.0, 0.0]]);

    if numOrder <= 5 and numOrder >= 1:
        Lmat = Lmat[0:numPredBasis,0:numPredBasis]
        Tmat = Tmat[0:numPredBasis,0:numOrder]
        Xmat = Xmat[0:numPredBasis,0:numOrder]
    else:
        print(" ");
        print(" numOrder = %i  is not supported." % numOrder);
        print(" ");
        raise

    Lmat_inv      = inv(Lmat);
    Ymat          = dot(Lmat_inv,Tmat);
    psiHatAtQuadP = zeros([numPredBasis,
                           numOrder,
                           numOrder]);
    for nq1 in range(0,numOrder):
        for nq2 in range(0,numOrder):
            psiHatAtQuadP[:,nq1,nq2] = 0.25*QuadData["omega"][nq1]\
                                           *QuadData["omega"][nq2]\
                              *dot(Lmat_inv,QuadData["psiAtQuadP"][:,nq1,nq2]);

    Wstar = zeros([numPredBasis*numEqns,
                   numGridCells]);

    Wconstant = zeros([numPredBasis*numEqns,
                       numGridCells]);

    for i in range(0,numGridCells):
        for j in range (0,numEqns):
            Wstar[j*numPredBasis:(j+1)*numPredBasis,i] \
                = dot(Xmat,Prim[j,i,:]);

            Wconstant[j*numPredBasis:(j+1)*numPredBasis,i] \
                = dot(Ymat,Prim[j,i,:]);

    return psiHatAtQuadP,Wstar,Wconstant;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def CreateBigTheta(numEqns,
                   numOrder,
                   numPredBasis,
                   t_center,
                   x_center,
                   timestep,
                   spacestep,
                   psiAtQuadP,
                   psiderxiAtQuad,
                   xi,
                   W,
                   source_primitive,
                   AppParams):

    from numpy import zeros;
    from numpy import dot;
    from FluxFunctions import EvalPrimJacobian;

    primvars = zeros([numEqns,numOrder,numOrder]);
    BigTheta = zeros([numEqns,numOrder,numOrder]);

    nu = timestep/spacestep;

    for m in range(0,numEqns):
        for a in range(0,numOrder):
            for b in range(0,numOrder):
                primvars[m,a,b] = dot(psiAtQuadP[:,a,b],\
                                      W[m*numPredBasis:(m+1)*numPredBasis]);

    primJac = -nu*EvalPrimJacobian(numEqns,numOrder,AppParams,primvars);

    for ell in range(0,numEqns):
        for a in range(0,numOrder):
            for b in range(0,numOrder):
                BigTheta[ell,a,b] = 0.5*timestep*\
                      source_primitive[ell](t_center+0.5*timestep*xi[b],\
                                            x_center+0.5*spacestep*xi[a],
                                            AppParams);
                for k in range(0,numEqns):
                    tmp1 = psiderxiAtQuad[:,a,b];
                    tmp2 = W[k*numPredBasis:(k+1)*numPredBasis];
                    BigTheta[ell,a,b] += primJac[ell,k,a,b]*dot(tmp1,tmp2);

    return BigTheta;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def correctionStep(DimParams,
                   MeshParams,
                   TimeParams,
                   AppParams,
                   QuadData,
                   source_conservative,
                   timestep,
                   t,
                   Q,  # initial condition
                   W): # predicted solution

    from numpy import copy,zeros,isnan,ones,min,max;
    from FluxFunctions import FluxFunction,NumericalFlux,SourceFunction;
    from FluxFunctions import NumericalFlux_LxF;
    from LimiterFunctions import LimitAverageFlux;
    from LimiterFunctions import CorrectionStep_Positivity_Limiter;
    from LimiterFunctions import CorrectionStep_Moment_Limiter;
    from legendre_poly import phi;
    import numpy as np

    boundary        = MeshParams['boundary'       ];
    numGridCells    = MeshParams['numGridCells'   ];
    xlow            = MeshParams['xlow'           ];
    spacestep       = MeshParams['spacestep'      ];
    numOrder        =  DimParams['numOrder'       ];
    numEqns         =  DimParams['numEqns'        ];
    numPredBasis    =  DimParams['numPredBasis'   ];
    xi              =   QuadData['xi'             ];
    omega           =   QuadData['omega'          ];
    phiAtQuad       =   QuadData['phiAtQuad'      ];
    phiDerAtQuadP   =   QuadData['phiDerAtQuadP'  ];
    psiAtQuadP      =   QuadData['psiAtQuadP'     ];
    num_phiAtPosPts =   QuadData['num_phiAtPosPts'];
    phiAtPosPts     =   QuadData['phiAtPosPts'    ];
    num_phiAtLimPts =   QuadData['num_phiAtLimPts'];
    phiAtLimPts     =   QuadData['phiAtLimPts'    ];
    limiters_minmod = TimeParams['limiters_minmod'];
    limiters_positv = TimeParams['limiters_positv'];
    epsilon         = TimeParams['epsilon'        ];

    NumFlux     = zeros([numGridCells+1,numEqns]);
    NumFlux_LxF = zeros([numGridCells+1,numEqns]);
    BigLambda   = ones(numGridCells+1);
    if limiters_minmod == False or numOrder==1:
        troubled_cell = zeros(numGridCells,dtype=int);
    speed_MaxGlobal = 0.0;

    nu = (timestep/spacestep);
    dx_over_dt = (spacestep/timestep);

    # ------------------------
    # Compute numerical fluxes
    # ------------------------
    if limiters_positv==True:
        for i in range(0,numGridCells+1):

            # first-order Lax-Friedrichs fluxes
            NumFlux_LxF[i,:] = NumericalFlux_LxF(i-1,
                                                 Q,
                                                 numGridCells,
                                                 numEqns,
                                                 boundary,
                                                 AppParams);

    for i in range(0,numGridCells+1):

        for tauIndex in range(0,numOrder):
            # high-order numerical flux
            Fluxi,speed_Max = NumericalFlux(i-1,
                                            W,
                                            tauIndex,
                                            numGridCells,
                                            numEqns,
                                            numPredBasis,
                                            boundary,
                                            AppParams,
                                            QuadData);
            if isnan(speed_Max)==True:
                print( "Error: speed_max is not a number" );
                raise;
            NumFlux[i,:] += 0.5*Fluxi*omega[tauIndex];
            speed_MaxGlobal = max([speed_MaxGlobal,speed_Max]);

    # -------------------------
    # Limiter for cell averages
    # -------------------------
    if numOrder>1 and limiters_positv==True:

        Qbar = copy(Q[:,:,0]); # Averge state for each variable
        dFlux = NumFlux_LxF - NumFlux;

        for i in range(0,numGridCells):

            # Lax-Friedrichs update (using only averages)
            Q_LxF = Qbar[:,i] - nu*(NumFlux_LxF[i+1,:] - NumFlux_LxF[i,:]);

            # Find flux limiting values on edges i and i+1
            Lambda_lft,Lambda_rgt = LimitAverageFlux(nu,
                                                     Qbar[:,i],
                                                     Q_LxF,
                                                     -dFlux[i,:],
                                                     dFlux[i+1,:],
                                                     NumFlux[i,:],
                                                     NumFlux[i+1,:],
                                                     AppParams,
                                                     epsilon);
            BigLambda[i]   = min([BigLambda[i],  Lambda_lft]);
            BigLambda[i+1] = min([BigLambda[i+1],Lambda_rgt]);

    # ----------------
    # Numerical update
    # ----------------
    CFL_actual = speed_MaxGlobal*nu;
    for i in range(0,numGridCells):
        x_i = float(xlow)+(float(i)+0.5)*spacestep;
        t_i = t + 0.5*timestep;

        for j in range (0,numOrder):
            doubleint = zeros(numEqns);
            source = zeros(numEqns);

            for xiIndex in range (0,numOrder):
                for tauIndex in range (0,numOrder):
                    doubleint += phiDerAtQuadP[j,xiIndex]\
                                *FluxFunction(W,
                                              i,
                                              tauIndex,
                                              xiIndex,
                                              numPredBasis,
                                              psiAtQuadP,
                                              AppParams)\
                                *omega[xiIndex]*omega[tauIndex];
                    source += phiAtQuad[j,xiIndex]\
                                *SourceFunction(t_i,
                                                x_i,
                                                timestep,
                                                spacestep,
                                                tauIndex,
                                                xiIndex,
                                                xi,
                                                source_conservative,
                                                AppParams)\
                                *omega[xiIndex]*omega[tauIndex];
            doubleint *= 0.5*nu;
            doubleint -= nu*(phi(j,1.0)*NumFlux[i+1,:] - phi(j,-1.0)*NumFlux[i,:]);
            Q[:,i,j] += doubleint + 0.25*timestep*source;

    # --------
    # Limiters
    # --------
    if numOrder>1:

        if limiters_positv==True:
            for i in range(0,numGridCells):
                theta = BigLambda[i];
                F_lft = theta*NumFlux[i,:] + (1.0-theta)*NumFlux_LxF[i,:];

                theta = BigLambda[i+1];
                F_rgt = theta*NumFlux[i+1,:] + (1.0-theta)*NumFlux_LxF[i+1,:];

                Q[:,i,0] = Qbar[:,i] - nu*(F_rgt - F_lft);

        if limiters_minmod==True:
            Q = CorrectionStep_Moment_Limiter(Q,
                                              numOrder,
                                              numEqns,
                                              MeshParams,
                                              AppParams,
                                              epsilon,
                                              num_phiAtLimPts,
                                              phiAtLimPts);

        if limiters_positv==True:
            Q = CorrectionStep_Positivity_Limiter(Q,
                                                  numGridCells,
                                                  numOrder,
                                                  numEqns,
                                                  epsilon,
                                                  AppParams,
                                                  num_phiAtPosPts,
                                                  phiAtPosPts);

    #troubled_cell_copy = np.copy(troubled_cell);
    #for i in range(0,numGridCells):
    #    if troubled_cell[i] == 1:
    #        if i == 0:
    #            troubled_cell_copy[i+1] = 1;
    #        if i == numGridCells-1:
    #            troubled_cell_copy[i-1] = 1;
    #        if i > 0 and i < numGridCells-1:
    #            troubled_cell_copy[i+1] = 1;
    #            troubled_cell_copy[i-1] = 1;

    return Q,CFL_actual,speed_MaxGlobal;
#-------------------------------------------------------------------------------

#---------------------------------- Output file methods ------------------------
def createPlotHelper(outputDir,
                     numFrames,
                     numOrder,
                     numGridCells,
                     xlow,
                     xhigh,
                     numEqns,
                     timePerFrame):
    import os;

    if not os.path.exists(outputDir):
        os.makedirs(outputDir);
    file = open(outputDir+ '/plot_helper.txt','w');
    file.write('%d' %numFrames + '\n'+'%d' %numOrder + '\n' \
        +'%d' %numGridCells + '\n' +'%24.15e' %xlow + '\n'+ '%24.15e' %xhigh \
        + '\n' + '%d' %numEqns + '\n' + '%24.15e' %timePerFrame + '\n');
    file.close();
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def outputQatEndOfFrame(Q,
                        fileNum,
                        numEqns,
                        numGridCells,
                        numOrder,
                        outputDir):

    filename = outputDir+ "/out" + str(fileNum)+".txt";
    file = open(filename,'w');
    for k in range (0,numEqns):
        for i in range (0,numGridCells):
            for j in range (0, numOrder):
                file.write("%24.16e "%Q[k,i,j]);
            file.write('\n');
    file.close();
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def outputNumIterations(numGridCells,
                        numIterations,
                        outputDir,
                        fileNum):

    filename = outputDir + "/outiterations" + str(fileNum) + ".txt";
    file = open(filename, 'w');
    for n in range (0,numGridCells):
        file.write('%d' %numIterations[n] + " ");
    file.close();
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def printWelcome():

    print("");
    print("  # -------------------------------------------------------------- #");
    print("  #  **************  Pythonic Lax-Wendroff Method  **************  #");
    print("  # -------------------------------------------------------------- #");
    print("  #  Developed during the Summer 2017 National Science Foundation  #");
    print("  #  (NSF) Research Experience for Undergraduates (REU) Program    #");
    print("  #  in Mathematics at Iowa State University (ISU) in Ames, Iowa.  #");
    print("  # -------------------------------------------------------------- #");
    print("  #  Research was funded by NSF grant DMS-1457443.                 #");
    print("  # -------------------------------------------------------------- #");
    print("  #  See:  https://orion.math.iastate.edu/reu                      #");
    print("  # -------------------------------------------------------------- #");
    print("  #  The code developers (in alphabetical order) are               #");
    print("  #      * Camille Felton    (REU Participant)                     #");
    print("  #      * Mariana Harris    (REU Participant)                     #");
    print("  #      * Caleb Logemann    (Graduate Student Mentor)             #");
    print("  #      * Stefan Nelson     (REU Participant)                     #");
    print("  #      * Ian Pelakh        (REU Participant)                     #");
    print("  #      * James Rossmanith  (Faculty Mentor)                      #");
    print("  # -------------------------------------------------------------- #");
    print("  #  This code is part of the following paper:                     #");
    print("  #      C. Felton, M. Harris, C. Logemann, S. Nelson,             #");
    print("  #      I. Pelakh, and J.A. Rossmanith. A Limiting Strategy for   #");
    print("  #      Locally-Implicit Lax-Wendroff Discontinuous Galerkin      #");
    print("  #      Methods, 2018. Submitted (https://arxiv.org/abs/?).       #");                               #");
    print("  # -------------------------------------------------------------- #");
#-------------------------------------------------------------------------------
