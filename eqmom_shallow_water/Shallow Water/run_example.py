#-------------------------------------------------------------------------------
# Vlasov Poisson PROBLEM EXAMPLE
#-------------------------------------------------------------------------------
def mynormal(x,xcenter,std):
    from numpy import sqrt,exp,pi
    return (1.0/sqrt(2.0*pi*std))*exp(-((x-xcenter)**2)/(2.0*std));

#def myPDF(x,alpha,k):
#    from numpy import cos,sqrt,pi
#    return (1+alpha*cos(k*x))*(1/sqrt(2*pi))*exp()

#------------------- Initial Conditions: Primitive variables -------------------
###For initial conditions laid out in Schaerer paper section 6.1
#def rho(x,AppParams):
#    return 1.0 + 4.7*mynormal(x,0.0,122.7);
#def u(x,AppParams):
#    return 0.0*x;
#def p(x,AppParams):
#    return 1.0 + 4.7*mynormal(x,0.0,122.7);
#def q(x,AppParams):
#    return 0.0*x;
#def rstar(x,AppParams):
#    return 2.0*(1.0 + 4.7*mynormal(x,0.0,122.7));
#def s(x,AppParams):
#    return 0.0*x;

###For initial conditions laid out in Landau example of Seal_PhD
#def rho(x,AppParams):
#    from numpy import cos
#    return 1.0 - 0.5*cos(0.5*x);
#def u(x,AppParams):
#    return 0.0*x;
#def p(x,AppParams):
#    from numpy import cos
#    return 3.0 + 1.5*cos(0.5*x);
#def q(x,AppParams):
#    return 0.0*x;
#def rstar(x,AppParams):
#    from numpy import cos
#    return 6.0-3.0*cos(0.5*x);


###For initial conditions for Method of Manufactured Solutions

#-------------------------------------------------------------------------------

#------------------- Initial Conditions: Primitive variables -------------------
def h(x,AppParams):
    from numpy import sin,pi;
    return 1.0 + 0.5*sin(pi*x);
def u(x,AppParams):
    from numpy import cos,pi;
    return cos(2.0*pi*x);
#-------------------------------------------------------------------------------

#------------------- Initial Conditions: Conserved variables -------------------
def q0(x,AppParams):
    return h(x,AppParams);
def q1(x,AppParams):
    return h(x,AppParams)*u(x,AppParams);
#-------------------------------------------------------------------------------

#---------------------------- Source Term Functions ----------------------------
def s0(t,x,primvars,AppParams):
    from numpy import cos,sin,pi;
    return -2.0*pi*(0.5*sin(pi*(x-t)) + 1.0)*sin(pi*(2.0*x-4.0*t))\
           + 0.5*pi*cos(pi*(2.0*x-4.0*t))*cos(pi*(x-t)) - 0.5*pi*cos(pi*(x-t));
def s1(t,x,primvars,AppParams):
    from numpy import cos,sin,pi;
    return 0.5*pi*(0.25*sin(pi*(x-t)) + 0.5)*cos(pi*(x-t))\
           - 4.0*pi*(0.5*sin(pi*(x-t)) + 1.0)*sin(pi*(2.0*x-4.0*t))\
           *cos(pi*(2.0*x-4.0*t)) + 4.0*pi*(0.5*sin(pi*(x-t)) + 1.0)\
           *sin(pi*(2.0*x-4.0*t)) + 0.25*pi*(0.5*sin(pi*(x-t)) + 1.0)\
           *cos(pi*(x-t)) + 0.5*pi*cos(pi*(2.0*x-4.0*t))**2\
           *cos(pi*(x-t)) - 0.5*pi*cos(pi*(2.0*x-4.0*t))*cos(pi*(x-t));
#-------------------------------------------------------------------------------

#---------------------------- Primitive Equations Source Term Functions --------
####Source term is of the form: ell*M^(ell-1)*E
def s0_prim(t,x,primvars,AppParams):
    return s0(t,x,primvars,AppParams)
def s1_prim(t,x,primvars,AppParams):
    return s1(t,x,primvars,AppParams)

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
if __name__ == "__main__":

    # -----------------------------
    # GRAB SOME IMPORTANT FUNCTIONS
    # -----------------------------
    # parameter function (this sits in the current local directory)
    from parameters import parameters_init;
    # set path to main library for DG code
    import sys; sys.path.insert(0,'../../lib');
    # main Lax-Wendroff code (this sits in the main library directory)
    from LaxWendroffDG import LaxWendDG;

    # --------------
    # SET PARAMETERS
    # --------------
    from os import system;
    output_dir = "output";
    system("rm -f -r " + output_dir);
    system("mkdir " + output_dir);
    AppParams,DimParams,MeshParams,TimeParams,PredParams \
        = parameters_init(output_dir);

    # --------------------------------
    # QUICK SANITY CHECK OF PARAMETERS
    # --------------------------------
    #assert(DimParams['numEqns']==5);

    # ------------------------------
    # EXECUTE LAX-WENDROFF DG METHOD
    # ------------------------------
    Q = LaxWendDG([q0,q1],          # initial condition function
                  [s0,s1],          # source term function for conservative vars
                  [s0_prim,s1_prim],       # source term function for primitive vars
                  AppParams,                 # application-specific parameters
                  DimParams,                 # parameters determining dimensions of arrays
                  MeshParams,                # mesh-specific parameters
                  TimeParams,                # time-stepping-specific parameters
                  PredParams,                # prediction-step-specific parameters
                  output_dir);               # directory to where output is written

#-------------------------------------------------------------------------------
