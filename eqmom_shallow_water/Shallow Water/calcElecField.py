
def calc_ElecField(numGridCells,numPredBasis,numOrder,xlow,xhigh,phiAtQuad,Q,AppParams,t,timestep):

    from numpy import pi,cos,linspace,zeros,sin,sqrt,log2
    from PoissonSolver import poisson_solve
    from electric_field import deriv_electricField
    import sys; sys.path.insert(0,'../../lib');
    from basis_data import computeCoefficients;

    E_field = zeros((numPredBasis,numGridCells));
    P = zeros((numGridCells,numOrder));
    spacestep = (xhigh-xlow)/numGridCells;
    order = numOrder;
    rho0 = sqrt(pi);  ###Changes with each example!!!!!
    for i in range(numGridCells):
        P[i,:] = Q[0,i,:];
        P[i,0] -= rho0;


    E = poisson_solve(numGridCells,order,xlow,xhigh,P,t,timestep)

    dE,ddE,dddE = deriv_electricField(numGridCells,E,Q,spacestep,numOrder,xlow,xhigh,t,AppParams)

    #dE = zeros([numGridCells,numOrder])
    #ddE = zeros([numGridCells,numOrder])
    #x = xlow - 0.5*spacestep
    #for k in range(0,numGridCells):
    #    x += spacestep
    #    dE[k,0] = (pi**1.5*cos(2.0*pi*t - 2.0*x)*sin(spacestep))/(2.0*spacestep)
    #    dE[k,1] = -(sqrt(3.0)*pi**1.5*(spacestep*cos(spacestep) - sin(spacestep))*sin(2.0*pi*t - 2.0*x))/(2.0*spacestep**2)
    #    dE[k,2] = (sqrt(5.0)*pi**1.5*cos(2.0*pi*t - 2.0*x)*(3.0*spacestep*cos(spacestep) \
    #                + (-3.0 + spacestep**2)*sin(spacestep)))/(2.0*spacestep**3)
    #    ddE[k,0] = -((pi**2.5*sin(spacestep)*sin(2.0*pi*t - 2.0*x))/spacestep)
    #    ddE[k,1] = (sqrt(3.0)*pi**2.5*cos(2.0*pi*t - 2.0*x)*(-(spacestep*cos(spacestep)) + sin(spacestep)))/spacestep**2
    #    ddE[k,2] = -((sqrt(5.0)*pi**2.5*(3.0*spacestep*cos(spacestep) \
    #                + (-3.0 + spacestep**2)*sin(spacestep))*sin(2.0*pi*t - 2.0*x))/spacestep**3)

    if numOrder==1:
        for i in range(0,numGridCells):
            E_field[0,i] = E[i,0];

    elif numOrder==2:
        for i in range(0,numGridCells):
           E_field[0,i] = E[i,0]+timestep*0.5*dE[i,0]
           E_field[1,i] = E[i,1]+timestep*0.5*dE[i,1]
           E_field[2,i] = (timestep*dE[i,0])/(2.0*sqrt(3.0))

    elif numOrder==3:
        for i in range(0,numGridCells):
           E_field[0,i] = E[i,0]+timestep*0.5*dE[i,0] + (timestep**2/6.0)*ddE[i,0];

           E_field[1,i] = E[i,1]+timestep*0.5*dE[i,1] + (timestep**2/6.0)*ddE[i,1];

           E_field[2,i] = (timestep*dE[i,0])/(2.0*sqrt(3.0)) + (timestep**2*ddE[i,0])/(4.0*sqrt(3.0));

           E_field[3,i] = E[i,2] + (1.0/6.0)*timestep*(3.0*dE[i,2]+timestep*ddE[i,2]);

           E_field[4,i] = (timestep*dE[i,1])/(2.0*sqrt(3.0)) + (timestep**2*ddE[i,1])/(4.0*sqrt(3.0));

           E_field[5,i] = (timestep**2*ddE[i,0])/(12.0*sqrt(5.0));

    elif numOrder==4:
        for i in range(0,numGridCells):
           E_field[0,i] = E[i,0]+timestep*0.5*dE[i,0] + (timestep**2/6.0)*ddE[i,0]\
                        + (timestep**3/24.0)*dddE[i,0];
           E_field[1,i] = E[i,1]+timestep*0.5*dE[i,1] + (timestep**2/6.0)*ddE[i,1]\
                        + (timestep**3/24.0)*dddE[i,1];
           E_field[2,i] = (timestep*dE[i,0])/(2.0*sqrt(3.0)) + (timestep**2*ddE[i,0])/(4.0*sqrt(3.0)) + \
                        -  (sqrt(3.0)*timestep**3*dddE[i,0])/40.0;
           E_field[3,i] = E[i,2] + (1.0/24.0)*timestep*(12.0*dE[i,2]+timestep* \
                        (4.0*ddE[i,2]+timestep*dddE[i,2]));
           E_field[4,i] = (timestep*dE[i,1])/(2.0*sqrt(3.0)) + (timestep**2*ddE[i,1])/(4.0*sqrt(3.0)) + \
                        -  (sqrt(3.0)*timestep**3*dddE[i,1])/40.0;
           E_field[5,i] = (timestep**2*ddE[i,0])/(12.0*sqrt(5.0)) + (timestep**3*dddE[i,0])/(24.0*sqrt(5.0));
           E_field[6,i] = E[i,3] + (timestep*(12.0*dE[i,3] + timestep*(4.0*ddE[i,3] + timestep*dddE[i,3])))/24.0;
           E_field[7,i] = (timestep*(20.0*dE[i,2] + timestep*(10.0*ddE[i,2] + 3.0*timestep*dddE[i,2])))/(40.0*sqrt(3.0));
           E_field[8,i] = (timestep**2*ddE[i,1])/(12.0*sqrt(5.0)) + (timestep**3*dddE[i,1])/(24.0*sqrt(5.0));
           E_field[9,i] = (timestep**3*dddE[i,0])/(120.0*sqrt(7.0));


    return(E_field)
