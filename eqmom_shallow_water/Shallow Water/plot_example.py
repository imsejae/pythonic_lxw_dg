#----------------------------------------------------------------------------------
# plot_example.py receive .txt files "output/out*.txt" with the solution
# obtained with the Lax Wendrof DG with limiters method.
#----------------------------------------------------------------------------------
def h_ex(x,t):
    from numpy import sin,pi;
    return 1.0 + 0.5*sin(pi*(x-t));

def u_ex(x,t):
    from numpy import cos,pi;
    return cos(2.0*pi*(x-2.0*t));


#-------------------------------------------------------------------------------
def plot(x,
         Q,
         E, ###Added to plot electric field
         xlow,
         xhigh,
         variables,
         t,
         AppParams):

    import numpy as np
    import matplotlib
    import matplotlib.pylab as plt
    matplotlib.rcParams.update({'font.size': 16, 'font.family': 'serif'})



    numVariables = len(variables)
    y = np.zeros([len(x),numVariables])
    yElec = np.zeros(len(x))
    for i in range(0, len(x)):
        y[i,0] = Plot.findy(x[i],Q[0,:,:], xlow, xhigh)
        y[i,1] = Plot.findy(x[i],Q[1,:,:], xlow, xhigh)/y[i,0]


    h_error = max(abs(y[:,0]-h_ex(x,t)))
    u_error = max(abs(y[:,1]-u_ex(x,t)))
    error = max(h_error,u_error)
    print("h_error is", h_error)
    print("u_error is", u_error)
    print("error is", error)


    xticks = np.linspace(xlow,xhigh,9);

    plt.figure(1)
    plt.clf()
    plt.grid()
    plt.plot(x,y[:,0], 'o', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,h_ex(x,t), '-', label = "analytical", linewidth = 2.0, color='r')
    plt.title(r'Height: $h(t,x)$ at time $t=$' +str(t))
    plt.legend(prop={'size':14}, loc =  'upper left')
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    plt.xticks(xticks)

    plt.figure(2)
    plt.clf()
    plt.grid()
    plt.plot(x,y[:,1], 'o', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,u_ex(x,t), '-', label = "analytical", linewidth = 2.0, color='r')
    plt.title(r'Velocity: $u(t,x)$ at time $t=$' +str(t))
    plt.legend(prop={'size':14}, loc =  'upper left')
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    plt.xticks(xticks)



    plt.draw()
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
if __name__== '__main__':

    # -----------------------------
    # GRAB SOME IMPORTANT FUNCTIONS
    # -----------------------------
    # parameter function (this sits in the current local directory)
    from parameters import parameters_init;
    # set path to main library for DG code
    import sys; sys.path.insert(0,'../../lib');

    # --------------
    # SET PARAMETERS
    # --------------
    output_dir = "output";
    AppParams,DimParams,MeshParams,TimeParams,PredParams \
        = parameters_init(output_dir);

    # -------------
    # PLOT SOLUTION
    # -------------
    import Plot
    Plot.plotFrames(output_dir,AppParams,plot);

#-------------------------------------------------------------------------------
