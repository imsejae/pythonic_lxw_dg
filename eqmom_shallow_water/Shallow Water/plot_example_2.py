#----------------------------------------------------------------------------------
# plot_example.py receive .txt files "output/out*.txt" with the solution
# obtained with the Lax Wendrof DG with limiters method.
#----------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def plot(x1,
         x2,
         Q1,
         Q2,
         xlow,
         xhigh,
         variables,
         t,
         AppParams):

    import numpy as np
    import matplotlib
    import matplotlib.pylab as plt
    #import exact_riemann_solution as exact
    matplotlib.rcParams.update({'font.size': 16, 'font.family': 'serif'})

    gamma   = 3.0; #AppParams['gamma' ]
    rhol    = AppParams['rhol'  ];
    rhor    = AppParams['rhor'  ];
    ul      = AppParams['ul'    ];
    ur      = AppParams['ur'    ];
    pl      = AppParams['pl'    ];
    pr      = AppParams['pr'    ];
    ql      = AppParams['ql'    ];
    qr      = AppParams['qr'    ];
    rstarl  = AppParams['rstarl'];
    rstarr  = AppParams['rstarr'];
    xshock  = AppParams['xshock'];

    numVariables = len(variables)
    y1 = np.zeros([len(x1),numVariables+1])
    for i in range(0, len(x1)):
        y1[i,0] = Plot.findy(x1[i],Q1[0,:,:], xlow, xhigh)
        y1[i,1] = Plot.findy(x1[i],Q1[1,:,:], xlow, xhigh)/y1[i,0]
        y1[i,2] = Plot.findy(x1[i],Q1[2,:,:], xlow, xhigh)-y1[i,0]*y1[i,1]**2
        y1[i,3] = Plot.findy(x1[i],Q1[3,:,:], xlow, xhigh) - y1[i,0]*y1[i,1]**3 - 3*y1[i,2]*y1[i,1]
        y1[i,4] = Plot.findy(x1[i],Q1[4,:,:], xlow, xhigh) - y1[i,0]*y1[i,1]**4 - 6*y1[i,2]*y1[i,1]**2 \
        - 4*y1[i,3]*y1[i,1] - y1[i,2]**2/y1[i,0] - y1[i,3]**2/y1[i,2]
        y1[i,5] = Plot.findy(x1[i],Q1[4,:,:], xlow, xhigh) - y1[i,0]*y1[i,1]**4 - 6*y1[i,2]*y1[i,1]**2 \
        - 4*y1[i,3]*y1[i,1]

    y2 = np.zeros([len(x2),numVariables+1])
    for i in range(0, len(x2)):
        y2[i,0] = Plot.findy(x2[i],Q2[0,:,:], xlow, xhigh)
        y2[i,1] = Plot.findy(x2[i],Q2[1,:,:], xlow, xhigh)/y2[i,0]
        y2[i,2] = Plot.findy(x2[i],Q2[2,:,:], xlow, xhigh)-y2[i,0]*y2[i,1]**2
        y2[i,3] = Plot.findy(x2[i],Q2[3,:,:], xlow, xhigh) - y2[i,0]*y2[i,1]**3 - 3*y2[i,2]*y2[i,1]
        y2[i,4] = Plot.findy(x2[i],Q2[4,:,:], xlow, xhigh) - y2[i,0]*y2[i,1]**4 - 6*y2[i,2]*y2[i,1]**2 \
        - 4*y2[i,3]*y2[i,1] - y2[i,2]**2/y2[i,0] - y2[i,3]**2/y2[i,2]
        y2[i,5] = Plot.findy(x2[i],Q2[4,:,:], xlow, xhigh) - y2[i,0]*y2[i,1]**4 - 6*y2[i,2]*y2[i,1]**2 \
        - 4*y2[i,3]*y2[i,1]

    #rho_exact, u_exact, p_exact = \
    #    exact.exact_riemann_solution(xshock,rhol,ul,pl,rhor,ur,pr,x,t,gamma);

    xticks = np.linspace(xlow,xhigh,9);

    plt.figure(1)
    plt.clf()
    plt.grid()
    ylow  =  0.0;
    yhigh =  1.6;
    yticks = np.linspace(ylow,yhigh,5);
    #plt.gca().set_aspect(((xhigh-xlow)/(yhigh-ylow))*0.5)
    plt.plot(x1,y1[:,0], 'o', label = "200", linewidth = 2.0, color='b')
    plt.plot(x2,y2[:,0], '-', label = "800", linewidth = 2.0, color='r')
    plt.title(r'Density: $\rho(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    #plt.ylim(ylow,yhigh)
    plt.xticks(xticks)
    #plt.yticks(yticks)
    plt.legend(prop={'size':14}, loc =  'lower left')
    plt.savefig("num_euler_shocktube_density.pdf", format='pdf')

    plt.figure(2)
    plt.clf()
    plt.grid()
    ylow  = -0.55;
    yhigh =  -0.25;
    yticks = np.linspace(ylow,yhigh,5);
    #plt.gca().set_aspect(((xhigh-xlow)/(yhigh-ylow))*0.5)
    plt.plot(x1,y1[:,1], 'o', label = "200", linewidth = 2.0, color='b')
    plt.plot(x2,y2[:,1], '-', label = "800", linewidth = 2.0, color='r')
    plt.title(r'Velocity: $u(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    #plt.ylim(ylow,yhigh)
    plt.xticks(xticks)
    #plt.yticks(yticks)
    plt.legend(prop={'size':14}, loc =  'upper left')
    plt.savefig("num_euler_shocktube_velocity.pdf", format='pdf')

    plt.figure(3)
    plt.clf()
    plt.grid()
    ylow  =  0.0;
    yhigh =  1.5;
    yticks = np.linspace(ylow,yhigh,5);
    #plt.gca().set_aspect(((xhigh-xlow)/(yhigh-ylow))*0.5)
    plt.plot(x1,y1[:,2], 'o', label = "200", linewidth = 2.0, color='b')
    plt.plot(x2,y2[:,2], '-', label = "800", linewidth = 2.0, color='r')
    plt.title(r'Pressure: $p(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    #plt.ylim(ylow,yhigh)
    plt.xticks(xticks)
    #plt.yticks(yticks)
    plt.legend(prop={'size':14}, loc =  'lower left')
    plt.savefig("num_euler_shocktube_pressure.pdf", format='pdf')

    plt.figure(4)
    plt.clf()
    plt.grid()
    ylow  =  0.0;
    yhigh =  1.5;
    yticks = np.linspace(ylow,yhigh,5);
    #plt.gca().set_aspect(((xhigh-xlow)/(yhigh-ylow))*0.5)
    plt.plot(x1,y1[:,3], 'o', label = "200", linewidth = 2.0, color='b')
    plt.plot(x2,y2[:,3], '-', label = "800", linewidth = 2.0, color='r')
    plt.title(r'Heat Flux: $q(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    #plt.ylim(ylow,yhigh)
    plt.xticks(xticks)
    #plt.yticks(yticks)
    plt.legend(prop={'size':14}, loc =  'upper right')
    plt.savefig("num_euler_shocktube_heat_flux.pdf", format='pdf')

    plt.figure(5)
    plt.clf()
    plt.grid()
    ylow  =  0.0;
    yhigh =  1.5;
    yticks = np.linspace(ylow,yhigh,5);
    #plt.gca().set_aspect(((xhigh-xlow)/(yhigh-ylow))*0.5)
    plt.plot(x1,y1[:,4], 'o', label = "200", linewidth = 2.0, color='b')
    plt.plot(x2,y2[:,4], '-', label = "800", linewidth = 2.0, color='r')
    #plt.title(r'rstar: $rstar(t,x)$ at time $t=$' +str(t))
    plt.title(r'k: $k(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    #plt.ylim(ylow,yhigh)
    plt.xticks(xticks)
    #plt.yticks(yticks)
    plt.legend(prop={'size':14}, loc =  'lower left')
    plt.savefig("num_euler_shocktube_rstar.pdf", format='pdf')

    plt.figure(6)
    plt.clf()
    plt.grid()
    ylow  =  0.0;
    yhigh =  1.5;
    yticks = np.linspace(ylow,yhigh,5);
    #plt.gca().set_aspect(((xhigh-xlow)/(yhigh-ylow))*0.5)
    plt.plot(x1,y1[:,5], 'o', label = "200", linewidth = 2.0, color='b')
    plt.plot(x2,y2[:,5], '-', label = "800", linewidth = 2.0, color='r')
    #plt.title(r'rstar: $rstar(t,x)$ at time $t=$' +str(t))
    plt.title(r'r: $r(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    #plt.ylim(ylow,yhigh)
    plt.xticks(xticks)
    #plt.yticks(yticks)
    plt.legend(prop={'size':14}, loc =  'lower left')
    plt.savefig("num_euler_shocktube_r.pdf", format='pdf')

    plt.draw()
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
if __name__== '__main__':

    # -----------------------------
    # GRAB SOME IMPORTANT FUNCTIONS
    # -----------------------------
    # parameter function (this sits in the current local directory)
    from parameters import parameters_init;
    # set path to main library for DG code
    import sys; sys.path.insert(0,'../../lib');

    # --------------
    # SET PARAMETERS
    # --------------
    output_dir1 = "output3";
    output_dir2 = "output4";
    AppParams,DimParams1,MeshParams1,TimeParams1,PredParams1 \
        = parameters_init(output_dir1);
    AppParams,DimParams2,MeshParams2,TimeParams2,PredParams2 \
        = parameters_init(output_dir2);

    # -------------
    # PLOT SOLUTION
    # -------------
    import Plot
    #Plot.plotFrames(output_dir1,AppParams1,plot1);
    #Plot.plotFrames(output_dir2,AppParams2,plot2);

    Plot.plotFrames_comparison(output_dir1,output_dir2,AppParams,plot)

#-------------------------------------------------------------------------------
