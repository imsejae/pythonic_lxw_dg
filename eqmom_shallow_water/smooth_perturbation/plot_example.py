#----------------------------------------------------------------------------------
# plot_example.py receive .txt files "output/out*.txt" with the solution
# obtained with the Lax Wendrof DG with limiters method.
#----------------------------------------------------------------------------------
def rho_ex(x,t):
    from numpy import exp,pi,sqrt
    return(1.0 + (47.0*exp(-((5.0*x**2)/(1227.0+10.0*t**2))))/(2.0* sqrt(5.0*pi)*sqrt(1227.0+10.0*t**2)))

def u_ex(x,t):
    from numpy import exp,pi,sqrt
    return((47.0 *exp(-((5.0 *x**2)/(1227.0+10.0*t**2))) *sqrt(5.0/pi)*t*x)/ \
    (sqrt(1227.0+10.0*t**2))**3)

def p_ex(x,t):
    from numpy import exp,pi,sqrt
    #return(1.0/(exp(-((5.0/1227.0)+10.0/(1227.0+10.0*t**2))*x**2)*(10.0*exp((5.0/1227.0)+10.0/(1227.0+10.0*t**2)*x**2) \
    #*(sqrt(pi))**3*(sqrt(1227.0+10.0*t**2))**7+519115.0*sqrt(5.0)*exp((50.0*t**2*x**2)/(1227.0*(1227.0+10.0*t**2)))*t**2*x**2 \
    #- 110450.0*exp((5.0*x**2)/1227.0)*sqrt(pi)*t**2*sqrt(1227.0+10.0*t**2)*x**2 + 47.0*sqrt(5.0)*exp((10.0*(1227.0+5.0*t**2)*x**2) \
    #/(1227.0*(1227.0+10.0*t**2)))*pi*(1227.0+10.0*t**2)*(1505529.0+10.0*t**2*(1227.0+10.0*x**2))))/ (10.0*(sqrt(pi))**3 \
    #*(sqrt(1227.0+10.0*t**2))**7))

    return((10*exp((0.004074979625101874 + 10.0/(1227.0 + 10.0*t**2))*x**2)*pi**1.5*(1227.0 + 10.0*t**2)**3.5 + \
         519115.0*sqrt(5)*exp((50.0*t**2*x**2)/(1227.0*(1227.0 + 10.0*t**2)))*t**2*x**2 - \
         110450.0*exp((5.0*x**2)/1227.0)*sqrt(pi)*t**2*sqrt(1227.0 + 10.0*t**2)*x**2 + \
         47.0*sqrt(5.0)*exp((10.0*(1227.0 + 5.0*t**2)*x**2)/(1227.0*(1227.0 + 10.0*t**2)))*pi*(1227.0 + 10.0*t**2)* \
          (1505529.0 + 10.0*t**2*(1227.0 + 10.0*x**2)))/ \
       (10.0*exp((0.004074979625101874 + 10.0/(1227.0 + 10.0*t**2))*x**2)*pi**1.5*(1227.0 + 10.0*t**2)**3.5))

def q_ex(x,t):
    from numpy import exp,pi,sqrt
    return(-1.0/(2.0*pi**2*(1227.0+10.0*t**2)**5) * 47.0*exp(-((5.0/1227.0)+15.0/(1227.0+10.0*t**2))*x**2) \
    *t*x*(519115.0*exp(50.0*t**2*x**2/(1227.0*(1227.0+10.0*t**2)))*t**2*x**2-44180.0*exp(5.0*x**2/1227.0) \
    *sqrt(5.0*pi)*t**2*sqrt(1227.0+10.0*t**2)*x**2+20.0*sqrt(5.0)*exp(5.0*x**2*(3681.0+10.0*t**2)/ \
    (1227.0*(1227.0+10.0*t**2)))*sqrt(pi)**3*t**2*sqrt(1227.0+10.0*t**2)**3*(3681.0+30.0*t**2-10.0*x**2) \
    +141.0*exp(10.0*(1227.0+5.0*t**2)*x**2/(1227.0*(1227.0+10.0*t**2)))*pi*(1227.0+10.0*t**2) \
    *(1505529.0+10.0*t**2*(1227.0+10.0*x**2))))

def r_ex(x,t):
    from numpy import exp,pi,sqrt
    #return(1.0/(10.0*sqrt(pi)**5*sqrt(1227.0+10.0*t**2)**13)*(exp(-((5.0/1227.0)+20.0/(1227.0+10.0*t**2))*x**2)*(30.0*exp(((5.0/1227.0)+20.0/ \
    #(1227.0+10.0*t**2))*x**2)*sqrt(pi)**5*sqrt(1227.0+10.0*t**2)**13 + 5733625175.0 *sqrt(5.0) \
    #* exp(50.0*t**2*x**2/(1227.0*(1227.0+10.0*t**2)))*t**4*x**4 - 3659760750.0*exp(5.0*x**2/1227.0) \
    #*sqrt(pi)*t**4*sqrt(1227.0+10.0*t**2)*x**4 + 220900.0*exp(5.0*(3681.0+10.0*t**2)*x**2/(1227.0* \
    #(1227.0+10.0*t**2)))*sqrt(pi)**3*t**2 * sqrt(1227.0+10.0*t**2)**3*x**2*(-4516587.0 + \
    #300.0*t**4 - 200.0*t**2*x**2) + 3114690.0*sqrt(5.0)*exp(10.0*(1227.0+5.0*t**2)*x**2/ \
    #(1227.0*(1227.0+10.0*t**2))) * pi*t**2*(1227.0*10.0*t**2)*x**2 * (1505529.0+10.0*t**2 \
    #*(1227.0+10.0*x**2)) + 47.0*sqrt(5.0)*exp(10.0*(2454.0+5.0*t**2)*x**2/(1227.0 \
    #*(1227.0+10.0*t**2)))*pi**2*(1227.0+10.0*t**2)**2 * (6799852709523.0 + 90331740.0*t**2 \
    #*(1227.0+10.0*x**2) + 100.0*t**4*(4516587.0 + 73620.0*x**2+100.0*x**4)))))#/(10.0*sqrt(pi)**5 \
    #*sqrt(1227.0+10.0*t**2)**13))

    return((30.0*exp((0.004074979625101874 + 20.0/(1227.0 + 10.0*t**2))*x**2)*pi**2.5*(1227.0 + 10.0*t**2)**6.5 + \
         5733625175.0*sqrt(5.0)*exp((50.0*t**2*x**2)/(1227.0*(1227.0 + 10.0*t**2)))*t**4*x**4 - \
         3659760750.0*exp((5.0*x**2)/1227.0)*sqrt(pi)*t**4*sqrt(1227.0 + 10.0*t**2)*x**4 + \
         220900.0*exp((5.0*(3681.0 + 10.0*t**2)*x**2)/(1227.0*(1227.0 + 10.0*t**2)))*pi**1.5*t**2*(1227.0 + 10.0*t**2)**1.5*x**2* \
          (-4516587.0 + 300.0*t**4 - 200.0*t**2*x**2) + \
         3114690.0*sqrt(5.0)*exp((10.0*(1227.0 + 5.0*t**2)*x**2)/(1227.0*(1227.0 + 10.0*t**2)))*pi*t**2*(1227.0 + 10.0*t**2)*x**2* \
          (1505529.0 + 10.0*t**2*(1227.0 + 10.0*x**2)) + \
         47.0*sqrt(5.0)*exp((10.0*(2454.0 + 5.0*t**2)*x**2)/(1227.0*(1227.0 + 10.0*t**2)))*pi**2*(1227.0 + 10.0*t**2)**2* \
          (6799852709523.0 + 90331740.0*t**2*(1227.0 + 10.0*x**2) + 100.0*t**4*(4516587.0 + 73620.0*x**2 + 100.0*x**4)))/
       (10.0*exp((0.004074979625101874 + 20.0/(1227.0 + 10.0*t**2))*x**2)*pi**2.5*(1227.0 + 10.0*t**2)**6.5))

def k_ex(x,t):
    return(r_ex(x,t)-(p_ex(x,t)**2/rho_ex(x,t))-(q_ex(x,t)**2/p_ex(x,t)))

def s_ex(x,t):
    from numpy import exp,pi,sqrt
    return((-235*t*x*(1146725035*exp((50*t**2*x**2)/(1227.*(1227 + 10*t**2))) \
        *t**4*x**4 - 195187240*exp((5*x**2)/1227.)*sqrt(5*pi)*t**4* \
        sqrt(1227 + 10*t**2)*x**4 + 88360*sqrt(5)*exp((5*(3681 + 10*t**2)*x**2) \
        /(1227.*(1227 + 10*t**2)))*pi**1.5*t**2*(1227 + 10*t**2)**1.5*x**2* \
       (-1505529 + 50*t**4 - 5*t**2*(1227 + 10*x**2)) + 1038230*exp((10*(1227 \
       + 5*t**2)*x**2)/(1227.*(1227 + 10*t**2)))*pi*t**2*(1227 + 10*t**2)*x**2* \
      (1505529 + 10*t**2*(1227 + 10*x**2)) + 40*sqrt(5)*exp((25*(1227 + 2*t**2) \
      *x**2)/(1227.*(1227 + 10*t**2)))*pi**2.5*t**2*(1227 + 10*t**2)**2.5* \
      (736200*t**4 + 1500*t**6 - 1505529*(-3681 + 10*x**2) - \
      25*t**2*(-4516587 + 4908*x**2 + 4*x**4)) + 47*exp((10*(2454 + 5*t**2) \
      *x**2)/(1227.*(1227 + 10*t**2)))*pi**2*(1227 + 10*t**2)**2* \
      (6799852709523 + 90331740*t**2*(1227 + 10*x**2) + 100*t**4*(4516587 \
      + 73620*x**2 + 100*x**4))))/(2.*exp((0.004074979625101874 + 25/(1227 \
      + 10*t**2))*x**2)*pi**3*(1227 + 10*t**2)**8))
#-------------------------------------------------------------------------------
def plot(x,
         Q,
         xlow,
         xhigh,
         variables,
         t,
         AppParams):

    import numpy as np
    import matplotlib
    import matplotlib.pylab as plt
    matplotlib.rcParams.update({'font.size': 16, 'font.family': 'serif'})

    numVariables = len(variables)
    y = np.zeros([len(x),numVariables])
    for i in range(0, len(x)):
        y[i,0] = Plot.findy(x[i],Q[0,:,:], xlow, xhigh)
        y[i,1] = Plot.findy(x[i],Q[1,:,:], xlow, xhigh)/y[i,0]
        y[i,2] = Plot.findy(x[i],Q[2,:,:], xlow, xhigh)-y[i,0]*y[i,1]**2
        y[i,3] = Plot.findy(x[i],Q[3,:,:], xlow, xhigh) - y[i,0]*y[i,1]**3 - 3*y[i,2]*y[i,1]
        y[i,4] = Plot.findy(x[i],Q[4,:,:], xlow, xhigh) - y[i,0]*y[i,1]**4 - 6*y[i,2]*y[i,1]**2 \
        - 4*y[i,3]*y[i,1] - y[i,2]**2/y[i,0] - y[i,3]**2/y[i,2]
        y[i,5] = Plot.findy(x[i],Q[5,:,:], xlow, xhigh) - y[i,0]*y[i,1]**5 - 10*y[i,2]*y[i,1]**3 \
        - 10*y[i,3]*y[i,1]**2 - 5*y[i,1]*(y[i,4]+y[i,2]**2/y[i,0] + y[i,3]**2/y[i,2])


    xticks = np.linspace(xlow,xhigh,9);

    plt.figure(1)
    plt.clf()
    plt.grid()
    plt.plot(x,y[:,0], '-', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,rho_ex(x,t), '-', label = "analytical", linewidth = 2.0, color='r')
    plt.title(r'Density: $\rho(t,x)$ at time $t=$' +str(t))
    plt.legend(prop={'size':14}, loc =  'upper left')
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    plt.xticks(xticks)

    plt.figure(2)
    plt.clf()
    plt.grid()
    plt.plot(x,y[:,1], '-', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,u_ex(x,t), '-', label = "analytical", linewidth = 2.0, color='r')
    plt.title(r'Velocity: $u(t,x)$ at time $t=$' +str(t))
    plt.legend(prop={'size':14}, loc =  'upper left')
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    plt.xticks(xticks)

    plt.figure(3)
    plt.clf()
    plt.grid()
    plt.plot(x,y[:,2], '-', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,p_ex(x,t), '-', label = "analytical", linewidth = 2.0, color='r')
    plt.title(r'Pressure: $p(t,x)$ at time $t=$' +str(t))
    plt.legend(prop={'size':14}, loc =  'upper left')
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    plt.xticks(xticks)
    plt.savefig("num_euler_shocktube_pressure.pdf", format='pdf')

    plt.figure(4)
    plt.clf()
    plt.grid()
    plt.plot(x,y[:,3], '-', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,q_ex(x,t), '-', label = "analytical", linewidth = 2.0, color='r')
    plt.title(r'Heat Flux: $q(t,x)$ at time $t=$' +str(t))
    plt.legend(prop={'size':14}, loc =  'upper left')
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    plt.xticks(xticks)
    plt.savefig("num_euler_shocktube_heat_flux.pdf", format='pdf')

    plt.figure(5)
    plt.clf()
    plt.grid()
    plt.plot(x,y[:,4], '-', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,k_ex(x,t), '-', label = "analytical", linewidth = 2.0, color='r')
    plt.legend(prop={'size':14}, loc =  'upper left')
    plt.title(r'k: $k(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    plt.xticks(xticks)

    plt.figure(6)
    plt.clf()
    plt.grid()
    plt.plot(x,y[:,5], '-', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,s_ex(x,t), '-', label = "analytical", linewidth = 2.0, color='r')
    plt.legend(prop={'size':14}, loc =  'upper left')
    plt.title(r's: $s(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    plt.xticks(xticks)

    plt.draw()
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
if __name__== '__main__':

    # -----------------------------
    # GRAB SOME IMPORTANT FUNCTIONS
    # -----------------------------
    # parameter function (this sits in the current local directory)
    from parameters import parameters_init;
    # set path to main library for DG code
    import sys; sys.path.insert(0,'../../lib');

    # --------------
    # SET PARAMETERS
    # --------------
    output_dir = "output";
    AppParams,DimParams,MeshParams,TimeParams,PredParams \
        = parameters_init(output_dir);

    # -------------
    # PLOT SOLUTION
    # -------------
    import Plot
    Plot.plotFrames(output_dir,AppParams,plot);

#-------------------------------------------------------------------------------
