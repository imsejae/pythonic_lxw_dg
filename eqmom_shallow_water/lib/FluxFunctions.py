
#-------------------------------------------------------------------------------
def speed_max(u,T,dels): #dels

    # WARNING: this function takes as input "rstar" and NOT "r":
    #
    #         rstar = r - p^2/rho - q^2/p
    #

    from numpy import sqrt,abs,max,array,size,isnan;
    from eqmom_closure import m_star

    #print("T is", T);
    s = abs(u)+sqrt(T);
    #if isnan(sqrt(T)):
    #    print("T is", T)

    return s;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------

#------------------- Convert from Conservative to Primitive Variables ----------
def GetPrimFromCons(Q_at_QuadPts,
                    numEqns,
                    numQuadPts,
                    AppParams):

    return Q_at_QuadPts


#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def FluxFunction(W,
                 i,
                 tauIndex,
                 xiIndex,
                 numPredBasis,
                 psiAtQuadP,
                 AppParams,
                 numEqns,
                 dels,
                 t,
                 numGridCells,
                 xloc,
                 tloc): #take out t and numGridCells!

    from basis_data import FindPrimitiveVar;
    from numpy import array,zeros,pi;
    from eqmom_closure import m_star;

    Q = zeros(numEqns);
    # get primitive variables from prediction step
    for j in range(0,numEqns):
        Q[j] =FindPrimitiveVar(W[j*numPredBasis:(j+1)*numPredBasis,:],
                               tauIndex,
                               xiIndex,
                               i,
                               numPredBasis,
                               psiAtQuadP);

    return array([Q[1],0.5*Q[0]**2 + (Q[1]**2)/Q[0]])

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def EvalPrimJacobian(numEqns,
                     numOrder,
                     AppParams,
                     primvars):

    from numpy import zeros;

    primJac  = zeros([numEqns,numEqns,numOrder,numOrder]);

    for a in range(0,numOrder):
        for b in range(0,numOrder):
            h   = primvars[0,a,b];
            u     = primvars[1,a,b]/h;

            primJac[0,0,a,b] = 0.0;
            primJac[0,1,a,b] = 1.0;

            primJac[1,0,a,b] = h - u**2;
            primJac[1,1,a,b] = 2.0*u;


    return primJac;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def NumericalFlux(i,
                  W,
                  tauIndex,
                  numGridCells,
                  numEqns,
                  numPredBasis,
                  boundary,
                  AppParams,
                  QuadData,
                  dels,
                  t,
                  xloc,
                  tloc): #dels
    # here W1, W2, and W3 are 2 dimensional because they store over all
    # the grid cells this returns the unitegrated form of flux

    from numpy import sqrt,abs,max,zeros,isnan,array,pi,shape;
    from eqmom_closure import m_star;

    if boundary == 'periodic':
        # these are the periodic boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1))
        iMod = i*(i>=0)+(i<0)*(numGridCells-1)
    elif boundary == 'extrapolation':
        # these are the 0 derivative boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1))+(i==(numGridCells-1))*i
        iMod = i*(i>=0)+(i<0)*(0)

    W1 = zeros([numEqns,numPredBasis,numGridCells])

    for j in range(0,numEqns):
        W1[j,:,:] = W[j*numPredBasis:(j+1)*numPredBasis,:];

    consL = zeros(numEqns);
    consR = zeros(numEqns);


    NumFlux = zeros(numEqns);

    for k in range(0,numPredBasis):
        # k is the basis function index
        for j in range(0,numEqns):
            consL[j] += QuadData['psiAtQuadRight'][k,tauIndex]*W1[j,k,iMod];
            consR[j] += QuadData['psiAtQuadLeft'][k,tauIndex]* W1[j,k,ip1];

    fL = array([consL[1],0.5*consL[0]**2 + (consL[1]**2)/consL[0]])
    fR = array([consR[1],0.5*consR[0]**2 + (consR[1]**2)/consR[0]])


    uL = consL[1]/consL[0];
    uR = consR[1]/consR[0];
    u_ave = 0.5*(uL+uR);
    TL = consL[0]
    TR = consR[0]
    T_ave = 0.5*(TL+TR);

    #print("p is", consR[2]-consR[0]*(consR[1]/consR[0])**2)

    sL   = speed_max(uL,TL,dels); #dels
    sR   = speed_max(uR,TR,dels); #dels
    #sAve = speed_max(rho_ave,u_ave,p_ave,q_ave,rstar_ave);
    sAve = speed_max(u_ave,T_ave,dels); #dels

    #if (isnan(sL) or isnan(sR) or isnan(sAve)):
    #    print(" ")
    #    print("ERROR: NumericalFlux in FluxFunctions.py")
    #    print("    iMod = ",iMod,"ip1 = ",ip1)
    #    print("    (rhoL_mod,uL,pL_mod,qL,rstarL_mod) = ",[rhoL_mod,uL,pL_mod,qL,rstarL_mod])
    #    print("    (rhoR_mod,uR,pR_mod,qR,rstarR_mod) = ",[rhoR_mod,uR,pR_mod,qR,rstarR_mod])
    #    print("    (sL,sR,sAve) = ",[sL,sR,sAve])
    #    print("     Left coeffs: W1[:,iMod] = ",W1[:,iMod])
    #    print("     Left coeffs: W2[:,iMod] = ",W2[:,iMod])
    #    print("     Left coeffs: W3[:,iMod] = ",W3[:,iMod])
    #    print("     Left coeffs: W4[:,iMod] = ",W4[:,iMod])
    #    print("     Left coeffs: W5[:,iMod] = ",W5[:,iMod])
    #    print("    Right coeffs: W1[:,ip1]  = ",W1[:,ip1])
    #    print("    Right coeffs: W2[:,ip1]  = ",W2[:,ip1])
    #    print("    Right coeffs: W3[:,ip1]  = ",W3[:,ip1])
    #    print("    Right coeffs: W4[:,ip1]  = ",W4[:,ip1])
    #    print("    Right coeffs: W5[:,ip1]  = ",W5[:,ip1])
    #    print(" ")
    #    raise

    lambdaMax = max([sL,sR,sAve]);

    for m in range(0,numEqns):
        NumFlux[m] = 0.5*(fR[m]+fL[m] - lambdaMax*(consR[m]-consL[m]));


    return NumFlux,lambdaMax
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def NumericalFlux_LxF(i,
                      Q,
                      numGridCells,
                      numEqns,
                      boundary,
                      AppParams,
                      dels): #dels

    from numpy import sqrt,abs,max,zeros,isnan,array;
    from eqmom_closure import m_star;

    if boundary == 'periodic':
        # these are the periodic boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1))
        iMod = i*(i>=0)+(i<0)*(numGridCells-1)
    elif boundary == 'extrapolation':
        # these are the 0 derivative boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1))+(i==(numGridCells-1))*i
        iMod = i*(i>=0)+(i<0)*(0)

    primL = primvars(Q[0,iMod,0],Q[1,iMod,0],Q[2,iMod,0],\
                     Q[3,iMod,0],Q[4,iMod,0]);
    primR = primvars(Q[0,ip1,0],Q[1,ip1,0],Q[2,ip1,0],\
                     Q[3,ip1,0],Q[4,ip1,0]);

    rhoL   = primL[0];
    uL     = primL[1];
    pL     = primL[2];
    qL     = primL[3];
    rstarL = primL[4];

    rhoR   = primR[0];
    uR     = primR[1];
    pR     = primR[2];
    qR     = primR[3];
    rstarR = primR[4];

    rL = rstarL + pL**2/rhoL + qL**2/pL;
    rR = rstarR + pR**2/rhoR + qR**2/pR;

    #fL = flux(rhoL,uL,pL,qL,rL);
    #fR = flux(rhoR,uR,pR,qR,rR);

    #Flux for eqmom_closure
    fL = m_star(primL,dels); #dels
    fR = m_star(primR,dels); #dels

    rhoL_mod   = rhoL #max([rhoL,1.0e-10]);   # desingularize for wave speed only
    rhoR_mod   = rhoR #max([rhoR,1.0e-10]);   # desingularize for wave speed only
    pL_mod     = pL #max([pL,1.0e-10]);     # desingularize for wave speed only
    pR_mod     = pR #max([pR,1.0e-10]);     # desingularize for wave speed only
    rstarL_mod = rstarL #max([rstarL,1.0e-10]); # desingularize for wave speed only
    rstarR_mod = rstarR #max([rstarR,1.0e-10]); # desingularize for wave speed only

    rho_ave   = 0.5*(rhoL_mod+rhoR_mod);
    u_ave     = 0.5*(uL+uR);
    q_ave     = 0.5*(qL+qR);
    p_ave     = 0.5*(pL_mod+pR_mod);
    rstar_ave = 0.5*(rstarL_mod+rstarR_mod);

    sL   = speed_max(rhoL_mod,uL,pL_mod,qL,rstarL_mod); #dels
    sR   = speed_max(rhoR_mod,uR,pR_mod,qR,rstarR_mod); #dels
    sAve = speed_max(rho_ave,u_ave,p_ave,q_ave,rstar_ave); #dels

    if (isnan(sL) or isnan(sR) or isnan(sAve)):
        print(" ")
        print("ERROR: NumericalFlux in FluxFunctions.py")
        print("    iMod = ",iMod,"ip1 = ",ip1)
        print("    (rhoL_mod,uL,pL_mod,qL,rstarL_mod) = ",[rhoL_mod,uL,pL_mod,qL,rstarL_mod])
        print("    (rhoR_mod,uR,pR_mod,qR,rstarR_mod) = ",[rhoR_mod,uR,pR_mod,qR,rstarR_mod])
        print("    (sL,sR,sAve) = ",[sL,sR,sAve])
        print("     Left coeffs: W1[:,iMod] = ",W1[:,iMod])
        print("     Left coeffs: W2[:,iMod] = ",W2[:,iMod])
        print("     Left coeffs: W3[:,iMod] = ",W3[:,iMod])
        print("     Left coeffs: W4[:,iMod] = ",W4[:,iMod])
        print("     Left coeffs: W5[:,iMod] = ",W5[:,iMod])
        print("    Right coeffs: W1[:,ip1]  = ",W1[:,ip1])
        print("    Right coeffs: W2[:,ip1]  = ",W2[:,ip1])
        print("    Right coeffs: W3[:,ip1]  = ",W3[:,ip1])
        print("    Right coeffs: W4[:,ip1]  = ",W4[:,ip1])
        print("    Right coeffs: W5[:,ip1]  = ",W5[:,ip1])
        print(" ")
        raise

    lambdaMax = max([sL,sR,sAve]);

    NumFlux = zeros(numEqns);
    for m in range(0,numEqns):
        NumFlux[m] = 0.5*(fR[m]+fL[m] - lambdaMax*(Q[m,ip1,0]-Q[m,iMod,0]));

    return NumFlux;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
#No source function
def SourceFunction(t_i,
                   x_i,
                   i,
                   timestep,
                   spacestep,
                   tauIndex,
                   xiIndex,
                   xi,
                   s,
                   W,
                   psiAtQuadP,
                   E_field,
                   AppParams,
                   numEqns,
                   numPredBasis,
                   numOrder):



    from numpy import array,zeros,shape,dot;

    tmp = zeros(numEqns);
    vars = zeros(numEqns);

    for m in range(0,numEqns):
        vars[m] = dot(psiAtQuadP[:,xiIndex,tauIndex],W[m*numPredBasis:(m+1)*numPredBasis,i]);

    #E_vars = dot(psiAtQuadP[:,xiIndex,tauIndex],E_field[:,i]);

    for j in range(0,numEqns):
        #print("t_i", t_i)
        #print("x_i", x_i)
        #print("xi[0]", xi[0])
        #print("tauIndex", tauIndex)
        #print("xiIndex", xiIndex)
        tmp[j] = s[j](t_i+0.5*timestep*xi[tauIndex],
                           x_i+0.5*spacestep*xi[xiIndex],
                           vars,
                           #E_vars,
                           AppParams);

    return tmp



    #return array([s[0](t_i+0.5*timestep*xi[tauIndex],
    #                   x_i+0.5*spacestep*xi[xiIndex],
    #                   AppParams),
    #              s[1](t_i+0.5*timestep*xi[tauIndex],
    #                   x_i+0.5*spacestep*xi[xiIndex],
    #                   AppParams),
    #              s[2](t_i+0.5*timestep*xi[tauIndex],
    #                   x_i+0.5*spacestep*xi[xiIndex],
    #                   AppParams),
    #              s[3](t_i+0.5*timestep*xi[tauIndex],
    #                   x_i+0.5*spacestep*xi[xiIndex],
    #                   AppParams),
    #              s[4](t_i+0.5*timestep*xi[tauIndex],
    #                   x_i+0.5*spacestep*xi[xiIndex],
    #                   AppParams)]);
#-------------------------------------------------------------------------------
