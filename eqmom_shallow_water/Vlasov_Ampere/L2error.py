#-------------------------------------------------------------------------------
def rho_exact(t,x):
    from numpy import sin,pi;
    return 1.0 + 0.5*sin(3.0*pi*(x-0.5*t));
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def u_exact(t,x):
    return 0.5+0.0*x;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def p_exact(t,x):
    return 0.75+0.0*x;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def q_exact(t,x):
    return 0.5+0.0*x;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def r_exact(t,x):
    return 3.0+0.0*x;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def L2error(xlow,
            dx,
            numGridCells,
            numOrder,
            time,
            Q,
            AppParams):

    from numpy import sqrt;
    from basis_data import L2Project_GaussQuadrature;

    #gamma = AppParams['gamma']

    rho_exact_soln = lambda x : rho_exact(time,x);
    m_exact_soln   = lambda x : rho_exact(time,x)*u_exact(time,x);
    E_exact_soln   = lambda x : rho_exact(time,x)*u_exact(time,x)**2 + p_exact(time,x);
    hf_exact_soln  = lambda x : rho_exact(time,x)*u_exact(time,x)**3 \
                                + 3.0*p_exact(time,x)*u_exact(time,x) + q_exact(time,x);
    kur_exact_soln = lambda x : rho_exact(time,x)*u_exact(time,x)**4 \
                                + 6.0*p_exact(time,x)*u_exact(time,x)**2 \
                                + 4.0*q_exact(time,x)*u_exact(time,x) + r_exact(time,x);

    rho_ex_coeffs = L2Project_GaussQuadrature(rho_exact_soln,
                                              xlow,
                                              dx,
                                              numGridCells,
                                              numOrder+1,
                                              numOrder+1);

    m_ex_coeffs = L2Project_GaussQuadrature(m_exact_soln,
                                            xlow,
                                            dx,
                                            numGridCells,
                                            numOrder+1,
                                            numOrder+1);

    E_ex_coeffs = L2Project_GaussQuadrature(E_exact_soln,
                                            xlow,
                                            dx,
                                            numGridCells,
                                            numOrder+1,
                                            numOrder+1);

    hf_ex_coeffs = L2Project_GaussQuadrature(hf_exact_soln,
                                            xlow,
                                            dx,
                                            numGridCells,
                                            numOrder+1,
                                            numOrder+1);

    kur_ex_coeffs = L2Project_GaussQuadrature(kur_exact_soln,
                                            xlow,
                                            dx,
                                            numGridCells,
                                            numOrder+1,
                                            numOrder+1);

    err_rho = 0.0;
    err_m   = 0.0;
    err_E   = 0.0;
    err_hf  = 0.0;
    err_kur = 0.0;
    l2_exact_rho = 0.0;
    l2_exact_m   = 0.0;
    l2_exact_E   = 0.0;
    l2_exact_hf  = 0.0;
    l2_exact_kur = 0.0;
    for i in range(0,numGridCells):
        for k in range(0,numOrder):
            err_rho += (Q[0,i,k]-rho_ex_coeffs[i,k])**2
            err_m   += (Q[1,i,k]-m_ex_coeffs[i,k]  )**2
            err_E   += (Q[2,i,k]-E_ex_coeffs[i,k]  )**2
            err_hf  += (Q[3,i,k]-hf_ex_coeffs[i,k] )**2
            err_kur += (Q[4,i,k]-kur_ex_coeffs[i,k])**2
            l2_exact_rho += (rho_ex_coeffs[i,k])**2
            l2_exact_m   += (m_ex_coeffs[i,k]  )**2
            l2_exact_E   += (E_ex_coeffs[i,k]  )**2
            l2_exact_hf  += (hf_ex_coeffs[i,k] )**2
            l2_exact_kur += (kur_ex_coeffs[i,k])**2
        for k in range(numOrder,numOrder+1):
            err_rho += (rho_ex_coeffs[i,k])**2
            err_m   += (m_ex_coeffs[i,k]  )**2
            err_E   += (E_ex_coeffs[i,k]  )**2
            err_hf  += (hf_ex_coeffs[i,k] )**2
            err_kur += (kur_ex_coeffs[i,k])**2
            l2_exact_rho += (rho_ex_coeffs[i,k])**2
            l2_exact_m   += (m_ex_coeffs[i,k]  )**2
            l2_exact_E   += (E_ex_coeffs[i,k]  )**2
            l2_exact_hf  += (hf_ex_coeffs[i,k] )**2
            l2_exact_kur += (kur_ex_coeffs[i,k])**2

    final_err_rho = sqrt(err_rho/l2_exact_rho);
    final_err_m   = sqrt(err_m  /l2_exact_m  );
    final_err_E   = sqrt(err_E  /l2_exact_E  );
    final_err_hf  = sqrt(err_hf /l2_exact_hf );
    final_err_kur = sqrt(err_kur/l2_exact_kur);

    final_total_error = final_err_rho + final_err_m + final_err_E + final_err_hf + final_err_kur;

    filename = "error.dat";
    file = open(filename,'w');
    file.write("%i     %i     %22.15e     %22.15e\n" % \
        (numOrder,numGridCells,final_total_error,dx));
    file.close();
#-------------------------------------------------------------------------------
