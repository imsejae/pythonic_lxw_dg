#-------------------------------------------------------------------------------
# Vlasov Poisson PROBLEM EXAMPLE
#-------------------------------------------------------------------------------
def mynormal(x,xcenter,std):
    from numpy import sqrt,exp,pi
    return (1.0/sqrt(2.0*pi*std))*exp(-((x-xcenter)**2)/(2.0*std));

#def myPDF(x,alpha,k):
#    from numpy import cos,sqrt,pi
#    return (1+alpha*cos(k*x))*(1/sqrt(2*pi))*exp()

#------------------- Initial Conditions: Primitive variables -------------------
###For initial conditions laid out in Schaerer paper section 6.1
#def rho(x,AppParams):
#    return 1.0 + 4.7*mynormal(x,0.0,122.7);
#def u(x,AppParams):
#    return 0.0*x;
#def p(x,AppParams):
#    return 1.0 + 4.7*mynormal(x,0.0,122.7);
#def q(x,AppParams):
#    return 0.0*x;
#def rstar(x,AppParams):
#    return 2.0*(1.0 + 4.7*mynormal(x,0.0,122.7));
#def s(x,AppParams):
#    return 0.0*x;

###For initial conditions laid out in Landau example of Seal_PhD
#def rho(x,AppParams):
#    from numpy import cos
#    return 1.0 - 0.5*cos(0.5*x);
#def u(x,AppParams):
#    return 0.0*x;
#def p(x,AppParams):
#    from numpy import cos
#    return 3.0 + 1.5*cos(0.5*x);
#def q(x,AppParams):
#    return 0.0*x;
#def rstar(x,AppParams):
#    from numpy import cos
#    return 6.0-3.0*cos(0.5*x);


###For initial conditions for Method of Manufactured Solutions

#-------------------------------------------------------------------------------

#------------------- Initial Conditions: Conserved variables -------------------
def q0(x,AppParams):
    from numpy import cos,sqrt,pi
    return 0.5*sqrt(pi)*(2.0-cos(2*x))
def q1(x,AppParams):
    from numpy import cos,sqrt,pi
    return (sqrt(pi)/8.0)*(2.0-cos(2*x))
def q2(x,AppParams):
    from numpy import cos,sqrt,pi
    return (3.0*sqrt(pi)/32.0)*(2.0-cos(2*x))
def q3(x,AppParams):
    from numpy import cos,sqrt,pi
    return (7.0*sqrt(pi)/128.0)*(2.0-cos(2*x))
def q4(x,AppParams):
    from numpy import cos,sqrt,pi
    return (25.0*sqrt(pi)/512.0)*(2.0-cos(2*x))
def q5(x,AppParams): ##Electric Field
    from numpy import pi,sqrt,sin
    return((-0.25*sqrt(pi)*sin(2*x)))
#-------------------------------------------------------------------------------

#---------------------------- Source Term Functions ----------------------------
####Source term is of the form: ell*M^(ell-1)*E
def s0(t,x,primvars,AppParams):
    from numpy import sin,pi,sqrt,cos
    return (sqrt(pi)*(-1.0 + 4.0*pi)*sin(2.0*pi*t - 2.0*x))/4.0
def s1(t,x,primvars,AppParams):
    from numpy import sin,pi,sqrt,cos,shape
    tmp = ((-3.0*sqrt(pi) - 4.0*pi + 4.0*pi**1.5 + \
    2.0*pi*cos(2.0*pi*t - 2.0*x))*sin(2.0*pi*t - 2.0*x))/16.0 \
    + primvars[0]*primvars[5]
    return(tmp)
def s2(t,x,primvars,AppParams):
    from numpy import sin,pi,sqrt,cos
    tmp = ((-7.0*sqrt(pi) - 8.0*pi + 12.0*pi**1.5 + \
    4.0*pi*cos(2.0*pi*t - 2.0*x))*sin(2.0*pi*t - 2.0*x))/64.0 \
     + 2*primvars[1]*primvars[5]
    return(tmp)
def s3(t,x,primvars,AppParams):
    from numpy import sin,pi,sqrt,cos
    tmp = ((-25.0*sqrt(pi) - 36.0*pi + 28.0*pi**1.5 + \
    18.0*pi*cos(2.0*pi*t - 2.0*x))*sin(2.0*pi*t - 2.0*x))/256.0 \
    + 3*primvars[2]*primvars[5]
    return(tmp)
def s4(t,x,primvars,AppParams):
    from numpy import sin,pi,sqrt,cos
    tmp = ((-81.0*sqrt(pi) - 112.0*pi + 100.0*pi**1.5 + \
    56.0*pi*cos(2.0*pi*t - 2.0*x))*sin(2.0*pi*t - 2.0*x))/1024.0 \
    + 4*primvars[3]*primvars[5]
    return(tmp)

def s5(t,x,primvars,AppParams):
    from numpy import sqrt,pi,cos
    C1 = sqrt(pi)/4.0 + (sqrt(pi)/8.0)*(4.0*pi-1)*cos(2.0*x-2.0*pi*t)
    tmp = -primvars[1] + C1
    return(tmp)

#def s0(t,x,primvars,E_vars,AppParams):
#    from numpy import sin,pi,sqrt,cos
#    tmp = (sqrt(pi)*(-1.0 + 4.0*pi)*sin(2.0*pi*t - 2.0*x))/4.0
#    return(tmp)

#def s1(t,x,primvars,E_vars,AppParams):
#    from numpy import sin,pi,sqrt,cos
#    tmp = (sqrt(pi)*(-3.0 + 4.0*pi)*sin(2.0*pi*t - 2.0*x))/16.0
#    return(tmp)

#def s2(t,x,primvars,E_vars,AppParams):
#    from numpy import sin,pi,sqrt,cos
#    tmp = (sqrt(pi)*(-7.0 + 12.0*pi)*sin(2.0*pi*t - 2.0*x))/64.0
#    return(tmp)

#def s3(t,x,primvars,E_vars,AppParams):
#    from numpy import sin,pi,sqrt,cos
#    tmp = (sqrt(pi)*(-25.0 + 28.0*pi)*sin(2.0*pi*t - 2.0*x))/256.0
#    return(tmp)

#def s4(t,x,primvars,E_vars,AppParams):
#    from numpy import sin,pi,sqrt,cos
#    tmp = (sqrt(pi)*(-81.0 + 100.0*pi)*sin(2.0*pi*t - 2.0*x))/1024.0
#    return(tmp)

#-------------------------------------------------------------------------------

#---------------------------- Primitive Equations Source Term Functions --------
####Source term is of the form: ell*M^(ell-1)*E
def s0_prim(t,x,primvars,AppParams):
    return s0(t,x,primvars,AppParams)
def s1_prim(t,x,primvars,AppParams):
    return s1(t,x,primvars,AppParams)
def s2_prim(t,x,primvars,AppParams):
    return s2(t,x,primvars,AppParams)
def s3_prim(t,x,primvars,AppParams):
    return s3(t,x,primvars,AppParams)
def s4_prim(t,x,primvars,AppParams):
    return s4(t,x,primvars,AppParams)
def s5_prim(t,x,primvars,AppParams):
    return s5(t,x,primvars,AppParams)
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
if __name__ == "__main__":

    # -----------------------------
    # GRAB SOME IMPORTANT FUNCTIONS
    # -----------------------------
    # parameter function (this sits in the current local directory)
    from parameters import parameters_init;
    # set path to main library for DG code
    import sys; sys.path.insert(0,'../../lib');
    # main Lax-Wendroff code (this sits in the main library directory)
    from LaxWendroffDG import LaxWendDG;

    # --------------
    # SET PARAMETERS
    # --------------
    from os import system;
    output_dir = "output";
    system("rm -f -r " + output_dir);
    system("mkdir " + output_dir);
    AppParams,DimParams,MeshParams,TimeParams,PredParams \
        = parameters_init(output_dir);

    # --------------------------------
    # QUICK SANITY CHECK OF PARAMETERS
    # --------------------------------
    #assert(DimParams['numEqns']==5);

    # ------------------------------
    # EXECUTE LAX-WENDROFF DG METHOD
    # ------------------------------
    Q = LaxWendDG([q0,q1,q2,q3,q4,q5],          # initial condition function
                  [s0,s1,s2,s3,s4,s5],          # source term function for conservative vars
                  [s0_prim,s1_prim,s2_prim,s3_prim,s4_prim,s5_prim],       # source term function for primitive vars
                  AppParams,                 # application-specific parameters
                  DimParams,                 # parameters determining dimensions of arrays
                  MeshParams,                # mesh-specific parameters
                  TimeParams,                # time-stepping-specific parameters
                  PredParams,                # prediction-step-specific parameters
                  output_dir);               # directory to where output is written

#-------------------------------------------------------------------------------
