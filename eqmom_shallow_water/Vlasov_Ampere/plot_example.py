#----------------------------------------------------------------------------------
# plot_example.py receive .txt files "output/out*.txt" with the solution
# obtained with the Lax Wendrof DG with limiters method.
#----------------------------------------------------------------------------------
def rho_ex(x,t):
    from numpy import exp,pi,sqrt,cos
    return(0.5*sqrt(pi)*(2.0-cos(2.0*pi*t-2.0*x)))

def u_ex(x,t):
    from numpy import exp,pi,sqrt,cos
    return(0.25*x/x)

def p_ex(x,t):
    from numpy import exp,pi,sqrt,cos
    return(-(sqrt(pi)*(-2.0 + cos(2.0*pi*t - 2.0*x)))/16.0)

def q_ex(x,t):
    from numpy import exp,pi,sqrt,cos
    return(0.0*x)

def r_ex(x,t):
    from numpy import exp,pi,sqrt,cos
    return((-3.0*sqrt(pi)*(-2.0 + cos(2.0*pi*t - 2.0*x)))/128.0)

def k_ex(x,t):
    return(r_ex(x,t)-(p_ex(x,t)**2/rho_ex(x,t))-(q_ex(x,t)**2/p_ex(x,t)))

def E_ex(x,t):
    from numpy import sqrt,pi,sin
    return(-0.25*sqrt(pi)*sin(2*x - 2*pi*t))


#-------------------------------------------------------------------------------
def plot(x,
         Q,
         E, ###Added to plot electric field
         xlow,
         xhigh,
         variables,
         t,
         AppParams):

    import numpy as np
    import matplotlib
    import matplotlib.pylab as plt
    matplotlib.rcParams.update({'font.size': 16, 'font.family': 'serif'})



    numVariables = len(variables)
    y = np.zeros([len(x),numVariables])
    yElec = np.zeros(len(x))
    for i in range(0, len(x)):
        y[i,0] = Plot.findy(x[i],Q[0,:,:], xlow, xhigh)
        y[i,1] = Plot.findy(x[i],Q[1,:,:], xlow, xhigh)/y[i,0]
        y[i,2] = Plot.findy(x[i],Q[2,:,:], xlow, xhigh)-y[i,0]*y[i,1]**2
        y[i,3] = Plot.findy(x[i],Q[3,:,:], xlow, xhigh) - y[i,0]*y[i,1]**3 - 3*y[i,2]*y[i,1]
        y[i,4] = Plot.findy(x[i],Q[4,:,:], xlow, xhigh) - y[i,0]*y[i,1]**4 - 6*y[i,2]*y[i,1]**2 \
        - 4*y[i,3]*y[i,1] - y[i,2]**2/y[i,0] - y[i,3]**2/y[i,2]
        #y[i,5] = Plot.findy(x[i],Q[5,:,:], xlow, xhigh) - y[i,0]*y[i,1]**5 - 10*y[i,2]*y[i,1]**3 \
        #- 10*y[i,3]*y[i,1]**2 - 5*y[i,1]*(y[i,4]+y[i,2]**2/y[i,0] + y[i,3]**2/y[i,2])
        #elecField[i] = Plot.findy(x[i],E[:,:], xlow, xhigh)
        y[i,5] = Plot.findy(x[i],Q[5,:,:],xlow,xhigh)
        yElec[i] = Plot.findy(x[i],E[0,:,:], xlow, xhigh)


    rho_error = max(abs(y[:,0]-rho_ex(x,t)))
    u_error = max(abs(y[:,1]-u_ex(x,t)))
    p_error = max(abs(y[:,2]-p_ex(x,t)))
    q_error = max(abs(y[:,3]-q_ex(x,t)))
    k_error = max(abs(y[:,4]-k_ex(x,t)))
    elec_error = max(abs(y[:,5]-E_ex(x,t)))
    error = max(rho_error,u_error,p_error,q_error,k_error)
    print("rho_error is", rho_error)
    print("u_error is", u_error)
    print("p_error is", p_error)
    print("q_error is", q_error)
    print("k_error is", k_error)
    print("elec_error is", elec_error)
    print("error is", error)


    xticks = np.linspace(xlow,xhigh,9);

    plt.figure(1)
    plt.clf()
    plt.grid()
    plt.plot(x,y[:,0], 'o', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,rho_ex(x,t), '-', label = "analytical", linewidth = 2.0, color='r')
    plt.title(r'Density: $\rho(t,x)$ at time $t=$' +str(t))
    plt.legend(prop={'size':14}, loc =  'upper left')
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    plt.xticks(xticks)

    plt.figure(2)
    plt.clf()
    plt.grid()
    plt.plot(x,y[:,1], 'o', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,u_ex(x,t), '-', label = "analytical", linewidth = 2.0, color='r')
    plt.title(r'Velocity: $u(t,x)$ at time $t=$' +str(t))
    plt.legend(prop={'size':14}, loc =  'upper left')
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    plt.xticks(xticks)

    plt.figure(3)
    plt.clf()
    plt.grid()
    plt.plot(x,y[:,2], 'o', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,p_ex(x,t), '-', label = "analytical", linewidth = 2.0, color='r')
    plt.title(r'Pressure: $p(t,x)$ at time $t=$' +str(t))
    plt.legend(prop={'size':14}, loc =  'upper left')
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    plt.xticks(xticks)
    plt.savefig("num_euler_shocktube_pressure.pdf", format='pdf')

    plt.figure(4)
    plt.clf()
    plt.grid()
    plt.plot(x,y[:,3], 'o', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,q_ex(x,t), '-', label = "analytical", linewidth = 2.0, color='r')
    plt.title(r'Heat Flux: $q(t,x)$ at time $t=$' +str(t))
    plt.legend(prop={'size':14}, loc =  'upper left')
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    plt.xticks(xticks)
    plt.savefig("num_euler_shocktube_heat_flux.pdf", format='pdf')

    plt.figure(5)
    plt.clf()
    plt.grid()
    plt.plot(x,y[:,4], 'o', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,k_ex(x,t), '-', label = "analytical", linewidth = 2.0, color='r')
    plt.legend(prop={'size':14}, loc =  'upper left')
    plt.title(r'k: $k(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    plt.xticks(xticks)

    #plt.figure(6)
    #plt.clf()
    #plt.grid()
    #plt.plot(x,y[:,5], '-', label = "numerical", linewidth = 2.0, color='b')
    #plt.plot(x,s_ex(x,t), '-', label = "analytical", linewidth = 2.0, color='r')
    #plt.legend(prop={'size':14}, loc =  'upper left')
    #plt.title(r's: $s(t,x)$ at time $t=$' +str(t))
    #plt.xlabel(r'$x$')
    #plt.ylabel('')
    #plt.xlim(xlow,xhigh)
    #plt.xticks(xticks)

    plt.figure(6)
    plt.clf()
    plt.grid()
    plt.plot(x,y[:,5], 'o', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,E_ex(x,t), '-', label = "analytical", linewidth = 2.0, color='r')
    plt.title(r'Electric Field: $E(t,x)$ at time $t=$' +str(t))
    plt.legend(prop={'size':14}, loc =  'upper left')
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    plt.xticks(xticks)


    plt.draw()
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
if __name__== '__main__':

    # -----------------------------
    # GRAB SOME IMPORTANT FUNCTIONS
    # -----------------------------
    # parameter function (this sits in the current local directory)
    from parameters import parameters_init;
    # set path to main library for DG code
    import sys; sys.path.insert(0,'../../lib');

    # --------------
    # SET PARAMETERS
    # --------------
    output_dir = "output";
    AppParams,DimParams,MeshParams,TimeParams,PredParams \
        = parameters_init(output_dir);

    # -------------
    # PLOT SOLUTION
    # -------------
    import Plot
    Plot.plotFrames(output_dir,AppParams,plot);

#-------------------------------------------------------------------------------
