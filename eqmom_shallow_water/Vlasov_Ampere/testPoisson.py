from numpy import pi,cos,linspace,zeros,sin,sqrt,log2,shape
from PoissonSolver import poisson_solve
from parameters import parameters_init
import sys; sys.path.insert(0,'../../lib');
from basis_data import computeCoefficients;
from numpy.linalg import norm;
import Plot
import matplotlib
import matplotlib.pylab as plt
matplotlib.rcParams.update({'font.size': 14, 'font.family': 'serif'})

numGridCells = 50;
order = 5; #Choice of 1 through 5
xlow = -2*pi
xhigh = 2*pi
spacestep = (xhigh-xlow)/numGridCells;
x = linspace(xlow+0.5*spacestep, xhigh-0.5*spacestep, numGridCells);
#P = zeros(numGridCells);
#for k in range(0,numGridCells):
#    P[k] = -0.5*cos(0.5*x[k]);

from os import system;
output_dir = "output";
system("rm -f -r " + output_dir);
system("mkdir " + output_dir);
AppParams,DimParams,MeshParams,TimeParams,PredParams \
    = parameters_init(output_dir);

def rhoMinusRhoNot(x,AppParams):
    return -0.5*cos(0.5*x);

def exact_elecField(x,AppParams):
    return -sin(0.5*x);

def exact_potential(x,AppParams):
    return -2.0*cos(0.5*x)-2.0;

P = zeros((numGridCells,order));
P[:,:] = computeCoefficients(rhoMinusRhoNot,
xlow,
spacestep,
numGridCells,
order,
AppParams);

exact_E = zeros((numGridCells,order));
exact_E[:,:] = computeCoefficients(exact_elecField,
xlow,
spacestep,
numGridCells,
order,
AppParams);

exact_Phi = zeros((numGridCells,order));
exact_Phi[:,:] = computeCoefficients(exact_potential,
xlow,
spacestep,
numGridCells,
order,
AppParams);

#print("Ready to start Poisson solver");
#print("P is", P);

E,Phi = poisson_solve(numGridCells,order,xlow,xhigh,P);

rel_error_E_num = 0.0;
rel_error_E_den = 0.0;
for i in range(0,numGridCells):
    for m in range(0,order):
        rel_error_E_num += (E[i,m]-exact_E[i,m])**2;
        rel_error_E_den += exact_E[i,m]**2;
rel_error_E = sqrt(rel_error_E_num/rel_error_E_den);

rel_error_Phi_num = 0.0;
rel_error_Phi_den = 0.0;
for i in range(0,numGridCells):
    for m in range(0,order):
        rel_error_Phi_num += (Phi[i,m]-exact_Phi[i,m])**2;
        rel_error_Phi_den += exact_Phi[i,m]**2;
rel_error_Phi = sqrt(rel_error_Phi_num/rel_error_Phi_den);

print("  E  relative error is", rel_error_E);
print("Phi  relative error is", rel_error_Phi);

numGridCells2 = 2*numGridCells;
spacestep2 = (xhigh-xlow)/(numGridCells2);
P2 = zeros((numGridCells2,order));
P2[:,:] = computeCoefficients(rhoMinusRhoNot,
                              xlow,
                              spacestep2,
                              numGridCells2,
                              order,
                              AppParams);

E2,Phi2 = poisson_solve(numGridCells2,order,xlow,xhigh,P2);

exact_E2 = zeros((numGridCells2,order));
exact_E2[:,:] = computeCoefficients(exact_elecField,
                                    xlow,
                                    spacestep2,
                                    numGridCells2,
                                    order,
                                    AppParams);

exact_Phi2 = zeros((numGridCells2,order));
exact_Phi2[:,:] = computeCoefficients(exact_potential,
                                      xlow,
                                      spacestep2,
                                      numGridCells2,
                                      order,
                                      AppParams);

rel_error_E_num2 = 0.0;
rel_error_E_den2 = 0.0;
for i in range(0,numGridCells2):
    for m in range(0,order):
        rel_error_E_num2 += (E2[i,m]-exact_E2[i,m])**2;
        rel_error_E_den2 += exact_E2[i,m]**2;
rel_error_E2 = sqrt(rel_error_E_num2/rel_error_E_den2);

rel_error_Phi_num2 = 0.0;
rel_error_Phi_den2 = 0.0;
for i in range(0,numGridCells2):
    for m in range(0,order):
        rel_error_Phi_num2 += (Phi2[i,m]-exact_Phi2[i,m])**2;
        rel_error_Phi_den2 += exact_Phi2[i,m]**2;
rel_error_Phi2 = sqrt(rel_error_Phi_num2/rel_error_Phi_den2);

print(" ");
print("  E2 relative error is", rel_error_E2);
print("Phi2 relative error is", rel_error_Phi2);
print(" ");
print(" log2(  E error ratio) = ",log2(rel_error_E/rel_error_E2));
print(" log2(Phi error ratio) = ",log2(rel_error_Phi/rel_error_Phi2));
print(" ");

numVariables = 1;
y = zeros([len(x),numVariables])
for i in range(0, len(x)):
    y[i,0] = Plot.findy(x[i],E, xlow, xhigh);
exact_y = zeros([len(x),numVariables])
for i in range(0, len(x)):
    exact_y[i,0] = Plot.findy(x[i], exact_E, xlow, xhigh);

plt.figure(1)
plt.plot(x,y)
plt.plot(x,exact_y)

y_pot = zeros([len(x),numVariables])
for i in range(0, len(x)):
    y_pot[i,0] = Plot.findy(x[i], Phi, xlow, xhigh);
exact_y_pot = zeros([len(x),numVariables])
for i in range(0, len(x)):
    exact_y_pot[i,0] = Plot.findy(x[i], exact_Phi, xlow, xhigh);
plt.figure(2)
plt.plot(x,y_pot)
plt.plot(x,exact_y_pot)

plt.figure(3)
Pplot = zeros(len(x));
for i in range(0, len(x)):
    Pplot[i] = Plot.findy(x[i], P, xlow, xhigh);
Pplot_exact = rhoMinusRhoNot(x,AppParams);
plt.plot(x,Pplot)
plt.plot(x,Pplot_exact)

plt.show()

dE,ddE,dddE = deriv_electricField(numGridCells,E,Q,spacestep)
