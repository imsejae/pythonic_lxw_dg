# README #

Pythonic discontinuous Galerkin Lax-Wendroff Code

### What is this repository for? ###

This code implements a Pythonic discontinuous Galerkin Lax-Wendroff scheme for solving
several nonlinear time-dependent partial differential equations
in one space-dimension:

* Burgers equation
* Shallow water equations
* Compressible Euler equations

This code is part of the following paper:

C. Felton, M. Harris, C. Logemann, S. Nelson, I. Pelakh,
and J.A. Rossmanith. A Limiting Strategy for
Locally-Implicit Lax-Wendroff Discontinuous Galerkin
Methods, 2018. Submitted (https://arxiv.org/abs/1806.06756).

This code was developed as part of the 2017 Summer REU
(Research Experience for Undergraduates) program at
Iowa State University and funded by the National
Science Foundation under the following grant: NSF Grant
DMS-1457443. The developers are (in alphabetical order):

* Camille Felton (REU Participant)
* Mariana Harris (REU Participant)
* Caleb Logeman (Graduate Student Mentor)
* Stefan Nelson (REU Participant)
* Ian Pelakh (REU Participant)
* James A. Rossmanith (Faculty Mentor)

### Who do I talk to? ###

* Admin: James A. Rossmanith (rossmani@iastate.edu)
