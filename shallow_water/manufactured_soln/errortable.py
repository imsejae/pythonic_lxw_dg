## ------------------------------------------------------------------------- ##
def errortable():

    import os
    import sys
    import numpy as np
    import errorhelper as eh

    NumMxs = 6;
    NumMorders = 3;
    StartingMorder = 3;
    NumRuns = NumMxs*NumMorders;
    Debug = False

    mx         = np.zeros(NumRuns,int);
    morder     = np.zeros(NumRuns,int);

    for m in range(0,NumMorders):
        for k in range(0,NumMxs):
            mx[m*NumMxs+k]  = 10*np.power(2,k);
            morder[m*NumMxs+k] = m+StartingMorder;

    if Debug==False:
        print " "
        isfile = os.path.isfile('run_example.py');

        if isfile==False:
            print(" ERROR: cannot find run_example.py.");
            return -1;

        for m in range(0,NumRuns):
            os.system('rm -f error.dat *.pyc output/* ../lib/*.pyc ../../lib/*.pyc;');
            print("   (%d/%d): Executing code with (mx,morder) = (%3d,%3d) ..."%(m+1,NumRuns,mx[m],morder[m]));
            eh.CreateInputFile(mx[m],morder[m]);
            os.system('pypy run_example.py');
            eh.AddToErrorTable(m);
            os.system('rm -f error.dat');
            print("   (%d/%d): Finished."%(m+1,NumRuns));
            print(" ");
        os.system('rm -f error.dat');

        eh.CreateLaTeXTable(NumMorders,NumMxs,StartingMorder);

## ------------------------------------------------------------------------- ##


#----------------------------------------------------------
if __name__=='__main__':
    """
    If executed at command line prompt, simply run errortable
    """

    errortable()
