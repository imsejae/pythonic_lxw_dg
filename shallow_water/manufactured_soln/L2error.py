#-------------------------------------------------------------------------------
def h_exact(t,x):
    from numpy import sin,pi;
    return 1.0 + 0.5*sin(pi*(x-t));
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def u_exact(t,x):
    from numpy import cos,pi;
    return cos(2.0*pi*(x-2.0*t));
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def L2error(xlow,
            dx,
            numGridCells,
            numOrder,
            time,
            Q,
            AppParams):

    from numpy import sqrt;
    from basis_data import L2Project_GaussQuadrature;

    h_exact_soln = lambda x : h_exact(time,x);
    m_exact_soln = lambda x : h_exact(time,x)*u_exact(time,x);

    h_ex_coeffs = L2Project_GaussQuadrature(h_exact_soln,
                                            xlow,
                                            dx,
                                            numGridCells,
                                            numOrder+1,
                                            numOrder+1);

    m_ex_coeffs = L2Project_GaussQuadrature(m_exact_soln,
                                            xlow,
                                            dx,
                                            numGridCells,
                                            numOrder+1,
                                            numOrder+1);

    err_h = 0.0;
    err_m = 0.0;
    l2_exact_h = 0.0;
    l2_exact_m = 0.0;
    for i in range(0,numGridCells):
        for k in range(0,numOrder):
            err_h += (Q[0,i,k]-h_ex_coeffs[i,k])**2
            err_m += (Q[1,i,k]-m_ex_coeffs[i,k])**2
            l2_exact_h += (h_ex_coeffs[i,k])**2
            l2_exact_m += (m_ex_coeffs[i,k])**2
        for k in range(numOrder,numOrder+1):
            err_h += (h_ex_coeffs[i,k])**2
            err_m += (m_ex_coeffs[i,k])**2
            l2_exact_h += (h_ex_coeffs[i,k])**2
            l2_exact_m += (m_ex_coeffs[i,k])**2

    final_err_h = sqrt(err_h/l2_exact_h);
    final_err_m = sqrt(err_m/l2_exact_m);

    final_total_error = final_err_h + final_err_m;

    filename = "error.dat";
    file = open(filename,'w');
    file.write("%i     %i     %22.15e     %22.15e\n" % \
        (numOrder,numGridCells,final_total_error,dx));
    file.close();
#-------------------------------------------------------------------------------
