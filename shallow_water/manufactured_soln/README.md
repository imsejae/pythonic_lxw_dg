# Shallow water equations: method of manufactured solutions example #

### Files:
* `errortable.py`    - (main) executes a full convergence study of method
* `run_example.py`   - (main) runs Lax-Wendroff DG method
* `plot_example.py`  - (main) plots solution from Lax-Wendroff DG method
* `parameters.py`    - (main) contains parameter values for simulation
* `errorhelper.py`   - (helper) routines for full convergence study of method
* `L2error.py`       - (helper) computes L2-error of solution

### Use the Makefile to run code:
* `make help`     : this will list all possible targets
* `make run`      : this will execute the DG code
* `make plot`     : this will plot the DG solution
* `make clean`    : this will remove all `*.pyc` and `output/*` files
* `make cleanrun` : this will clean and run
* `make all`      : this will clean, run, and plot
* `make error`    : this will carry out a convergence study

### Initial conditions:
* `h(t=0,x) = 1.0 + 0.5*sin(pi*x)`
* `u(t=0,x) = cos(2.0*pi*x)`

### Exact solutions:
* `h(t,x) = 1.0 + 0.5*sin(pi*(x-t))`
* `u(t,x) = cos(2.0*pi*x-4.0*pi*t)`

### Source terms:
* `s0(t,x) = -2.0*pi*(0.5*sin(pi*(x-t)) + 1.0)*sin(pi*(2.0*x-4.0*t))+ 0.5*pi*cos(pi*(2.0*x-4.0*t))*cos(pi*(x-t)) - 0.5*pi*cos(pi*(x-t))`
* `s1(t,x) = 0.5*pi*(0.25*sin(pi*(x-t)) + 0.5)*cos(pi*(x-t)) - 4.0*pi*(0.5*sin(pi*(x-t)) + 1.0)*sin(pi*(2.0*x-4.0*t))*cos(pi*(2.0*x-4.0*t)) + 4.0*pi*(0.5*sin(pi*(x-t)) + 1.0)*sin(pi*(2.0*x-4.0*t)) + 0.25*pi*(0.5*sin(pi*(x-t)) + 1.0)*cos(pi*(x-t)) + 0.5*pi*cos(pi*(2.0*x-4.0*t))**2*cos(pi*(x-t)) - 0.5*pi*cos(pi*(2.0*x-4.0*t))*cos(pi*(x-t))`
