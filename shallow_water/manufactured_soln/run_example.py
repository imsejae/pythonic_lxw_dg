#-------------------------------------------------------------------------------
# METHOD OF MANUFACTURED SOLUTIONS EXAMPLE
#-------------------------------------------------------------------------------

#------------------- Initial Conditions: Primitive variables -------------------
def h(x,AppParams):
    from numpy import sin,pi;
    return 1.0 + 0.5*sin(pi*x);
def u(x,AppParams):
    from numpy import cos,pi;
    return cos(2.0*pi*x);
#-------------------------------------------------------------------------------

#------------------- Initial Conditions: Conserved variables -------------------
def q0(x,AppParams):
    return h(x,AppParams);
def q1(x,AppParams):
    return h(x,AppParams)*u(x,AppParams);
#-------------------------------------------------------------------------------

#---------------------------- Source Term Functions ----------------------------
def s0(t,x,AppParams):
    from numpy import cos,sin,pi;
    return -2.0*pi*(0.5*sin(pi*(x-t)) + 1.0)*sin(pi*(2.0*x-4.0*t))\
           + 0.5*pi*cos(pi*(2.0*x-4.0*t))*cos(pi*(x-t)) - 0.5*pi*cos(pi*(x-t));
def s1(t,x,AppParams):
    from numpy import cos,sin,pi;
    return 0.5*pi*(0.25*sin(pi*(x-t)) + 0.5)*cos(pi*(x-t))\
           - 4.0*pi*(0.5*sin(pi*(x-t)) + 1.0)*sin(pi*(2.0*x-4.0*t))\
           *cos(pi*(2.0*x-4.0*t)) + 4.0*pi*(0.5*sin(pi*(x-t)) + 1.0)\
           *sin(pi*(2.0*x-4.0*t)) + 0.25*pi*(0.5*sin(pi*(x-t)) + 1.0)\
           *cos(pi*(x-t)) + 0.5*pi*cos(pi*(2.0*x-4.0*t))**2\
           *cos(pi*(x-t)) - 0.5*pi*cos(pi*(2.0*x-4.0*t))*cos(pi*(x-t));
#-------------------------------------------------------------------------------

#---------------------------- Primitive Equations Source Term Functions --------
def s0_prim(t,x,AppParams):
    from numpy import cos,sin,pi;
    return -2.0*pi*(0.5*sin(pi*(x-t)) + 1.0)*sin(pi*(2.0*x-4.0*t))\
           + 0.5*pi*cos(pi*(2.0*x-4.0*t))*cos(pi*(x-t)) - 0.5*pi*cos(pi*(x-t));
def s1_prim(t,x,AppParams):
    from numpy import cos,sin,pi;
    return 0.5*pi*(AppParams['gravity']*cos(pi*(x-t)) \
           + 8.0*sin(2.0*pi*(x-2.0*t)) - 2.0*sin(4.0*pi*(x-2.0*t)));
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
if __name__ == "__main__":

    # -----------------------------
    # GRAB SOME IMPORTANT FUNCTIONS
    # -----------------------------
    # parameter function (this sits in the current local directory)
    from parameters import parameters_init;
    # set path to main library for DG code
    import sys; sys.path.insert(0,'../../lib');
    # main Lax-Wendroff code (this sits in the main library directory)
    from LaxWendroffDG import LaxWendDG;

    # --------------
    # SET PARAMETERS
    # --------------
    from os import system;
    output_dir = "output";
    system("rm -f -r " + output_dir);
    system("mkdir " + output_dir);
    AppParams,DimParams,MeshParams,TimeParams,PredParams \
        = parameters_init(output_dir);

    # --------------------------------
    # QUICK SANITY CHECK OF PARAMETERS
    # --------------------------------
    assert(DimParams['numEqns']==2);
    assert(AppParams['gravity']>=0.0);

    # ------------------------------
    # EXECUTE LAX-WENDROFF DG METHOD
    # ------------------------------
    Q = LaxWendDG([q0,q1],           # initial condition function
                  [s0,s1],           # source term function for conservative vars
                  [s0_prim,s1_prim], # source term function for primitive vars
                  AppParams,         # application-specific parameters
                  DimParams,         # parameters determining dimensions of arrays
                  MeshParams,        # mesh-specific parameters
                  TimeParams,        # time-stepping-specific parameters
                  PredParams,        # prediction-step-specific parameters
                  output_dir,        # directory to where output is written
                  verbosity=False);  # Turn-off printing runtime info to screen

    # Compute relative L2 error and print to file "error.dat"
    from L2error import L2error;
    L2error(MeshParams['xlow'],
            MeshParams['spacestep'],
            MeshParams['numGridCells'],
            DimParams['numOrder'],
            TimeParams['endTime'],
            Q,
            AppParams);

#-------------------------------------------------------------------------------
