#----------------------------------------------------------------------------------
# plot_example.py receive .txt files "output/out*.txt" with the solution
# obtained with the Lax Wendrof DG with limiters method.
#----------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def plot(x,
         Q,
         xlow,
         xhigh,
         variables,
         t,
         AppParams):

    import numpy as np
    import matplotlib
    import matplotlib.pylab as plt
    from run_example import h,u
    import exact_riemann_solution as exact
    matplotlib.rcParams.update({'font.size': 14, 'font.family': 'serif'});

    gravity = AppParams['gravity']
    hl      = AppParams['hl'     ];
    hr      = AppParams['hr'     ];
    ul      = AppParams['ul'     ];
    ur      = AppParams['ur'     ];
    xshock  = AppParams['xshock' ];

    numVariables = len(variables)
    y = np.zeros([len(x),numVariables])
    m = np.zeros(len(x));
    for i in range(0, len(x)):
        y[i,0] = Plot.findy(x[i],Q[0,:,:], xlow, xhigh);
        m[i] = Plot.findy(x[i],Q[1,:,:], xlow, xhigh);
        y[i,1] = m[i]/y[i,0];

    h_exact, u_exact = \
        exact.exact_riemann_solution(xshock,hl,ul,hr,ur,\
                                     x,t=t,grav=gravity);

    m_exact = h_exact*u_exact;

    xticks = np.linspace(xlow,xhigh,9);

    plt.figure(1)
    plt.clf()
    plt.grid()
    ylow  = -0.1;
    yhigh =  1.1;
    yticks = np.linspace(ylow,yhigh,5);
    plt.gca().set_aspect(((xhigh-xlow)/(yhigh-ylow))*0.5)
    plt.plot(x,y[:,0], 'o', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,h_exact, label = "exact", linewidth =1.5,  color='r')
    plt.title(r'Height: $h(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    plt.ylim(ylow,yhigh)
    plt.xticks(xticks)
    plt.yticks(yticks)
    plt.legend(prop={'size':14}, loc =  'upper center')
    plt.plot(x,0*x, label = "zero", linewidth =1.5,  color='k')
    plt.savefig("num_shllw_doublerarefaction_height.pdf", format='pdf')

    plt.figure(2)
    plt.clf()
    plt.grid()
    ylow  = -2.25;
    yhigh =  2.25;
    yticks = np.linspace(ylow,yhigh,5);
    plt.gca().set_aspect(((xhigh-xlow)/(yhigh-ylow))*0.5)
    plt.plot(x,m, 'o', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,m_exact, label = "exact", linewidth =1.5,  color='r')
    plt.title(r'Momentum: $hu(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    plt.ylim(ylow,yhigh)
    plt.xticks(xticks)
    plt.yticks(yticks)
    plt.legend(prop={'size':14}, loc =  'upper left')
    plt.savefig("num_shllw_doublerarefaction_mom.pdf", format='pdf')

    plt.figure(3)
    plt.clf()
    plt.grid()
    xlow_mod  = -0.2;
    xhigh_mod =  0.2;
    xticks_mod = np.linspace(xlow_mod,xhigh_mod,9);
    ylow  =  -0.01;
    yhigh =  0.07;
    yticks = np.linspace(ylow,yhigh,5);
    plt.gca().set_aspect(((xhigh_mod-xlow_mod)/(yhigh-ylow))*0.5)
    plt.plot(x,y[:,0], 'o', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,h_exact, label = "exact", linewidth =1.5,  color='r')
    plt.title(r'Height: $h(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow_mod,xhigh_mod)
    plt.ylim(ylow,yhigh)
    plt.xticks(xticks_mod)
    plt.yticks(yticks)
    plt.legend(prop={'size':14}, loc =  'upper center')
    plt.plot(x,0*x, label = "zero", linewidth =1.5,  color='k')
    plt.savefig("num_shllw_doublerarefaction_height_zoom.pdf", format='pdf')

    plt.figure(4)
    plt.clf()
    plt.grid()
    ylow  = -0.05;
    yhigh =  0.05;
    yticks = np.linspace(ylow,yhigh,5);
    plt.gca().set_aspect(((xhigh_mod-xlow_mod)/(yhigh-ylow))*0.5)
    plt.plot(x,m, 'o', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,m_exact, label = "exact", linewidth =1.5,  color='r')
    plt.title(r'Momentum: $hu(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow_mod,xhigh_mod)
    plt.ylim(ylow,yhigh)
    plt.xticks(xticks_mod)
    plt.yticks(yticks)
    plt.legend(prop={'size':14}, loc =  'upper left')
    plt.savefig("num_shllw_doublerarefaction_mom_zoom.pdf", format='pdf')

    plt.draw()
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
if __name__== '__main__':

    # -----------------------------
    # GRAB SOME IMPORTANT FUNCTIONS
    # -----------------------------
    # parameter function (this sits in the current local directory)
    from parameters import parameters_init;
    # set path to main library for DG code
    import sys; sys.path.insert(0,'../../lib');

    # --------------
    # SET PARAMETERS
    # --------------
    output_dir = "output";
    AppParams,DimParams,MeshParams,TimeParams,PredParams \
        = parameters_init(output_dir);

    # -------------
    # PLOT SOLUTION
    # -------------
    import Plot
    Plot.plotFrames(output_dir,AppParams,plot);

#-------------------------------------------------------------------------------
