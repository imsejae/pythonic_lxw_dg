# Shallow water equations: Gaussian bump example #

### Files:
* `run_example.py`   - (main) runs Lax-Wendroff DG method
* `plot_example.py`  - (main) plots solution from Lax-Wendroff DG method
* `parameters.py`    - (main) contains parameter values for simulation

### Use the Makefile to run code:
* `make help`     : this will list all possible targets
* `make run`      : this will execute the DG code
* `make plot`     : this will plot the DG solution
* `make clean`    : this will remove all `*.pyc` and `output/*` files
* `make cleanrun` : this will clean and run
* `make all`      : this will clean, run, and plot

### Initial conditions:
* `h(t=0,x) = exp(-100.0*x**2) + 1.0`
* `u(t=0,x) = 0.0`
