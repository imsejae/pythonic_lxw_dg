#----------------------------------------------------------------------------------
# plot_example.py receive .txt files "output/out*.txt" with the solution
# obtained with the Lax Wendrof DG with limiters method.
#----------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def plot(x,
         Q,
         xlow,
         xhigh,
         variables,
         t,
         AppParams):

    import numpy as np
    import matplotlib
    import matplotlib.pylab as plt
    from run_example import h,u
    import exact_riemann_solution as exact
    matplotlib.rcParams.update({'font.size': 14, 'font.family': 'serif'})

    gravity = AppParams['gravity']
    hl      = AppParams['hl'     ];
    hr      = AppParams['hr'     ];
    ul      = AppParams['ul'     ];
    ur      = AppParams['ur'     ];
    xshock  = AppParams['xshock' ];

    numVariables = len(variables)
    y = np.zeros([len(x),numVariables])
    for i in range(0, len(x)):
        y[i,0] = Plot.findy(x[i],Q[0,:,:], xlow, xhigh);
        y[i,1] = Plot.findy(x[i],Q[1,:,:], xlow, xhigh)\
                /Plot.findy(x[i],Q[0,:,:], xlow, xhigh);
    h_exact, u_exact = \
        exact.exact_riemann_solution(xshock,hl,ul,hr,ur,x,t=t,grav=gravity);

    xticks = np.linspace(xlow,xhigh,9);

    plt.figure(1)
    plt.clf()
    plt.grid()
    ylow  = -0.1;
    yhigh =  1.2;
    yticks = np.linspace(ylow,yhigh,5);
    plt.gca().set_aspect(((xhigh-xlow)/(yhigh-ylow))*0.5)
    plt.plot(x,y[:,0], 'o', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,h_exact, label = "exact", linewidth =1.5,  color='r')
    plt.title(r'Height: $h(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    plt.ylim(ylow,yhigh)
    plt.xticks(xticks)
    plt.yticks(yticks)
    plt.legend(prop={'size':14}, loc =  'upper right')
    plt.savefig("num_shllw_dambreak_height.pdf", format='pdf')

    plt.figure(2)
    plt.clf()
    plt.grid()
    ylow  = -0.1;
    yhigh =  0.85;
    yticks = np.linspace(ylow,yhigh,5);
    plt.gca().set_aspect(((xhigh-xlow)/(yhigh-ylow))*0.5)
    plt.plot(x,y[:,1], 'o', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,u_exact, label = "exact", linewidth =1.5,  color='r')
    plt.title(r'Velocity: $u(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    plt.ylim(ylow,yhigh)
    plt.xticks(xticks)
    plt.yticks(yticks)
    plt.legend(prop={'size':14}, loc =  'upper left')
    plt.savefig("num_shllw_dambreak_velocity.pdf", format='pdf')

    plt.draw()
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
if __name__== '__main__':

    # -----------------------------
    # GRAB SOME IMPORTANT FUNCTIONS
    # -----------------------------
    # parameter function (this sits in the current local directory)
    from parameters import parameters_init;
    # set path to main library for DG code
    import sys; sys.path.insert(0,'../../lib');

    # --------------
    # SET PARAMETERS
    # --------------
    output_dir = "output";
    AppParams,DimParams,MeshParams,TimeParams,PredParams \
        = parameters_init(output_dir);

    # -------------
    # PLOT SOLUTION
    # -------------
    import Plot
    Plot.plotFrames(output_dir,AppParams,plot);

#-------------------------------------------------------------------------------
