# Shallow water equations #

### Sub-directories:
* `lib/`                      : library files relevant to all examples
* `double_rarefaction/`       : Riemann problem with two rarefactions and a vacuum state
* `dambreak/`                 : Riemann problem with 1-rarefaction and a 2-shock
* `gaussian_bump/`            : smooth initial data that shocks
* `gaussian_bump_no_limiter/` : smooth initial data that shocks (no limiters)
* `manufactured_soln/`        : forced shallow water equations with convergence test

### Variables:
* `t`                : time
* `x`                : spatial coordinate
* `g`                : gravitational constant
* `h(t,x)`           : thickness of fluid layer
* `u(t,x)`           : depth-averaged macroscopic fluid velocity
* `m(t,x) = h*u`     : depth-averaged macroscopic fluid momentum
* `s0(t,x), s1(t,x)` : source term functions

### Equations:
*   `(h)_{,t} + (h*u)_{,x}               = s0`     : conservation of mass
* `(h*u)_{,t} + (h*u^2 + 0.5*g*h^2)_{,x} = s1`     : conservation of momentum

### Primitive Equation Jacobian:
* `A = array([[u, h], [g, u]]);`

### Eigenvalues of Primitive Equation Jacobian:
* `lambda = u-sqrt(g*h), u+sqrt(g*h)`
