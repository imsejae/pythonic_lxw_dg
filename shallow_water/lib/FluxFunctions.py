#------------------- Convert from Conservative to Primitive Variables ----------
def GetPrimFromCons(Q_at_QuadPts,
                    numEqns,
                    numQuadPts,
                    AppParams):

    from numpy import zeros,max;
    Prim_at_QuadPts = zeros([numEqns,
                             numQuadPts]);

    for nq in range(0,numQuadPts):
        h   = Q_at_QuadPts[0,nq];
        mom = Q_at_QuadPts[1,nq];

        u = mom/max([1.0e-10,h]); # desingularize velocity

        Prim_at_QuadPts[0,nq] = h
        Prim_at_QuadPts[1,nq] = u;

    return Prim_at_QuadPts;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def FluxFunction(W,
                 i,
                 tauIndex,
                 xiIndex,
                 numPredBasis,
                 psiAtQuadP,
                 AppParams):

    from basis_data import FindPrimitiveVar;
    from numpy import array;

    grav = AppParams['gravity'];

    h = FindPrimitiveVar(W[:numPredBasis,:], tauIndex, xiIndex, i,
                         numPredBasis, psiAtQuadP);
    u = FindPrimitiveVar(W[numPredBasis:,:], tauIndex, xiIndex, i,
                         numPredBasis, psiAtQuadP);

    return array([h*u,h*u**2+0.5*grav*h**2]);
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def EvalPrimJacobian(numEqns,
                     numOrder,
                     AppParams,
                     primvars):

    from numpy import zeros;

    primJac  = zeros([numEqns,numEqns,numOrder,numOrder]);
    grav = AppParams['gravity'];

    for a in range(0,numOrder):
        for b in range(0,numOrder):
            h = primvars[0,a,b];
            u = primvars[1,a,b];

            primJac[0,0,a,b] = u;
            primJac[0,1,a,b] = h;
            primJac[1,0,a,b] = grav;
            primJac[1,1,a,b] = u;

    return primJac;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def NumericalFlux(i,
                  W,
                  tauIndex,
                  numGridCells,
                  numEqns,
                  numPredBasis,
                  boundary,
                  AppParams,
                  QuadData):

    # here W1 and W2 are 2 dimensional because they store over all
    # the grid cells this returns the unitegrated form of flux

    from numpy import sqrt,zeros,max,abs;

    grav = AppParams['gravity'];

    if boundary == 'periodic':
        # these are the periodic boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1));
        iMod = i*(i>=0)+(i<0)*(numGridCells-1);
    elif boundary == 'extrapolation':
        # these are the 0 derivative boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1))+(i==(numGridCells-1))*i;
        iMod = i*(i>=0)+(i<0)*(0);

    W1 = W[:numPredBasis,:];
    W2 = W[numPredBasis:,:];

    NumFlux = zeros(numEqns);
    ul = 0.0; # velocity of left state
    hl = 0.0; # height of left state
    ur = 0.0; # velocity of right state
    hr = 0.0; # height of right state

    for k in range(0,numPredBasis):
        # k is the basis function index
        hl += QuadData['psiAtQuadRight'][k,tauIndex]*W1[k,iMod]; # height of left state
        ul += QuadData['psiAtQuadRight'][k,tauIndex]*W2[k,iMod]; # velocity of left state
        hr += QuadData['psiAtQuadLeft' ][k,tauIndex]*W1[k,ip1];  # height of right state
        ur += QuadData['psiAtQuadLeft' ][k,tauIndex]*W2[k,ip1];  # velocity of right state

    if hl <=0.0 or hr <=0.0:
        print "Error: hl or hr was negative in flux";
        print "    hl = ",hl;
        print "    hr = ",hr;
        print W1[:,iMod];
        raise

    hl_mod = max([hl,1.0e-10]);  # desingularize h for wave speed only
    hr_mod = max([hr,1.0e-10]);  # desingularize h for wave speed only
    ul_abs = abs(ul);
    ur_abs = abs(ur);
    lambdaMax = max([ul_abs+sqrt(grav*hl_mod),\
                     ur_abs+sqrt(grav*hr_mod),\
                     0.5*(ur_abs+ul_abs)+sqrt(0.5*grav*(hl_mod+hr_mod))]);

    # quadLeft refers to the left most point of the i+1 cell
    # and quad right refers to the right point of the i cell

    mom_r = hr*ur - lambdaMax*hr; # this is h*u at the right
    mom_l = hl*ul + lambdaMax*hl; # this is h*u at the left
    nrg_r = hr*ur**2 + 0.5*grav*hr**2 - lambdaMax*hr*ur; # this is f_2 at i+1: hu^2+.5gh^2
    nrg_l = hl*ul**2 + 0.5*grav*hl**2 + lambdaMax*hl*ul; # this is f_2 at   i: hu^2+.5gh^2
    NumFlux[0] = 0.5 * ( mom_r + mom_l );
    NumFlux[1] = 0.5 * ( nrg_r + nrg_l );

    return NumFlux,lambdaMax;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def NumericalFlux_LxF(i,
                      Q,
                      numGridCells,
                      numEqns,
                      boundary,
                      AppParams):

    from numpy import sqrt,zeros,max,abs;

    grav = AppParams['gravity'];

    if boundary == 'periodic':
        # these are the periodic boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1));
        iMod = i*(i>=0)+(i<0)*(numGridCells-1);
    elif boundary == 'extrapolation':
        # these are the 0 derivative boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1))+(i==(numGridCells-1))*i;
        iMod = i*(i>=0)+(i<0)*(0);

    hl = Q[0,iMod,0];
    hl_mod = max([hl,1.0e-10]);
    ul = Q[1,iMod,0]/hl_mod;

    hr = Q[0,ip1,0];
    hr_mod = max([hr,1.0e-10]);
    ur = Q[1,ip1,0]/hr_mod;

    NumFlux = zeros(numEqns);

    ul_abs = abs(ul);
    ur_abs = abs(ur);
    lambdaMax = max([ul_abs+sqrt(grav*hl_mod),\
                     ur_abs+sqrt(grav*hr_mod),\
                     0.5*(ur_abs+ul_abs)+sqrt(0.5*grav*(hl_mod+hr_mod))]);

    # quadLeft refers to the left most point of the i+1 cell
    # and quad right refers to the right point of the i cell

    mom_r = hr*ur - lambdaMax*hr; # this is h*u at the right
    mom_l = hl*ul + lambdaMax*hl; # this is h*u at the left
    nrg_r = hr*ur**2 + 0.5*grav*hr**2 - lambdaMax*hr*ur; # this is f_2 at i+1: hu^2+.5gh^2
    nrg_l = hl*ul**2 + 0.5*grav*hl**2 + lambdaMax*hl*ul; # this is f_2 at   i: hu^2+.5gh^2
    NumFlux[0] = 0.5 * ( mom_r + mom_l );
    NumFlux[1] = 0.5 * ( nrg_r + nrg_l );

    return NumFlux;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def SourceFunction(t_i,
                   x_i,
                   timestep,
                   spacestep,
                   tauIndex,
                   xiIndex,
                   xi,
                   s,
                   AppParams):

    from numpy import array;

    return array([s[0](t_i+0.5*timestep*xi[tauIndex],
                       x_i+0.5*spacestep*xi[xiIndex],
                       AppParams),
                  s[1](t_i+0.5*timestep*xi[tauIndex],
                       x_i+0.5*spacestep*xi[xiIndex],
                       AppParams)]);
#-------------------------------------------------------------------------------
