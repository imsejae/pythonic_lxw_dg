#------------------------------------------------------------------------------
def LimitAverageFlux(nu,
                     Q_bar,
                     Q_LxF,
                     dFlux_lft,
                     dFlux_rgt,
                     NumFlux_lft,
                     NumFlux_rgt,
                     AppParams,
                     epsilon):

    from numpy import min,abs;

    Gamma = (Q_LxF[0]-epsilon)/nu;

    if dFlux_lft[0]<0.0 and dFlux_rgt[0]<0.0:

        Lambda_lft = min([1.0,Gamma/(abs(dFlux_lft[0])+abs(dFlux_rgt[0]))]);
        Lambda_rgt = 0.0+Lambda_lft;

    elif dFlux_lft[0]<0.0:

        Lambda_lft = min([1.0,Gamma/abs(dFlux_lft[0])]);
        Lambda_rgt = 1.0;

    elif dFlux_rgt[0]<0.0:

        Lambda_lft = 1.0;
        Lambda_rgt = min([1.0,Gamma/abs(dFlux_rgt[0])]);

    else:

        Lambda_lft = 1.0;
        Lambda_rgt = 1.0;

    return Lambda_lft,Lambda_rgt;
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def PredictionStep_Positivity_Limiter(Win,
                                      numGridCells,
                                      numOrder,
                                      numPredBasis,
                                      numEqns,
                                      epsilon,
                                      AppParams,
                                      QuadData):

    num_psiAtPositivityPts = QuadData['num_psiAtPosPts'];
    psiAtPositivityPts     = QuadData['psiAtPosPts'    ];

    from numpy import min,copy;

    Wout = copy(Win);

    for i in range (0,numGridCells):

        h_ave = Win[0,i];
        h_min = h_ave;

        if h_ave < epsilon:
            print ("ERROR: average height in PREDICTION is less than epsilon")
            print "i = %i, h_ave = %24.16e\n" % (i,h_ave);
            raise

        for nq in range(0,num_psiAtPositivityPts):
            h_val = h_ave;
            for ell in range (1,numPredBasis):
                h_val += psiAtPositivityPts[ell,nq] * Win[ell,i];
            h_min = min([h_min,h_val]);

        if h_min>=epsilon:
            theta = 1.0;
        else:
            theta = min([1.0, (h_ave-epsilon)/(h_ave-h_min)]);
            for ell in range (1,numPredBasis):
                Wout[ell,i] *= theta;
            for ell in range (numPredBasis+1,2*numPredBasis):
                Wout[ell,i] *= theta;

    return Wout;
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def CorrectionStep_Positivity_Limiter(Qin,
                                      numGridCells,
                                      numOrder,
                                      numEqns,
                                      epsilon,
                                      AppParams,
                                      num_phiAtPosPts,
                                      phiAtPosPts):

    from numpy import isnan,min,copy;

    Qout = copy(Qin);

    for i in range (0,numGridCells):

        h_ave = Qin[0,i,0];
        h_min = h_ave;

        if h_ave < epsilon:
            print ("ERROR: average height in CORRECTION is less than epsilon")
            print "i = %i, h_ave = %24.16e\n" % (i,h_ave);
            raise

        for ell in range (0,num_phiAtPosPts):
            h_val = h_ave;

            for k in range (1,numOrder):
                h_val += Qin[0,i,k] * phiAtPosPts[k,ell];

            h_min = min([h_min,h_val]);

        if h_min>epsilon:
            theta = 1.0;
        else:
            theta = min([1.0, (h_ave - epsilon)/(h_ave - h_min)]);
            for ell in range (1,numOrder):
                Qout[0,i,ell] *= theta;
                Qout[1,i,ell] *= theta;

    return Qout;
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def CorrectionStep_Moment_Limiter(Q,
                                  numOrder,
                                  numEqns,
                                  MeshParams,
                                  AppParams,
                                  epsilon,
                                  num_phiAtLimPts,
                                  phiAtLimPts):

    # convert to characteristic variables
    dC_right,dC_left,C_cent = convert_to_char_vars(Q,
                                                   MeshParams['numGridCells'],
                                                   numOrder,
                                                   numEqns,
                                                   MeshParams['boundary'],
                                                   AppParams);

    # hierarchical minmod limiter on each characteristic variable
    for i in range(0,MeshParams['numGridCells']):

        for m in range(0,numEqns):
            mstop = 0;
            ell = numOrder-2;

            while (mstop==0):

                C_unlimited = C_cent[m,i,ell];
                C_limited = minmod(  C_unlimited,
                                     dC_left[m,i,ell],
                                    dC_right[m,i,ell] );
                C_cent[m,i,ell] = C_limited;

                if abs(C_limited-C_unlimited)>epsilon or \
                   abs(C_limited)<=epsilon:
                    ell -= 1;
                    if ell == -1:
                        mstop = 1;
                else:
                    mstop = 1;

    # convert back to conservative variables
    Qout = convert_to_cons_vars(C_cent,
                                Q,
                                MeshParams['numGridCells'],
                                numOrder,
                                numEqns,
                                AppParams);

    return Qout;
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def convert_to_char_vars(Q,
                         numGridCells,
                         numOrder,
                         numEqns,
                         boundary,
                         AppParams):

    from numpy import zeros,arange,sqrt,array,dot,abs,max;
    grav = AppParams['gravity'];

    # Characteristic variable storage
    C_cent   = zeros([numEqns,numGridCells,numOrder-1]);
    dC_right = zeros([numEqns,numGridCells,numOrder-1]);
    dC_left  = zeros([numEqns,numGridCells,numOrder-1]);

    ip1_vec = arange(numGridCells)+1;
    im1_vec = arange(numGridCells)-1;
    if boundary=='periodic':
        ip1_vec[numGridCells-1] = 0;
        im1_vec[0] = numGridCells-1;
    elif boundary=='extrapolation':
        ip1_vec[numGridCells-1] = numGridCells-1;
        im1_vec[0] = 0;

    sc = zeros(numOrder-1);
    for ell in range(0,numOrder-1):
        sc[ell] = sqrt((2.0*ell+1.0)/(2.0*ell+3.0));

    for i in range(0,numGridCells):

        ip1 = ip1_vec[i];
        im1 = im1_vec[i];

        h_ave = max([1.0e-6,Q[0,i,0]]);
        u_ave = Q[1,i,0]/h_ave;

        Rinv = (-0.5/sqrt(grav*h_ave))*\
            array([[ u_ave-sqrt(grav*h_ave), -1.0],
                   [-u_ave-sqrt(grav*h_ave),  1.0]]);

        for ell in range(0,numOrder-1):
            C_cent[:,i,ell]   =         dot(Rinv, Q[:,i,ell+1]                );
            dC_left[:,i,ell]  = sc[ell]*dot(Rinv, Q[:,i,  ell] - Q[:,im1,ell] );
            dC_right[:,i,ell] = sc[ell]*dot(Rinv, Q[:,ip1,ell] - Q[:,i,  ell] );

    return dC_right,dC_left,C_cent
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def convert_to_cons_vars(Cin,
                         Qin,
                         numGridCells,
                         numOrder,
                         numEqns,
                         AppParams):

    from numpy import copy,array,dot,sqrt,max;
    grav = AppParams['gravity'];

    # Conservative variable storage
    Qout = copy(Qin);

    for i in range(0,numGridCells):

        h_ave = max([1.0e-6,Qin[0,i,0]]); #Qin[0,i,0];
        u_ave = Qin[1,i,0]/h_ave;

        R = array([[1.0,                    1.0                   ],
                   [u_ave+sqrt(grav*h_ave), u_ave-sqrt(grav*h_ave)]]);

        for ell in range(1,numOrder):
            Qout[:,i,ell] = dot(R, Cin[:,i,ell-1]);

    return Qout;
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def minmod(a,b,c):
    from numpy import sign,min;

    if a*b<0.0 or b*c<0.0 or a*c<0.0:
        return 0.0

    return sign(a)*min([abs(a),abs(b),abs(c)]);
#------------------------------------------------------------------------------
