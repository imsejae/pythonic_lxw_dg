#------------------------------------------------------------------------------
def LimitAverageFlux(nu,
                     Q_bar,
                     Q_LxF,
                     dFlux_lft,
                     dFlux_rgt,
                     NumFlux_lft,
                     NumFlux_rgt,
                     AppParams,
                     epsilon):

    from numpy import min,abs,isnan;

    ###########################################
    # PART I: density
    ###########################################
    Gamma = (Q_LxF[0]-epsilon)/nu;

    if dFlux_lft[0]<0.0 and dFlux_rgt[0]<0.0:

        Lambda_lft = min([1.0,Gamma/(abs(dFlux_lft[0])+abs(dFlux_rgt[0]))]);
        Lambda_rgt = 0.0+Lambda_lft;

    elif dFlux_lft[0]<0.0:

        Lambda_lft = min([1.0,Gamma/abs(dFlux_lft[0])]);
        Lambda_rgt = 1.0;

    elif dFlux_rgt[0]<0.0:

        Lambda_lft = 1.0;
        Lambda_rgt = min([1.0,Gamma/abs(dFlux_rgt[0])]);

    else:

        Lambda_lft = 1.0;
        Lambda_rgt = 1.0;

    ###########################################
    # PART II: pressure
    ###########################################

    gamma = AppParams['gamma'];
    p_LxF = (gamma-1.0)*(Q_LxF[2]-0.5*(Q_LxF[1]**2)/Q_LxF[0]);
    mu_11 = 1.0;
    mu_10 = 1.0;
    mu_01 = 1.0;

    # case 1:  alpha[0] = 1   and    alpha[1] = 1
    qc = Q_bar - nu*(Lambda_rgt*NumFlux_rgt - Lambda_lft*NumFlux_lft);
    pc = (gamma-1.0)*(qc[2]-0.5*(qc[1]**2)/qc[0]);
    if pc<=epsilon:
        mu_11 = (p_LxF-epsilon)/(p_LxF-pc);

    # case 2:  alpha[0] = 1   and    alpha[1] = 0
    qc = Q_bar + nu*(Lambda_lft*NumFlux_lft);
    pc = (gamma-1.0)*(qc[2]-0.5*(qc[1]**2)/qc[0]);
    if pc<=epsilon:
        mu_10 = (p_LxF-epsilon)/(p_LxF-pc);

    # case 3:  alpha[0] = 0   and    alpha[1] = 1
    qc = Q_bar - nu*(Lambda_rgt*NumFlux_rgt);
    pc = (gamma-1.0)*(qc[2]-0.5*(qc[1]**2)/qc[0]);
    if pc<=epsilon:
        mu_01 = (p_LxF-epsilon)/(p_LxF-pc);

    # take minimum mu
    mu = min([mu_11,mu_10,mu_01]);

    # rescale Lambdas
    Lambda_lft *= mu;
    Lambda_rgt *= mu;

    return Lambda_lft,Lambda_rgt;
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def PredictionStep_Positivity_Limiter(Win,
                                      numGridCells,
                                      numOrder,
                                      numPredBasis,
                                      numEqns,
                                      epsilon,
                                      AppParams,
                                      QuadData):

    num_psiAtPositivityPts = QuadData['num_psiAtPosPts'];
    psiAtPositivityPts     = QuadData['psiAtPosPts'    ];

    from numpy import min,copy;

    Wout = copy(Win);

    for i in range (0,numGridCells):

        rho_ave = Win[0,i];
        rho_min = rho_ave;

        if rho_ave < epsilon:
            print("ERROR: average density in PREDICTION is less than epsilon");
            print("i = %i, rho_ave = %24.16e\n" % (i,rho_ave));
            raise

        for nq in range(0,num_psiAtPositivityPts):
            rho_val = rho_ave;
            for ell in range (1,numPredBasis):
                rho_val += psiAtPositivityPts[ell,nq] * Win[ell,i];
            rho_min = min([rho_min,rho_val]);

        p_ave = Win[2*numPredBasis,i];
        p_min = p_ave;

        if p_ave < epsilon:
            print("ERROR: average pressure in PREDICTION is less than epsilon");
            print("i = %i, p_ave = %24.16e\n" % (i,p_ave));
            raise

        for nq in range(0,num_psiAtPositivityPts):
            p_val = p_ave;
            ell_mod = 0;
            for ell in range (2*numPredBasis+1,3*numPredBasis):
                p_val += psiAtPositivityPts[ell_mod,nq] * Win[ell,i];
                ell_mod += 1;
            p_min = min([p_min,p_val]);

        if rho_min>=epsilon and p_min>=epsilon:
            theta = 1.0;
        else:
            theta = min([1.0, \
                         (rho_ave-epsilon)/(rho_ave-rho_min), \
                         (p_ave-epsilon)/(p_ave-p_min)]);

            for ell in range (1,numPredBasis):
                Wout[ell,i] *= theta;
            for ell in range (numPredBasis+1,2*numPredBasis):
                Wout[ell,i] *= theta;
            for ell in range (2*numPredBasis+1,3*numPredBasis):
                Wout[ell,i] *= theta;

    return Wout;
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def CorrectionStep_Positivity_Limiter(Qin,
                                      numGridCells,
                                      numOrder,
                                      numEqns,
                                      epsilon,
                                      AppParams,
                                      num_phiAtPosPts,
                                      phiAtPosPts):

    from numpy import copy;
    gamma = AppParams['gamma'];
    Qout = copy(Qin);

    for i in range (0,numGridCells):

        rho_ave = Qin[0,i,0];
        mom_ave = Qin[1,i,0];
        nrg_ave = Qin[2,i,0];
        p_ave   = (gamma-1.0)*(nrg_ave - 0.5*(mom_ave**2)/rho_ave);

        rho_min = rho_ave;
        p_min   = p_ave;

        if rho_ave < epsilon:
            print("ERROR: average density in CORRECTION is less than epsilon");
            print("i = %i, rho_ave = %24.16e\n" % (i,rho_ave));
            raise

        if p_ave < epsilon:
            print("ERROR: average pressure in CORRECTION is less than epsilon");
            print("i = %i, p_ave = %24.16e\n" % (i,p_ave));
            raise

        # LIMIT TO GET POSITIVE DENSITY
        for ell in range (0,num_phiAtPosPts):
            rho_val = rho_ave;

            for k in range (1,numOrder):
                rho_val += Qin[0,i,k] * phiAtPosPts[k,ell];

            rho_min = min([rho_min,rho_val]);

        if rho_min>epsilon:
            theta = 1.0;
        else:
            theta = min([1.0, (rho_ave - epsilon)/(rho_ave - rho_min)]);
            for m in range(0,numEqns):
                for ell in range (1,numOrder):
                    Qout[m,i,ell] *= theta;

        # LIMIT TO GET POSITIVE PRESSURE
        for ell in range (0,num_phiAtPosPts):
            rho_val = Qout[0,i,0];
            mom_val = Qout[1,i,0];
            nrg_val = Qout[2,i,0];
            for k in range (1,numOrder):
                rho_val += Qout[0,i,k] * phiAtPosPts[k,ell];
                mom_val += Qout[1,i,k] * phiAtPosPts[k,ell];
                nrg_val += Qout[2,i,k] * phiAtPosPts[k,ell];

            p_val = (gamma-1.0)*(nrg_val - 0.5*(mom_val**2)/rho_val);
            p_min = min([p_min,p_val]);

        if p_min>epsilon:
            theta = 1.0;
        else:
            theta = min([1.0, (p_ave - epsilon)/(p_ave - p_min)]);
            for m in range(0,numEqns):
                for ell in range (1,numOrder):
                    Qout[m,i,ell] *= theta;

    return Qout;
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def CorrectionStep_Moment_Limiter(Q,
                                  numOrder,
                                  numEqns,
                                  MeshParams,
                                  AppParams,
                                  epsilon,
                                  num_phiAtLimPts,
                                  phiAtLimPts):

    # convert to characteristic variables
    dC_right,dC_left,C_cent = convert_to_char_vars(Q,
                                                   MeshParams['numGridCells'],
                                                   numOrder,
                                                   numEqns,
                                                   MeshParams['boundary'],
                                                   AppParams);

    # hierarchical minmod limiter on each characteristic variable
    for i in range(0,MeshParams['numGridCells']):

        for m in range(0,numEqns):
            mstop = 0;
            ell = numOrder-2;

            while (mstop==0):

                C_unlimited = C_cent[m,i,ell];
                C_limited = minmod(  C_unlimited,
                                     dC_left[m,i,ell],
                                    dC_right[m,i,ell] );
                C_cent[m,i,ell] = C_limited;

                if abs(C_limited-C_unlimited)>epsilon or \
                   abs(C_limited)<=epsilon:
                    ell -= 1;
                    if ell == -1:
                        mstop = 1;
                else:
                    mstop = 1;

    # convert back to conservative variables
    Qout = convert_to_cons_vars(C_cent,
                                Q,
                                MeshParams['numGridCells'],
                                numOrder,
                                numEqns,
                                AppParams);

    return Qout;
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def convert_to_char_vars(Q,
                         numGridCells,
                         numOrder,
                         numEqns,
                         boundary,
                         AppParams):

    from numpy import zeros,arange,sqrt,array,dot,abs,max;
    gamma = AppParams['gamma'];

    # Characteristic variable storage
    C_cent   = zeros([numEqns,numGridCells,numOrder-1]);
    dC_right = zeros([numEqns,numGridCells,numOrder-1]);
    dC_left  = zeros([numEqns,numGridCells,numOrder-1]);

    ip1_vec = arange(numGridCells)+1;
    im1_vec = arange(numGridCells)-1;
    if boundary=='periodic':
        ip1_vec[numGridCells-1] = 0;
        im1_vec[0] = numGridCells-1;
    elif boundary=='extrapolation':
        ip1_vec[numGridCells-1] = numGridCells-1;
        im1_vec[0] = 0;

    sc = zeros(numOrder-1);
    for ell in range(0,numOrder-1):
        sc[ell] = sqrt((2.0*ell+1.0)/(2.0*ell+3.0));

    for i in range(0,numGridCells):

        ip1 = ip1_vec[i];
        im1 = im1_vec[i];

        rho = max([1.0e-6,Q[0,i,0]]);
        u   = Q[1,i,0]/rho;
        p   = max([1.0e-6,(gamma-1.0)*(Q[2,i,0]-0.5*rho*u**2)]);
        cs  = sqrt(gamma*p/rho);

        Rinv = array([[(2.0*gamma*p + rho*u**2 - gamma*rho*u**2)/(2.0*gamma*p),
                       ((gamma-1.0)*rho*u)/(gamma*p),
                       (((1.0 - gamma)*rho)/(gamma*p))],
                      [(u*(2.0*gamma*p - cs*rho*u + cs*gamma*rho*u))/(4.0*cs*gamma*p),
                       -((gamma*p - cs*rho*u + cs*gamma*rho*u)/(2.0*cs*gamma*p)),
                       ((gamma-1.0)*rho)/(2.0*gamma*p)],
                      [(u*(-2.0*gamma*p - cs*rho*u + cs*gamma*rho*u))/(4.0*cs*gamma*p),
                       -((-gamma*p - cs*rho*u + cs*gamma*rho*u)/(2.0*cs*gamma*p)),
                       ((-1.0 + gamma)*rho)/(2.0*gamma*p)]]);

        for ell in range(0,numOrder-1):
            C_cent[:,i,ell]   =         dot(Rinv, Q[:,i,ell+1]                );
            dC_left[:,i,ell]  = sc[ell]*dot(Rinv, Q[:,i,  ell] - Q[:,im1,ell] );
            dC_right[:,i,ell] = sc[ell]*dot(Rinv, Q[:,ip1,ell] - Q[:,i,  ell] );

    return dC_right,dC_left,C_cent
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def convert_to_cons_vars(Cin,
                         Qin,
                         numGridCells,
                         numOrder,
                         numEqns,
                         AppParams):

    from numpy import copy,array,dot,sqrt,max;
    gamma = AppParams['gamma'];

    # Conservative variable storage
    Qout = copy(Qin);

    for i in range(0,numGridCells):

        rho = Qin[0,i,0]; #max([1.0e-6,Qin[0,i,0]]);
        u   = Qin[1,i,0]/rho;
        p   = (gamma-1.0)*(Qin[2,i,0]-0.5*rho*u**2); #max([1.0e-6,(gamma-1.0)*(Qin[2,i,0]-0.5*rho*u**2)]);
        cs  = sqrt(gamma*p/rho);

        R = array([[1.0, 1.0,  1.0],
                   [u,   u-cs, u+cs],
                   [0.5*u**2,
                    p*gamma/((gamma-1.0)*rho) + 0.5*u**2 - u*cs,
                    p*gamma/((gamma-1.0)*rho) + 0.5*u**2 + u*cs]]);

        for ell in range(1,numOrder):
            Qout[:,i,ell] = dot(R, Cin[:,i,ell-1]);

    return Qout;
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def minmod(a,b,c):
    from numpy import sign,min;

    if a*b<0.0 or b*c<0.0 or a*c<0.0:
        return 0.0

    return sign(a)*min([abs(a),abs(b),abs(c)]);
#------------------------------------------------------------------------------
