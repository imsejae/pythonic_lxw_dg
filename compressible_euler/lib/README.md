# Compressible Euler equations: library files #

### Files:
* `FluxFunctions.py`     : routines to evaluate fluxes, primitive variables, and source terms
* `LimiterFunctions.py`  : routines to apply prediction and correction limiters
