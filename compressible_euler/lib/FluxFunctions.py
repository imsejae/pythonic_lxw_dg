#------------------- Convert from Conservative to Primitive Variables ----------
def GetPrimFromCons(Q_at_QuadPts,
                    numEqns,
                    numQuadPts,
                    AppParams):

    from numpy import zeros,max;
    Prim_at_QuadPts = zeros([numEqns,
                             numQuadPts]);

    gamma = AppParams['gamma'];

    for nq in range(0,numQuadPts):
        rho    = Q_at_QuadPts[0,nq];
        mom    = Q_at_QuadPts[1,nq];
        energy = Q_at_QuadPts[2,nq];

        u = mom/max([1.0e-10,rho]); # desingularize velocity
        p = (gamma-1.0)*(energy - 0.5*mom*mom/rho);

        Prim_at_QuadPts[0,nq] = rho;
        Prim_at_QuadPts[1,nq] = u;
        Prim_at_QuadPts[2,nq] = p;

    return Prim_at_QuadPts;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def FluxFunction(W,
                 i,
                 tauIndex,
                 xiIndex,
                 numPredBasis,
                 psiAtQuadP,
                 AppParams):

    from basis_data import FindPrimitiveVar;
    from numpy import array;

    rho = FindPrimitiveVar(W[:numPredBasis,:],
                           tauIndex,
                           xiIndex,
                           i,
                           numPredBasis,
                           psiAtQuadP);
    u   = FindPrimitiveVar(W[numPredBasis:2*numPredBasis,:],
                           tauIndex,
                           xiIndex,
                           i,
                           numPredBasis,
                           psiAtQuadP);
    p   = FindPrimitiveVar(W[2*numPredBasis:,:],
                           tauIndex,
                           xiIndex,
                           i,
                           numPredBasis,
                           psiAtQuadP);

    gamma = AppParams['gamma'];

    return array([rho*u,rho*u**2+p,u*(gamma/(gamma-1.0)*p+0.5*rho*u**2)])
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def EvalPrimJacobian(numEqns,
                     numOrder,
                     AppParams,
                     primvars):

    from numpy import zeros;

    primJac  = zeros([numEqns,numEqns,numOrder,numOrder]);
    gamma = AppParams['gamma'];

    for a in range(0,numOrder):
        for b in range(0,numOrder):
            rho = primvars[0,a,b];
            u   = primvars[1,a,b];
            p   = primvars[2,a,b];

            primJac[0,0,a,b] = u;
            primJac[0,1,a,b] = rho;
            primJac[0,2,a,b] = 0.0;

            primJac[1,0,a,b] = 0.0;
            primJac[1,1,a,b] = u;
            primJac[1,2,a,b] = 1.0/rho;

            primJac[2,0,a,b] = 0.0;
            primJac[2,1,a,b] = gamma*p;
            primJac[2,2,a,b] = u;

    return primJac;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def NumericalFlux(i,
                  W,
                  tauIndex,
                  numGridCells,
                  numEqns,
                  numPredBasis,
                  boundary,
                  AppParams,
                  QuadData):
    # here W1, W2, and W3 are 2 dimensional because they store over all
    # the grid cells this returns the unitegrated form of flux

    from numpy import sqrt,abs,max,zeros;

    if boundary == 'periodic':
        # these are the periodic boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1))
        iMod = i*(i>=0)+(i<0)*(numGridCells-1)
    elif boundary == 'extrapolation':
        # these are the 0 derivative boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1))+(i==(numGridCells-1))*i
        iMod = i*(i>=0)+(i<0)*(0)

    W1 = W[:numPredBasis,:]
    W2 = W[numPredBasis:2*numPredBasis,:]
    W3 = W[2*numPredBasis:,:]

    NumFlux = zeros(numEqns);
    rhoL = 0.0; # density of left state
    uL   = 0.0; # velocity of left state
    pL   = 0.0; # pressure of left state
    rhoR = 0.0; # density of right state
    uR   = 0.0; # velocity of right state
    pR   = 0.0; # pressure of right state

    for k in range(0,numPredBasis):
        # k is the basis function index
        rhoL += QuadData['psiAtQuadRight'][k,tauIndex]*W1[k,iMod];
        uL   += QuadData['psiAtQuadRight'][k,tauIndex]*W2[k,iMod];
        pL   += QuadData['psiAtQuadRight'][k,tauIndex]*W3[k,iMod];
        rhoR += QuadData['psiAtQuadLeft'][k,tauIndex]* W1[k,ip1];
        uR   += QuadData['psiAtQuadLeft'][k,tauIndex]* W2[k,ip1];
        pR   += QuadData['psiAtQuadLeft'][k,tauIndex]* W3[k,ip1];

    gamma     = AppParams['gamma'];
    rhol_mod  = max([rhoL,1.0e-10]);  # desingularize rho for wave speed only
    rhor_mod  = max([rhoR,1.0e-10]);  # desingularize rho for wave speed only
    ul_abs    = abs(uL);
    ur_abs    = abs(uR);
    pl_mod    = max([pL,1.0e-10]);  # desingularize rho for wave speed only
    pr_mod    = max([pR,1.0e-10]);  # desingularize rho for wave speed only
    lambdaMax = max([ul_abs+sqrt(gamma*pl_mod/rhol_mod),
                     ur_abs+sqrt(gamma*pr_mod/rhor_mod),
                     0.5*(ur_abs+ul_abs)+sqrt(gamma*(pl_mod+pr_mod)/(rhol_mod+rhor_mod))]);

    NumFlux[0] = 0.5*(rhoR*uR + rhoL*uL - lambdaMax*(rhoR-rhoL));
    NumFlux[1] = 0.5*(rhoR*uR**2 + pR + rhoL*uL**2 + pL - lambdaMax*(rhoR*uR - rhoL*uL));
    NumFlux[2] = 0.5*(uR*(gamma/(gamma-1.0)*pR + 0.5*rhoR*uR**2) \
          + uL*(gamma/(gamma-1.0)*pL + 0.5*rhoL*uL**2) \
          - lambdaMax*(pR/(gamma-1.0) + 0.5*rhoR*uR**2 \
          - pL/(gamma-1.0) - 0.5*rhoL*uL**2));

    return NumFlux,lambdaMax
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def NumericalFlux_LxF(i,
                      Q,
                      numGridCells,
                      numEqns,
                      boundary,
                      AppParams):

    from numpy import sqrt,abs,max,zeros;

    if boundary == 'periodic':
        # these are the periodic boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1))
        iMod = i*(i>=0)+(i<0)*(numGridCells-1)
    elif boundary == 'extrapolation':
        # these are the 0 derivative boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1))+(i==(numGridCells-1))*i
        iMod = i*(i>=0)+(i<0)*(0)

    gamma     = AppParams['gamma'];

    rhoL     = Q[0,iMod,0];
    rhoL_mod = max([rhoL,1.0e-10]);
    uL       = Q[1,iMod,0]/rhoL_mod;
    pL       = (gamma-1.0)*(Q[2,iMod,0]-0.5*rhoL*uL**2);
    pL_mod   = max([pL,1.0e-10]);

    rhoR     = Q[0,ip1,0];
    rhoR_mod = max([rhoR,1.0e-10]);
    uR       = Q[1,ip1,0]/rhoR_mod;
    pR       = (gamma-1.0)*(Q[2,ip1,0]-0.5*rhoR*uR**2);
    pR_mod   = max([pR,1.0e-10]);

    lambdaMax = max([abs(uL)+sqrt(gamma*pL_mod/rhoL_mod),
                     abs(uR)+sqrt(gamma*pR_mod/rhoR_mod),
                     0.5*(abs(uR)+abs(uL))+sqrt(gamma*(pL_mod+pR_mod)/(rhoL_mod+rhoR_mod))]);

    NumFlux = zeros(numEqns);
    NumFlux[0] = 0.5*(rhoR*uR + rhoL*uL - lambdaMax*(rhoR-rhoL));
    NumFlux[1] = 0.5*(rhoR*uR**2 + pR + rhoL*uL**2 + pL - lambdaMax*(rhoR*uR - rhoL*uL));
    NumFlux[2] = 0.5*(uR*(gamma/(gamma-1.0)*pR + 0.5*rhoR*uR**2) \
          + uL*(gamma/(gamma-1.0)*pL + 0.5*rhoL*uL**2) \
          - lambdaMax*(pR/(gamma-1.0) + 0.5*rhoR*uR**2 \
          - pL/(gamma-1.0) - 0.5*rhoL*uL**2));

    return NumFlux;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def SourceFunction(t_i,
                   x_i,
                   timestep,
                   spacestep,
                   tauIndex,
                   xiIndex,
                   xi,
                   s,
                   AppParams):

    from numpy import array;

    return array([s[0](t_i+0.5*timestep*xi[tauIndex],
                       x_i+0.5*spacestep*xi[xiIndex],
                       AppParams),
                  s[1](t_i+0.5*timestep*xi[tauIndex],
                       x_i+0.5*spacestep*xi[xiIndex],
                       AppParams),
                  s[2](t_i+0.5*timestep*xi[tauIndex],
                       x_i+0.5*spacestep*xi[xiIndex],
                       AppParams)]);
#-------------------------------------------------------------------------------
