#----------------------------------------------------------
def CreateInputFile(mx,morder):

    f = open('parameters.py', 'w');

    f.write('#-------------------------------------------------------------------------------\n');
    f.write('def parameters_init(outputdir):\n');
    f.write('\n');
    f.write('    import param_funcs as pf\n');
    f.write('\n');
    f.write('    #-------------------------------------------------------------\n');
    f.write('    # APPLICATION-SPECIFIC PARAMETERS\n');
    f.write('    gamma = 1.4; # specific heat ratio (aka adiabatic index)\n');
    f.write('    #-------------------------------------------------------------\n');
    f.write('\n');
    f.write('    # ------------------------------------------------------------\n');
    f.write('    # DIMENSION PARAMETERS\n');
    f.write('    numOrder = %1d; # order of accuracy in both space and time\n' % morder);
    f.write('    numEqns  = 3; # number of equations\n');
    f.write('    # ------------------------------------------------------------\n');
    f.write('\n');
    f.write('    # ------------------------------------------------------------\n');
    f.write('    # MESH PARAMETERS\n');
    f.write('    xlow         = -1.0;             # left boundary in domain\n');
    f.write('    xhigh        =  1.0;             # right boundary in domain\n');
    f.write('    numGridCells =  %1d;             # number of grid cell in which the domain is divided\n' % mx);
    f.write('    boundary     =  \'periodic\'; # boundary conditions (\'extrapolation\' or \'periodic\')\n');
    f.write('    # ------------------------------------------------------------\n');
    f.write('\n');
    f.write('    # ------------------------------------------------------------\n');
    f.write('    # TIME STEPPING PARAMETERS\n');
    f.write('    CFL             = pf.set_recommended_cfl(numOrder);             # CFL Number\n');
    f.write('    CFL_max_allowed = pf.set_recommended_cfl_max_allowed(numOrder); # max allowed CFL Number\n');
    f.write('    initialTime     = 0.0;     # initial time\n');
    f.write('    endTime         = 1.0;     # final time\n');
    f.write('    numFrames       = 1;       # number of output frames\n');
    f.write('    timestepInitial = 1.0e-6;  # first time-step\n');
    f.write('    limiters_minmod = False;   # use moment limiters: yes = True, no = False\n');
    f.write('    limiters_positv = False;   # use positivity-preserving limiters: yes = True, no = False\n');
    f.write('    epsilon         = 1.0e-14; # epsilon for positivity limiter\n');
    f.write('    # ------------------------------------------------------------\n');
    f.write('\n');
    f.write('    # ------------------------------------------------------------\n');
    f.write('    # PARAMETERS FOR ITERATIVE SOLVER IN PREDICTION STEP\n');
    f.write('    use_default_iter = True;  # default number of iterations = numOrder\n');
    f.write('    custom_iter      = 50;    # if not using default, how many iterations\n');
    f.write('    # ------------------------------------------------------------\n');
    f.write('\n');
    f.write('    # ------------------------------------------------------------\n');
    f.write('    # DERIVED PARAMETERS\n');
    f.write('    numPredBasis,spacestep = pf.derived_parameters(numOrder,numGridCells,xlow,xhigh);\n');
    f.write('    # ------------------------------------------------------------\n');
    f.write('\n');
    f.write('    # ------------------------------------------------------------\n');
    f.write('    # STORE EVERYTHING IN A LOOKUP TABLE\n');
    f.write('    AppParams  = {\'gamma\'            : gamma};\n');
    f.write('\n');
    f.write('    z   = pf.create_lookup_table(numOrder,\n');
    f.write('                                 numEqns,\n');
    f.write('                                 numPredBasis,\n');
    f.write('                                 xlow,\n');
    f.write('                                 xhigh,\n');
    f.write('                                 numGridCells,\n');
    f.write('                                 spacestep,\n');
    f.write('                                 boundary,\n');
    f.write('                                 CFL,\n');
    f.write('                                 CFL_max_allowed,\n');
    f.write('                                 initialTime,\n');
    f.write('                                 endTime,\n');
    f.write('                                 numFrames,\n');
    f.write('                                 timestepInitial,\n');
    f.write('                                 limiters_minmod,\n');
    f.write('                                 limiters_positv,\n');
    f.write('                                 epsilon,\n');
    f.write('                                 use_default_iter,\n');
    f.write('                                 custom_iter);\n');
    f.write('\n');
    f.write('    DimParams = z[0]; MeshParams = z[1]; TimeParams = z[2]; PredParams = z[3];\n');
    f.write('    # ------------------------------------------------------------\n');
    f.write('\n');
    f.write('    # ------------------------------------------------------------\n');
    f.write('    # WRITE PARAMETERS TO FILE IN OUTPUT DIRECTORY (useful for later reference)\n');
    f.write('    pf.parameters_write_to_file(outputdir,\n');
    f.write('                                write_app_params,\n');
    f.write('                                AppParams,\n');
    f.write('                                DimParams,\n');
    f.write('                                MeshParams,\n');
    f.write('                                TimeParams,\n');
    f.write('                                PredParams);\n');
    f.write('    # ------------------------------------------------------------\n');
    f.write('\n');
    f.write('    # ------------------------------------------------------------\n');
    f.write('    # RETURN EVERYTHING\n');
    f.write('    return AppParams,DimParams,MeshParams,TimeParams,PredParams\n');
    f.write('    # ------------------------------------------------------------\n');
    f.write('\n');
    f.write('#-------------------------------------------------------------------------------\n');
    f.write('\n');
    f.write('#-------------------------------------------------------------------------------\n');
    f.write('def write_app_params(file,AppParams):\n');
    f.write('\n');
    f.write(r'    file.write("AppParams  = {gamma            : %22.15e};\n\n" '+'% AppParams[\'gamma\']);'+'\n');
    f.write('#-------------------------------------------------------------------------------\n');
    f.write('\n');

    f.close();

    return;
#----------------------------------------------------------

#----------------------------------------------------------
def AddToErrorTable(m):

    import string
    f = open('error.dat','r')

    linestring = f.readline()
    linelist = str.split(linestring)

    morder = int(linelist[0])
    mx = int(linelist[1])
    err = float(linelist[2])
    h = float(linelist[3])

    f.close();

    if m==0:
        fnew = open('errortable.dat', 'w');
    else:
        fnew = open('errortable.dat', 'a');

    fnew.write('%3d %3d %16.8e %16.8e\n'%(morder,mx,err,h));
    fnew.close();

    return;
#----------------------------------------------------------

#----------------------------------------------------------
def CreateLaTeXTable(NumMorders,NumMxs,morder_start):

    import string;
    import numpy as np;

    f = open('errortable.dat','r');

    mx    = np.zeros((NumMorders,NumMxs),int);
    err   = np.zeros((NumMorders,NumMxs),float);
    h     = np.zeros((NumMorders,NumMxs),float);
    p     = np.zeros(NumMorders,float);
    log2_err_ratio = np.zeros((NumMorders,NumMxs),float);

    for num in range(0,NumMorders):
        for m in range(0,NumMxs):
            linestring     = f.readline();
            linelist       = str.split(linestring);
            morder         = int(linelist[0]);
            mx[num,m]      = int(linelist[1]);
            err[num,m]     = float(linelist[2]);
            h[num,m]       = float(linelist[3]);

    for num in range(0,NumMorders):
        for m in range(1,NumMxs):
            log2_err_ratio[num,m] = np.log2(err[num,m-1]/err[num,m]);

    #for num in range(0,NumMorders):
    #    linestring = f.readline();
    #    linelist   = string.split(linestring);
    #    tmp        = int(linelist[0]);
    #    tmp        = int(linelist[1]);
    #    p[num]     = float(linelist[2]);
    #    tmp        = float(linelist[3]);

    f.close();

    F = open('error_table.tex','w');

    F.write(r'\documentclass[draft]{siamltex}'); F.write('\n');
    F.write('\n');
    F.write(r'\topmargin 0.0in'); F.write('\n');
    F.write(r'\headsep 0.2in'); F.write('\n');
    F.write(r'%\headheight  -2in%10.6pt'); F.write('\n');
    F.write(r'\oddsidemargin 0.0in '); F.write('\n');
    F.write(r'\textheight 8.75in '); F.write('\n');
    F.write(r'\textwidth 6.5in'); F.write('\n');
    F.write('\n');
    F.write(r'\usepackage{pslatex}'); F.write('\n');
    F.write(r'\usepackage{amsmath}'); F.write('\n');
    F.write(r'\usepackage{amssymb}'); F.write('\n');
    F.write(r'\usepackage{latexsym,pifont,color,comment}'); F.write('\n');
    F.write(r'\usepackage{stmaryrd}'); F.write('\n');
    F.write(r'\usepackage{esint}'); F.write('\n');
    F.write('\n');
    F.write(r'\pagestyle{plain}'); F.write('\n');
    F.write('\n');
    F.write(r'\begin{document}'); F.write('\n');
    F.write('\n');
    F.write(r'\begin{table}'); F.write('\n');
    F.write(r'\begin{center}'); F.write('\n');
    F.write(r'\begin{Large}'); F.write('\n');
    F.write(r'\begin{tabular}{|c|');
    for m in range(0,NumMorders):
        F.write('|c|c|');
    F.write('}\n');
    F.write(r'\hline'); F.write('\n');
    F.write(r'{\normalsize $N$}');
    for m in range(0,NumMorders):
        F.write(r' & {\normalsize $\text{e}_N (p=%i)$} & {\normalsize $\log_2\frac{\text{e}_{N/2}}{\text{e}_{N}}$} ' % (m+morder_start-1) );
    F.write(r'\\');
    F.write('\n');
    F.write(r'\hline\hline'); F.write('\n');

    for m in range(0,NumMxs):

        F.write(r'{\normalsize ');
        F.write('%d'%mx[0,m]);
        F.write(r'} ');

        for k in range(0,NumMorders):
            errtxt = '%.3e'%err[k,m]
            errlead = float(errtxt[0:5]);
            errexp  = int(errtxt[6:9]);

            #F.write(r'& {\normalsize $');
            #F.write('%4.3f'%errlead);
            #F.write(r' \times 10^{');
            #F.write('%i'%errexp);
            #F.write(r'}$} ');

            F.write(r'& {\normalsize ');
            F.write('%4.3f'%errlead);
            F.write(r'e');
            F.write('-%02d'%abs(errexp));
            F.write(r'} ');

            if m==0:
                F.write(r'& -- ');
            else:
                F.write(r'& {\normalsize $');
                F.write('%0.3f'%log2_err_ratio[k,m]);
                F.write(r'$} ');

        F.write(r'\\');
        if k<NumMorders-1:
            F.write(r'\hline\hline');
        else:
            F.write(r'\hline');
        F.write('\n');

    F.write(r'\end{tabular} '); F.write('\n');
    F.write(r'\caption{Relative $L^2$ errors for the 1D compressible Euler equations with constant pressure and fluid velocity and with periodic boundary conditions.');
    F.write('\n');
    F.write(r'\label{table:euler_1d_error}}'); F.write('\n');
    F.write(r'\end{Large}'); F.write('\n');
    F.write(r'\end{center}'); F.write('\n');
    F.write(r'\end{table}'); F.write('\n');
    F.write('\n');
    F.write('\n');
    F.write('\end{document}');
    F.close();

    return;
#----------------------------------------------------------
