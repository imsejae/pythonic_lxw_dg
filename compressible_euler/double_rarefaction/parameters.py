#-------------------------------------------------------------------------------
def parameters_init(outputdir):

    import param_funcs as pf

    #-------------------------------------------------------------
    # APPLICATION-SPECIFIC PARAMETERS
    gamma   =  1.4;    # specific heat ratio (aka adiabatic index)
    rhol    =  7.0;    # left state value of density
    rhor    =  7.0;    # right state value of density
    ul      = -1.0;    # left state value of velocity
    ur      =  1.0;    # right state value of velocity
    pl      =  0.2;    # left state value of pressure
    pr      =  0.2;    # right state value of pressure
    xshock  =  0.0;    # initial shock location
    #-------------------------------------------------------------

    # ------------------------------------------------------------
    # DIMENSION PARAMETERS
    numOrder = 4; # order of accuracy in both space and time
    numEqns  = 3; # number of equations
    # ------------------------------------------------------------

    # ------------------------------------------------------------
    # MESH PARAMETERS
    xlow         = -1.0;             # left boundary in domain
    xhigh        =  1.0;             # right boundary in domain
    numGridCells =  200;             # number of grid cell in which the domain is divided
    boundary     =  'extrapolation'; # boundary conditions ('extrapolation' or 'periodic')
    # ------------------------------------------------------------

    # ------------------------------------------------------------
    # TIME STEPPING PARAMETERS
    CFL             = pf.set_recommended_cfl(numOrder);             # CFL Number
    CFL_max_allowed = pf.set_recommended_cfl_max_allowed(numOrder); # max allowed CFL Number
    initialTime     = 0.0;     # initial time
    endTime         = 0.6;     # final time
    numFrames       = 6;       # number of output frames
    timestepInitial = 1.0e-6;  # first time-step
    limiters_minmod = True;    # use moment limiters: yes = True, no = False
    limiters_positv = True;    # use positivity-preserving limiters: yes = True, no = False
    epsilon         = 1.0e-14; # epsilon for positivity limiter
    # ------------------------------------------------------------

    # ------------------------------------------------------------
    # PARAMETERS FOR ITERATIVE SOLVER IN PREDICTION STEP
    use_default_iter = True;  # default number of iterations = numOrder
    custom_iter      = 50;    # if not using default, how many iterations
    # ------------------------------------------------------------

    # ------------------------------------------------------------
    # DERIVED PARAMETERS
    numPredBasis,spacestep = pf.derived_parameters(numOrder,numGridCells,xlow,xhigh);
    # ------------------------------------------------------------

    # ------------------------------------------------------------
    # STORE EVERYTHING IN A LOOKUP TABLE
    AppParams  = {'gamma'            : gamma,
                  'rhol'             : rhol,
                  'rhor'             : rhor,
                  'ul'               : ul,
                  'ur'               : ur,
                  'pl'               : pl,
                  'pr'               : pr,
                  'xshock'           : xshock};

    z   = pf.create_lookup_table(numOrder,
                                 numEqns,
                                 numPredBasis,
                                 xlow,
                                 xhigh,
                                 numGridCells,
                                 spacestep,
                                 boundary,
                                 CFL,
                                 CFL_max_allowed,
                                 initialTime,
                                 endTime,
                                 numFrames,
                                 timestepInitial,
                                 limiters_minmod,
                                 limiters_positv,
                                 epsilon,
                                 use_default_iter,
                                 custom_iter);

    DimParams = z[0]; MeshParams = z[1]; TimeParams = z[2]; PredParams = z[3];
    # ------------------------------------------------------------

    # ------------------------------------------------------------
    # WRITE PARAMETERS TO FILE IN OUTPUT DIRECTORY (useful for later reference)
    pf.parameters_write_to_file(outputdir,
                                write_app_params,
                                AppParams,
                                DimParams,
                                MeshParams,
                                TimeParams,
                                PredParams);
    # ------------------------------------------------------------

    # ------------------------------------------------------------
    # RETURN EVERYTHING
    return AppParams,DimParams,MeshParams,TimeParams,PredParams
    # ------------------------------------------------------------

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def write_app_params(file,AppParams):

    file.write("AppParams  = {gamma            : %22.15e,\n"    % AppParams['gamma']   );
    file.write("              rhol             : %22.15e,\n"    % AppParams['rhol']    );
    file.write("              rhor             : %22.15e,\n"    % AppParams['rhor']    );
    file.write("              ul               : %22.15e,\n"    % AppParams['ul']      );
    file.write("              ur               : %22.15e,\n"    % AppParams['ur']      );
    file.write("              pl               : %22.15e,\n"    % AppParams['pl']      );
    file.write("              pr               : %22.15e,\n"    % AppParams['pr']      );
    file.write("              xshock           : %22.15e};\n\n" % AppParams['xshock']  );
#-------------------------------------------------------------------------------
