#-------------------------------------------------------------------------------
# DOUBLE RAREFACTION PROBLEM EXAMPLE
#-------------------------------------------------------------------------------

#------------------- Initial Conditions: Primitive variables -------------------
def rho(x,AppParams):
    rhol   = AppParams['rhol'  ];
    rhor   = AppParams['rhor'  ];
    xshock = AppParams['xshock'];
    return (x<xshock)*(rhol) + (x>=xshock)*(rhor);
def u(x,AppParams):
    ul     = AppParams['ul'    ];
    ur     = AppParams['ur'    ];
    xshock = AppParams['xshock'];
    return (x<xshock)*(ul) + (x>=xshock)*(ur);
def p(x,AppParams):
    pl     = AppParams['pl'    ];
    pr     = AppParams['pr'    ];
    xshock = AppParams['xshock'];
    return (x<xshock)*(pl) + (x>=xshock)*(pr);
#-------------------------------------------------------------------------------

#------------------- Initial Conditions: Conserved variables -------------------
def q0(x,AppParams):
    return rho(x,AppParams)
def q1(x,AppParams):
    return rho(x,AppParams)*u(x,AppParams)
def q2(x,AppParams):
    gamma = AppParams['gamma'];
    return 0.5*rho(x,AppParams)*u(x,AppParams)**2 \
           + p(x,AppParams)/(gamma-1.0)
#-------------------------------------------------------------------------------

#---------------------------- Source Term Functions ----------------------------
def s0(t,x,AppParams):
    return 0.0
def s1(t,x,AppParams):
    return 0.0
def s2(t,x,AppParams):
    return 0.0
#-------------------------------------------------------------------------------

#---------------------------- Primitive Equations Source Term Functions --------
def s0_prim(t,x,AppParams):
    return 0.0
def s1_prim(t,x,AppParams):
    return 0.0
def s2_prim(t,x,AppParams):
    return 0.0
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
if __name__ == "__main__":

    # -----------------------------
    # GRAB SOME IMPORTANT FUNCTIONS
    # -----------------------------
    # parameter function (this sits in the current local directory)
    from parameters import parameters_init;
    # set path to main library for DG code
    import sys; sys.path.insert(0,'../../lib');
    # main Lax-Wendroff code (this sits in the main library directory)
    from LaxWendroffDG import LaxWendDG;

    # --------------
    # SET PARAMETERS
    # --------------
    from os import system;
    output_dir = "output";
    system("rm -f -r " + output_dir);
    system("mkdir " + output_dir);
    AppParams,DimParams,MeshParams,TimeParams,PredParams \
        = parameters_init(output_dir);

    # --------------------------------
    # QUICK SANITY CHECK OF PARAMETERS
    # --------------------------------
    assert(DimParams['numEqns']==3);
    assert(AppParams['gamma']>1.0);

    # ------------------------------
    # EXECUTE LAX-WENDROFF DG METHOD
    # ------------------------------
    Q = LaxWendDG([q0,q1,q2],                # initial condition function
                  [s0,s1,s2],                # source term function for conservative vars
                  [s0_prim,s1_prim,s2_prim], # source term function for primitive vars
                  AppParams,                 # application-specific parameters
                  DimParams,                 # parameters determining dimensions of arrays
                  MeshParams,                # mesh-specific parameters
                  TimeParams,                # time-stepping-specific parameters
                  PredParams,                # prediction-step-specific parameters
                  output_dir);               # directory to where output is written

#-------------------------------------------------------------------------------
