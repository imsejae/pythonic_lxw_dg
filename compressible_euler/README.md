# Compressible Euler equations #

### Sub-directories:
* `lib/`                 : library files relevant to all examples
* `double_rarefaction/`  : Riemann problem with two rarefactions and a vacuum state
* `sedov_blast/`         : 1 high-pressure grid cell surrounded by low pressure
* `shocktube/`           : Riemann problem with a 1-rarefaction, 2-contact, and 3-shock
* `smooth_soln/`         : Smooth exact solution to Euler with convergence test

### Variables:
* `t`               : time
* `x`               : spatial coordinate
* `gamma`           : specific heat ratio (aka adiabatic index)
* `rho(t,x)`        : macroscopic fluid density
* `u(t,x)`          : macroscopic fluid velocity
* `p(t,x)`          : thermal pressure
* `m(t,x) = rho*u`  : macroscopic fluid momentum density
* `E(t,x) = 0.5*rho*u^2 + p/(gamma-1)`   : macroscopic fluid energy density
* `s0(t,x), ..., s2(t,x)`         : source term functions

### Equations:
* `  (rho)_{,t} + (rho*u)_{,x}       = s0`      : conservation of mass
* `(rho*u)_{,t} + (rho*u^2 + p)_{,x} = s1`      : conservation of momentum
* `    (E)_{,t} + (u*(E+p))_{,x}     = s2`      : conservation of energy

### Primitive Equation Jacobian:
* `A = array([[u, rho, 0], [0, u, 1/rho], [0, gamma*p, u]]);`

### Eigenvalues of Primitive Equation Jacobian:
* `lambda = u-sqrt(gamma*p/rho), u, u+sqrt(gamma*p/rho)`
