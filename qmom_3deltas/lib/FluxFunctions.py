#-------------------------------------------------------------------------------
def primvars(Q1,Q2,Q3,Q4,Q5):

    # WARNING: this function outputs "rstar" and NOT "r":
    #
    #         rstar = r - p^2/rho - q^2/p
    #

    from numpy import zeros,max;

    prim = zeros(5);

    # density
    prim[0] = Q1;

    # velocity
    prim[1] = Q2/prim[0];

    # pressure
    prim[2] = Q3 - prim[0]*(prim[1]**2);

    # heat flux
    prim[3] = Q4 - prim[0]*(prim[1]**3) - 3.0*prim[2]*prim[1];

    # rstar
    prim[4] = Q5 - prim[0]*(prim[1]**4) - 6.0*prim[2]*(prim[1]**2) \
            - 4.0*prim[3]*prim[1] - (prim[2]**2)/prim[0] - (prim[3]**2)/prim[2];

    return prim;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def consvars(rho,u,p,q,r):

    # WARNING: this function takes as input "r" and NOT "rstar":
    #
    #         r = rstar + p^2/rho + q^2/p
    #

    from numpy import zeros;

    cons = zeros(5);

    cons[0] = rho;
    cons[1] = rho*u;
    cons[2] = rho*u**2 + p;
    cons[3] = rho*u**3 + 3.0*p*u + q;
    cons[4] = rho*u**4 + 6.0*p*u**2 + 4.0*q*u + r;

    return cons;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def flux(rho,u,p,q,r):

    # WARNING: this function takes as input "r" and NOT "rstar":
    #
    #         r = rstar + p^2/rho + q^2/p
    #

    from numpy import zeros;

    f = zeros(5);

    f[0] = rho*u;
    f[1] = rho*u**2 + p;
    f[2] = rho*u**3 + 3.0*p*u + q;
    f[3] = rho*u**4 + 6.0*p*u**2 + 4.0*q*u + r;
    f[4] = rho*u**5 + 10.0*p*u**3 + 10.0*q*u**2 + 5.0*u*r + 2.0*q*r/p - q**3/p**2;

    return f;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def speed_max(rho,u,p,q,rstar):

    # WARNING: this function takes as input "rstar" and NOT "r":
    #
    #         rstar = r - p^2/rho - q^2/p
    #

    from numpy import sqrt,abs;

    tmp1 = (p**2)*(rho**3)*(4.0*(p**3) + (q**2)*rho + 4.0*p*rho*rstar);
    tmp2 = (p**6)*(rho**7)*rstar*(p**2 + rho*rstar);

    s = abs(u) + abs(q)/(2.0*p) + sqrt(tmp1+4.0*sqrt(tmp2))/(2.0*(p**2)*(rho**2));

    return s;
#-------------------------------------------------------------------------------

#------------------- Convert from Conservative to Primitive Variables ----------
def GetPrimFromCons(Q_at_QuadPts,
                    numEqns,
                    numQuadPts,
                    AppParams):

    from numpy import zeros,max;
    Prim_at_QuadPts = zeros([numEqns,
                             numQuadPts]);

    for nq in range(0,numQuadPts):
        # conserved variables at quadrature points
        Q1 = Q_at_QuadPts[0,nq];
        Q2 = Q_at_QuadPts[1,nq];
        Q3 = Q_at_QuadPts[2,nq];
        Q4 = Q_at_QuadPts[3,nq];
        Q5 = Q_at_QuadPts[4,nq];

        # convert to primitive variables
        prim = primvars(Q1,Q2,Q3,Q4,Q5);

        # store in "Prim_at_QuadPts" with de-singularization
        Prim_at_QuadPts[0,nq] = prim[0]; #max([1.0e-10,prim[0]]);
        Prim_at_QuadPts[1,nq] = prim[1];
        Prim_at_QuadPts[2,nq] = prim[2]; #max([1.0e-10,prim[2]]);
        Prim_at_QuadPts[3,nq] = prim[3];
        Prim_at_QuadPts[4,nq] = prim[4]; #max([1.0e-10,prim[4]]);

    return Prim_at_QuadPts;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def FluxFunction(W,
                 i,
                 tauIndex,
                 xiIndex,
                 numPredBasis,
                 psiAtQuadP,
                 AppParams):

    from basis_data import FindPrimitiveVar;
    from numpy import array;

    # get primitive variables from prediction step
    rho = FindPrimitiveVar(W[:numPredBasis,:],
                           tauIndex,
                           xiIndex,
                           i,
                           numPredBasis,
                           psiAtQuadP);
    u   = FindPrimitiveVar(W[numPredBasis:2*numPredBasis,:],
                           tauIndex,
                           xiIndex,
                           i,
                           numPredBasis,
                           psiAtQuadP);
    p   = FindPrimitiveVar(W[2*numPredBasis:3*numPredBasis,:],
                           tauIndex,
                           xiIndex,
                           i,
                           numPredBasis,
                           psiAtQuadP);
    q   = FindPrimitiveVar(W[3*numPredBasis:4*numPredBasis,:],
                            tauIndex,
                            xiIndex,
                            i,
                            numPredBasis,
                            psiAtQuadP);
    rstar = FindPrimitiveVar(W[4*numPredBasis:5*numPredBasis,:],
                            tauIndex,
                            xiIndex,
                            i,
                            numPredBasis,
                            psiAtQuadP);

    # also need "r" (not "rstar" for call to flux)
    r = rstar + p**2/rho + q**2/p;

    return flux(rho,u,p,q,r);

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def EvalPrimJacobian(numEqns,
                     numOrder,
                     AppParams,
                     primvars):

    from numpy import zeros;

    primJac  = zeros([numEqns,numEqns,numOrder,numOrder]);

    for a in range(0,numOrder):
        for b in range(0,numOrder):
            rho   = primvars[0,a,b];
            u     = primvars[1,a,b];
            p     = primvars[2,a,b];
            q     = primvars[3,a,b];
            rstar = primvars[4,a,b];

            primJac[0,0,a,b] = u;
            primJac[0,1,a,b] = rho;
            primJac[0,2,a,b] = 0.0;
            primJac[0,3,a,b] = 0.0;
            primJac[0,4,a,b] = 0.0;

            primJac[1,0,a,b] = 0.0;
            primJac[1,1,a,b] = u;
            primJac[1,2,a,b] = 1.0/rho;
            primJac[1,3,a,b] = 0.0;
            primJac[1,4,a,b] = 0.0;

            primJac[2,0,a,b] = 0.0;
            primJac[2,1,a,b] = 3.0*p;
            primJac[2,2,a,b] = u;
            primJac[2,3,a,b] = 1.0;
            primJac[2,4,a,b] = 0.0;

            primJac[3,0,a,b] = -(p**2)/(rho**2);
            primJac[3,1,a,b] = 4.0*q;
            primJac[3,2,a,b] = -((q**2)/(p**2))-(p/rho);
            primJac[3,3,a,b] = ((2.0*q)/p)+u;
            primJac[3,4,a,b] = 1.0;

            primJac[4,0,a,b] = 0.0;
            primJac[4,1,a,b] = 5.0*rstar;
            primJac[4,2,a,b] = -(2.0*q*rstar)/(p**2);
            primJac[4,3,a,b] = (2.0*rstar)/p;
            primJac[4,4,a,b] = u;

    return primJac;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def NumericalFlux(i,
                  W,
                  tauIndex,
                  numGridCells,
                  numEqns,
                  numPredBasis,
                  boundary,
                  AppParams,
                  QuadData):
    # here W1, W2, and W3 are 2 dimensional because they store over all
    # the grid cells this returns the unitegrated form of flux

    from numpy import sqrt,abs,max,zeros,isnan;

    if boundary == 'periodic':
        # these are the periodic boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1))
        iMod = i*(i>=0)+(i<0)*(numGridCells-1)
    elif boundary == 'extrapolation':
        # these are the 0 derivative boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1))+(i==(numGridCells-1))*i
        iMod = i*(i>=0)+(i<0)*(0)

    W1 = W[0*numPredBasis:1*numPredBasis,:]
    W2 = W[1*numPredBasis:2*numPredBasis,:]
    W3 = W[2*numPredBasis:3*numPredBasis,:]
    W4 = W[3*numPredBasis:4*numPredBasis,:]
    W5 = W[4*numPredBasis:5*numPredBasis,:]

    NumFlux = zeros(numEqns);
    rhoL   = 0.0; # density of left state
    uL     = 0.0; # velocity of left state
    pL     = 0.0; # pressure of left state
    qL     = 0.0; #
    rstarL = 0.0;
    rhoR   = 0.0; # density of right state
    uR     = 0.0; # velocity of right state
    pR     = 0.0; # pressure of right state
    qR     = 0.0;
    rstarR = 0.0;

    for k in range(0,numPredBasis):
        # k is the basis function index
        rhoL   += QuadData['psiAtQuadRight'][k,tauIndex]*W1[k,iMod];
        uL     += QuadData['psiAtQuadRight'][k,tauIndex]*W2[k,iMod];
        pL     += QuadData['psiAtQuadRight'][k,tauIndex]*W3[k,iMod];
        qL     += QuadData['psiAtQuadRight'][k,tauIndex]*W4[k,iMod];
        rstarL += QuadData['psiAtQuadRight'][k,tauIndex]*W5[k,iMod];
        rhoR   += QuadData['psiAtQuadLeft'][k,tauIndex]* W1[k,ip1];
        uR     += QuadData['psiAtQuadLeft'][k,tauIndex]* W2[k,ip1];
        pR     += QuadData['psiAtQuadLeft'][k,tauIndex]* W3[k,ip1];
        qR     += QuadData['psiAtQuadLeft'][k,tauIndex]* W4[k,ip1];
        rstarR += QuadData['psiAtQuadLeft'][k,tauIndex]* W5[k,ip1];

    rL = rstarL + pL**2/rhoL + qL**2/pL;
    rR = rstarR + pR**2/rhoR + qR**2/pR;

    consL = consvars(rhoL,uL,pL,qL,rL);
    consR = consvars(rhoR,uR,pR,qR,rR);

    fL = flux(rhoL,uL,pL,qL,rL);
    fR = flux(rhoR,uR,pR,qR,rR);

    rhoL_mod   = max([rhoL,1.0e-10]);   # desingularize for wave speed only
    rhoR_mod   = max([rhoR,1.0e-10]);   # desingularize for wave speed only
    pL_mod     = max([pL,1.0e-10]);     # desingularize for wave speed only
    pR_mod     = max([pR,1.0e-10]);     # desingularize for wave speed only
    rstarL_mod = max([rstarL,1.0e-10]); # desingularize for wave speed only
    rstarR_mod = max([rstarR,1.0e-10]); # desingularize for wave speed only

    rho_ave   = 0.5*(rhoL_mod+rhoR_mod);
    u_ave     = 0.5*(uL+uR);
    q_ave     = 0.5*(qL+qR);
    p_ave     = 0.5*(pL_mod+pR_mod);
    rstar_ave = 0.5*(rstarL_mod+rstarR_mod);

    sL   = speed_max(rhoL_mod,uL,pL_mod,qL,rstarL_mod);
    sR   = speed_max(rhoR_mod,uR,pR_mod,qR,rstarR_mod);
    sAve = speed_max(rho_ave,u_ave,p_ave,q_ave,rstar_ave);

    if (isnan(sL) or isnan(sR) or isnan(sAve)):
        print(" ")
        print("ERROR: NumericalFlux in FluxFunctions.py")
        print("    iMod = ",iMod,"ip1 = ",ip1)
        print("    (rhoL_mod,uL,pL_mod,qL,rstarL_mod) = ",[rhoL_mod,uL,pL_mod,qL,rstarL_mod])
        print("    (rhoR_mod,uR,pR_mod,qR,rstarR_mod) = ",[rhoR_mod,uR,pR_mod,qR,rstarR_mod])
        print("    (sL,sR,sAve) = ",[sL,sR,sAve])
        print("     Left coeffs: W1[:,iMod] = ",W1[:,iMod])
        print("     Left coeffs: W2[:,iMod] = ",W2[:,iMod])
        print("     Left coeffs: W3[:,iMod] = ",W3[:,iMod])
        print("     Left coeffs: W4[:,iMod] = ",W4[:,iMod])
        print("     Left coeffs: W5[:,iMod] = ",W5[:,iMod])
        print("    Right coeffs: W1[:,ip1]  = ",W1[:,ip1])
        print("    Right coeffs: W2[:,ip1]  = ",W2[:,ip1])
        print("    Right coeffs: W3[:,ip1]  = ",W3[:,ip1])
        print("    Right coeffs: W4[:,ip1]  = ",W4[:,ip1])
        print("    Right coeffs: W5[:,ip1]  = ",W5[:,ip1])
        print(" ")
        raise

    lambdaMax = max([sL,sR,sAve]);

    for m in range(0,numEqns):
        NumFlux[m] = 0.5*(fR[m]+fL[m] - lambdaMax*(consR[m]-consL[m]));

    return NumFlux,lambdaMax
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def NumericalFlux_LxF(i,
                      Q,
                      numGridCells,
                      numEqns,
                      boundary,
                      AppParams):

    from numpy import sqrt,abs,max,zeros,isnan;

    if boundary == 'periodic':
        # these are the periodic boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1))
        iMod = i*(i>=0)+(i<0)*(numGridCells-1)
    elif boundary == 'extrapolation':
        # these are the 0 derivative boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1))+(i==(numGridCells-1))*i
        iMod = i*(i>=0)+(i<0)*(0)

    primL = primvars(Q[0,iMod,0],Q[1,iMod,0],Q[2,iMod,0],\
                     Q[3,iMod,0],Q[4,iMod,0]);
    primR = primvars(Q[0,ip1,0],Q[1,ip1,0],Q[2,ip1,0],\
                     Q[3,ip1,0],Q[4,ip1,0]);

    rhoL   = primL[0];
    uL     = primL[1];
    pL     = primL[2];
    qL     = primL[3];
    rstarL = primL[4];

    rhoR   = primR[0];
    uR     = primR[1];
    pR     = primR[2];
    qR     = primR[3];
    rstarR = primR[4];

    rL = rstarL + pL**2/rhoL + qL**2/pL;
    rR = rstarR + pR**2/rhoR + qR**2/pR;

    fL = flux(rhoL,uL,pL,qL,rL);
    fR = flux(rhoR,uR,pR,qR,rR);

    rhoL_mod   = rhoL #max([rhoL,1.0e-10]);   # desingularize for wave speed only
    rhoR_mod   = rhoR #max([rhoR,1.0e-10]);   # desingularize for wave speed only
    pL_mod     = pL #max([pL,1.0e-10]);     # desingularize for wave speed only
    pR_mod     = pR #max([pR,1.0e-10]);     # desingularize for wave speed only
    rstarL_mod = rstarL #max([rstarL,1.0e-10]); # desingularize for wave speed only
    rstarR_mod = rstarR #max([rstarR,1.0e-10]); # desingularize for wave speed only

    rho_ave   = 0.5*(rhoL_mod+rhoR_mod);
    u_ave     = 0.5*(uL+uR);
    q_ave     = 0.5*(qL+qR);
    p_ave     = 0.5*(pL_mod+pR_mod);
    rstar_ave = 0.5*(rstarL_mod+rstarR_mod);

    sL   = speed_max(rhoL_mod,uL,pL_mod,qL,rstarL_mod);
    sR   = speed_max(rhoR_mod,uR,pR_mod,qR,rstarR_mod);
    sAve = speed_max(rho_ave,u_ave,p_ave,q_ave,rstar_ave);

    if (isnan(sL) or isnan(sR) or isnan(sAve)):
        print(" ")
        print("ERROR: NumericalFlux in FluxFunctions.py")
        print("    iMod = ",iMod,"ip1 = ",ip1)
        print("    (rhoL_mod,uL,pL_mod,qL,rstarL_mod) = ",[rhoL_mod,uL,pL_mod,qL,rstarL_mod])
        print("    (rhoR_mod,uR,pR_mod,qR,rstarR_mod) = ",[rhoR_mod,uR,pR_mod,qR,rstarR_mod])
        print("    (sL,sR,sAve) = ",[sL,sR,sAve])
        print("     Left coeffs: W1[:,iMod] = ",W1[:,iMod])
        print("     Left coeffs: W2[:,iMod] = ",W2[:,iMod])
        print("     Left coeffs: W3[:,iMod] = ",W3[:,iMod])
        print("     Left coeffs: W4[:,iMod] = ",W4[:,iMod])
        print("     Left coeffs: W5[:,iMod] = ",W5[:,iMod])
        print("    Right coeffs: W1[:,ip1]  = ",W1[:,ip1])
        print("    Right coeffs: W2[:,ip1]  = ",W2[:,ip1])
        print("    Right coeffs: W3[:,ip1]  = ",W3[:,ip1])
        print("    Right coeffs: W4[:,ip1]  = ",W4[:,ip1])
        print("    Right coeffs: W5[:,ip1]  = ",W5[:,ip1])
        print(" ")
        raise

    lambdaMax = max([sL,sR,sAve]);

    NumFlux = zeros(numEqns);
    for m in range(0,numEqns):
        NumFlux[m] = 0.5*(fR[m]+fL[m] - lambdaMax*(Q[m,ip1,0]-Q[m,iMod,0]));

    return NumFlux;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
#No source function
def SourceFunction(t_i,
                   x_i,
                   timestep,
                   spacestep,
                   tauIndex,
                   xiIndex,
                   xi,
                   s,
                   AppParams):

    from numpy import array;

    return array([s[0](t_i+0.5*timestep*xi[tauIndex],
                       x_i+0.5*spacestep*xi[xiIndex],
                       AppParams),
                  s[1](t_i+0.5*timestep*xi[tauIndex],
                       x_i+0.5*spacestep*xi[xiIndex],
                       AppParams),
                  s[2](t_i+0.5*timestep*xi[tauIndex],
                       x_i+0.5*spacestep*xi[xiIndex],
                       AppParams),
                  s[3](t_i+0.5*timestep*xi[tauIndex],
                       x_i+0.5*spacestep*xi[xiIndex],
                       AppParams),
                  s[4](t_i+0.5*timestep*xi[tauIndex],
                       x_i+0.5*spacestep*xi[xiIndex],
                       AppParams)]);
#-------------------------------------------------------------------------------
