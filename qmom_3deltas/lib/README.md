# Quadrature-based moment-closure using 3 delta functions: library files #

### Files:
* `FluxFunctions.py`     : routines to evaluate fluxes, primitive variables, and source terms
* `LimiterFunctions.py`  : routines to apply prediction and correction limiters
