#----------------------------------------------------------------------------------
# plot_example.py receive .txt files "output/out*.txt" with the solution
# obtained with the Lax Wendrof DG with limiters method.
#----------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def plot(x,
         Q,
         xlow,
         xhigh,
         variables,
         t,
         AppParams):

    import numpy as np
    import matplotlib
    import matplotlib.pylab as plt
    #import exact_riemann_solution as exact
    matplotlib.rcParams.update({'font.size': 16, 'font.family': 'serif'})

    gamma   = 3.0; #AppParams['gamma' ]
    rhol    = AppParams['rhol'  ];
    rhor    = AppParams['rhor'  ];
    ul      = AppParams['ul'    ];
    ur      = AppParams['ur'    ];
    pl      = AppParams['pl'    ];
    pr      = AppParams['pr'    ];
    ql      = AppParams['ql'    ];
    qr      = AppParams['qr'    ];
    rstarl  = AppParams['rstarl'];
    rstarr  = AppParams['rstarr'];
    xshock  = AppParams['xshock'];

    numVariables = len(variables)
    y = np.zeros([len(x),numVariables])
    for i in range(0, len(x)):
        y[i,0] = Plot.findy(x[i],Q[0,:,:], xlow, xhigh)
        y[i,1] = Plot.findy(x[i],Q[1,:,:], xlow, xhigh)/y[i,0]
        y[i,2] = Plot.findy(x[i],Q[2,:,:], xlow, xhigh)-y[i,0]*y[i,1]**2
        y[i,3] = Plot.findy(x[i],Q[3,:,:], xlow, xhigh) - y[i,0]*y[i,1]**3 - 3*y[i,2]*y[i,1]
        y[i,4] = Plot.findy(x[i],Q[4,:,:], xlow, xhigh) - y[i,0]*y[i,1]**4 - 6*y[i,2]*y[i,1]**2 \
        - 4*y[i,3]*y[i,1] - y[i,2]**2/y[i,0] - y[i,3]**2/y[i,2]

    #rho_exact, u_exact, p_exact = \
    #    exact.exact_riemann_solution(xshock,rhol,ul,pl,rhor,ur,pr,x,t,gamma);

    xticks = np.linspace(xlow,xhigh,9);

    plt.figure(1)
    plt.clf()
    plt.grid()
    ylow  =  0.0;
    yhigh =  1.6;
    yticks = np.linspace(ylow,yhigh,5);
    #plt.gca().set_aspect(((xhigh-xlow)/(yhigh-ylow))*0.5)
    plt.plot(x,y[:,0], '-', label = "numerical", linewidth = 2.0, color='b')
    #plt.plot(x,rho_exact, label = "exact", linewidth =1.5,  color='r')
    plt.title(r'Density: $\rho(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    #plt.ylim(ylow,yhigh)
    plt.xticks(xticks)
    #plt.yticks(yticks)
    #plt.legend(prop={'size':14}, loc =  'lower left')
    plt.savefig("num_euler_shocktube_density.pdf", format='pdf')

    plt.figure(2)
    plt.clf()
    plt.grid()
    ylow  = -0.55;
    yhigh =  -0.25;
    yticks = np.linspace(ylow,yhigh,5);
    #plt.gca().set_aspect(((xhigh-xlow)/(yhigh-ylow))*0.5)
    plt.plot(x,y[:,1], '-', label = "numerical", linewidth = 2.0, color='b')
    #plt.plot(x,u_exact, label = "exact", linewidth =1.5,  color='r')
    plt.title(r'Velocity: $u(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    #plt.ylim(ylow,yhigh)
    plt.xticks(xticks)
    #plt.yticks(yticks)
    #plt.legend(prop={'size':14}, loc =  'upper left')
    plt.savefig("num_euler_shocktube_velocity.pdf", format='pdf')

    plt.figure(3)
    plt.clf()
    plt.grid()
    ylow  =  0.0;
    yhigh =  1.5;
    yticks = np.linspace(ylow,yhigh,5);
    #plt.gca().set_aspect(((xhigh-xlow)/(yhigh-ylow))*0.5)
    plt.plot(x,y[:,2], '-', label = "numerical", linewidth = 2.0, color='b')
    #plt.plot(x,p_exact, label = "exact", linewidth =1.5,  color='r')
    plt.title(r'Pressure: $p(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    #plt.ylim(ylow,yhigh)
    plt.xticks(xticks)
    #plt.yticks(yticks)
    #plt.legend(prop={'size':14}, loc =  'lower left')
    plt.savefig("num_euler_shocktube_pressure.pdf", format='pdf')

    plt.figure(4)
    plt.clf()
    plt.grid()
    ylow  =  0.0;
    yhigh =  1.5;
    yticks = np.linspace(ylow,yhigh,5);
    #plt.gca().set_aspect(((xhigh-xlow)/(yhigh-ylow))*0.5)
    plt.plot(x,y[:,3], '-', label = "numerical", linewidth = 2.0, color='b')
    #plt.plot(x,p_exact, label = "exact", linewidth =1.5,  color='r')
    plt.title(r'Heat Flux: $q(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    #plt.ylim(ylow,yhigh)
    plt.xticks(xticks)
    #plt.yticks(yticks)
    #plt.legend(prop={'size':14}, loc =  'lower left')
    plt.savefig("num_euler_shocktube_heat_flux.pdf", format='pdf')

    plt.figure(5)
    plt.clf()
    plt.grid()
    ylow  =  0.0;
    yhigh =  1.5;
    yticks = np.linspace(ylow,yhigh,5);
    #plt.gca().set_aspect(((xhigh-xlow)/(yhigh-ylow))*0.5)
    plt.plot(x,y[:,4], '-', label = "numerical", linewidth = 2.0, color='b')
    #plt.plot(x,p_exact, label = "exact", linewidth =1.5,  color='r')
    #plt.title(r'rstar: $rstar(t,x)$ at time $t=$' +str(t))
    plt.title(r'k: $k(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    #plt.ylim(ylow,yhigh)
    plt.xticks(xticks)
    #plt.yticks(yticks)
    #plt.legend(prop={'size':14}, loc =  'lower left')
    plt.savefig("num_euler_shocktube_rstar.pdf", format='pdf')

    plt.draw()
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
if __name__== '__main__':

    # -----------------------------
    # GRAB SOME IMPORTANT FUNCTIONS
    # -----------------------------
    # parameter function (this sits in the current local directory)
    from parameters import parameters_init;
    # set path to main library for DG code
    import sys; sys.path.insert(0,'../../lib');

    # --------------
    # SET PARAMETERS
    # --------------
    output_dir = "output";
    AppParams,DimParams,MeshParams,TimeParams,PredParams \
        = parameters_init(output_dir);

    # -------------
    # PLOT SOLUTION
    # -------------
    import Plot
    Plot.plotFrames(output_dir,AppParams,plot);

#-------------------------------------------------------------------------------
