def exact_riemann_solution(shock_location,q_l,q_r,x=None,t=None):
    """
    Return the exact solution to the Riemann problem with initial states ql and qr.
       The solution is computed at time t and points x (where x may be a 1D numpy array).
    """

    import numpy as np;

    # compute the wave speeds
    ws = np.zeros(2)

    # Find shock and rarefaction speeds
    if q_r<=q_l:
        ws[0] = 0.5*(q_l+q_r);
        ws[1] = 0.0+ws[0];
    else:
        ws[0] = q_l;
        ws[1] = q_r;

    # Compute return values

    # Choose a time based on the wave speeds
    if x is None: x = np.linspace(shock_location-1.,shock_loation+1.,1000)
    if t is None: t = 0.8*max(np.abs(x-shock_location))/max(np.abs(ws))

    xs = shock_location + ws*t # Wave locations

    # Find solution inside rarefaction fans
    if (t>0.0):
        xi = (x-shock_location)/t

        q_rarefaction = xi;

        q_out = (x<=xs[0])*q_l + (x>xs[0])*(x<=xs[1])*q_rarefaction + (x>xs[1])*q_r
    else:
        q_out = (x<=shock_location)*q_l + (x>shock_location)*q_r;

    return q_out
