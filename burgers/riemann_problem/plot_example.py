#----------------------------------------------------------------------------------
# plot_example.py receive .txt files "output/out*.txt" with the solution
# obtained with the Lax Wendrof DG with limiters method.
#----------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def plot(x,
         Q,
         xlow,
         xhigh,
         variables,
         t,
         AppParams):

    import numpy as np
    import matplotlib
    import matplotlib.pylab as plt
    from run_example import q
    import exact_riemann_solution as exact
    matplotlib.rcParams.update({'font.size': 14, 'font.family': 'serif'})

    ql      = AppParams['ql'     ];
    qr      = AppParams['qr'     ];
    xshock  = AppParams['xshock' ];

    numVariables = len(variables)
    y = np.zeros([len(x),numVariables])
    for i in range(0, len(x)):
        y[i,0] = Plot.findy(x[i],Q[0,:,:], xlow, xhigh);
    q_exact = exact.exact_riemann_solution(xshock,ql,qr,x,t=t);

    xticks = np.linspace(xlow,xhigh,9);

    plt.figure(1)
    plt.clf()
    plt.grid()
    qmin = np.min([ql,qr]);
    qmax = np.max([ql,qr]);
    qdiff = np.max([0.1*(qmax-qmin),0.01])
    ylow  =  qmin - qdiff;
    yhigh =  qmax + qdiff;
    yticks = np.linspace(ylow,yhigh,5);
    plt.gca().set_aspect(((xhigh-xlow)/(yhigh-ylow))*0.5)
    plt.plot(x,y[:,0], 'o', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x,q_exact, label = "exact", linewidth =1.5,  color='r')
    plt.title(r'Density: $q(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    plt.ylim(ylow,yhigh)
    plt.xticks(xticks)
    plt.yticks(yticks)
    plt.legend(prop={'size':14}, loc =  'upper left')

    plt.draw()
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
if __name__== '__main__':

    # -----------------------------
    # GRAB SOME IMPORTANT FUNCTIONS
    # -----------------------------
    # parameter function (this sits in the current local directory)
    from parameters import parameters_init;
    # set path to main library for DG code
    import sys; sys.path.insert(0,'../../lib');

    # --------------
    # SET PARAMETERS
    # --------------
    output_dir = "output";
    AppParams,DimParams,MeshParams,TimeParams,PredParams \
        = parameters_init(output_dir);

    # -------------
    # PLOT SOLUTION
    # -------------
    import Plot
    Plot.plotFrames(output_dir,AppParams,plot);

#-------------------------------------------------------------------------------
