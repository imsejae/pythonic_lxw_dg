#-------------------------------------------------------------------------------
# DAMBREAK PROBLEM EXAMPLE
#-------------------------------------------------------------------------------

#------------------- Initial Conditions: Primitive variables -------------------
def q(x,AppParams):
    ql     = AppParams['ql'    ];
    qr     = AppParams['qr'    ];
    xshock = AppParams['xshock'];
    return (x<xshock)*(ql) + (x>=xshock)*(qr);
#-------------------------------------------------------------------------------

#------------------- Initial Conditions: Conserved variables -------------------
def q0(x,AppParams):
    return q(x,AppParams);
#-------------------------------------------------------------------------------

#---------------------------- Source Term Functions ----------------------------
def s0(t,x,AppParams):
    return 0.0;
#-------------------------------------------------------------------------------

#---------------------------- Primitive Equations Source Term Functions --------
def s0_prim(t,x,AppParams):
    return 0.0;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
if __name__ == "__main__":

    # -----------------------------
    # GRAB SOME IMPORTANT FUNCTIONS
    # -----------------------------
    # parameter function (this sits in the current local directory)
    from parameters import parameters_init;
    # set path to main library for DG code
    import sys; sys.path.insert(0,'../../lib');
    # main Lax-Wendroff code (this sits in the main library directory)
    from LaxWendroffDG import LaxWendDG;

    # --------------
    # SET PARAMETERS
    # --------------
    from os import system;
    output_dir = "output";
    system("rm -f -r " + output_dir);
    system("mkdir " + output_dir);
    AppParams,DimParams,MeshParams,TimeParams,PredParams \
        = parameters_init(output_dir);

    # --------------------------------
    # QUICK SANITY CHECK OF PARAMETERS
    # --------------------------------
    assert(DimParams['numEqns']==1);

    # ------------------------------
    # EXECUTE LAX-WENDROFF DG METHOD
    # ------------------------------
    Q = LaxWendDG([q0],              # initial condition function
                  [s0],              # source term function for conservative vars
                  [s0_prim],         # source term function for primitive vars
                  AppParams,         # application-specific parameters
                  DimParams,         # parameters determining dimensions of arrays
                  MeshParams,        # mesh-specific parameters
                  TimeParams,        # time-stepping-specific parameters
                  PredParams,        # prediction-step-specific parameters
                  output_dir);       # directory to where output is written

#-------------------------------------------------------------------------------
