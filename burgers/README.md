# Burgers equation #

### Sub-directories:
* `lib/`                      : library files relevant to all examples
* `riemann_problem/`          : Riemann problem
* `sine_to_shock/`            : sine curve evolves into shockwave
* `sine_to_shock_no_limiter/` : sine curve evolves into shockwave (no limiters)

### Variables:
* `t`              : time
* `x`              : spatial coordinate
* `q(t,x)`         : density
* `s0(t,x)`        : source term in conservation equation

### Equations:
* `(q)_{,t} + (0.5*q^2)_{,x} = s0`     : conservation

### Primitive Equation Jacobian:
* `A = q;`

### Eigenvalues of Primitive Equation Jacobian:
* `lambda = q`
