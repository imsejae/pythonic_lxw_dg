#----------------------------------------------------------------------------------
# plot_example.py receive .txt files "output/out*.txt" with the solution
# obtained with the Lax Wendrof DG with limiters method.
#----------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def plot(x,
         Q,
         xlow,
         xhigh,
         variables,
         t,
         AppParams):

    import numpy as np
    import matplotlib
    import matplotlib.pylab as plt
    from run_example import q
    import exact_solution as exact
    matplotlib.rcParams.update({'font.size': 14, 'font.family': 'serif'})

    numVariables = len(variables)
    y = np.zeros([len(x),numVariables])
    for i in range(0, len(x)):
        y[i,0] = Plot.findy(x[i],Q[0,:,:], xlow, xhigh);
    xlong = np.linspace(xlow,xhigh,20001);
    x_exact,q_exact = exact.exact_solution(xlong,t);
    q0 = q(x,AppParams);

    xticks = np.linspace(xlow,xhigh,9);

    plt.figure(1)
    plt.clf()
    plt.grid()
    ylow  =  -1.2;
    yhigh =   1.2;
    yticks = np.linspace(ylow,yhigh,5);
    plt.gca().set_aspect(((xhigh-xlow)/(yhigh-ylow))*0.5)
    plt.plot(x,y[:,0], 'o', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x_exact,q_exact, label = "exact", linewidth =1.5,  color='r')
    plt.plot(x,q0,label = "initial",linewidth=2,color="black",linestyle="dashed");
    plt.title(r'Density: $q(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow,xhigh)
    plt.ylim(ylow,yhigh)
    plt.xticks(xticks)
    plt.yticks(yticks)
    plt.legend(prop={'size':14}, loc =  'lower left')
    plt.savefig("num_burgers_no_limiter.pdf", format='pdf')

    plt.figure(2)
    plt.clf()
    plt.grid()
    xlow_mod  =   0.4;
    xhigh_mod =   0.6;
    xticks_mod = np.linspace(xlow_mod,xhigh_mod,9);
    ylow_mod  =  -1.2;
    yhigh_mod =   1.2;
    yticks = np.linspace(ylow_mod,yhigh_mod,5);
    plt.gca().set_aspect(((xhigh_mod-xlow_mod)/(yhigh_mod-ylow_mod))*0.5)
    plt.plot(x,y[:,0], 'o', label = "numerical", linewidth = 2.0, color='b')
    plt.plot(x_exact,q_exact, label = "exact", linewidth =1.5,  color='r')
    plt.title(r'Density: $q(t,x)$ at time $t=$' +str(t))
    plt.xlabel(r'$x$')
    plt.ylabel('')
    plt.xlim(xlow_mod,xhigh_mod)
    plt.ylim(ylow_mod,yhigh_mod)
    plt.xticks(xticks_mod)
    plt.yticks(yticks)
    plt.xlim(xlow_mod,xhigh_mod)
    plt.ylim(ylow_mod,yhigh_mod)
    plt.legend(prop={'size':14}, loc =  'lower left')
    plt.savefig("num_burgers_zoom_no_limiter.pdf", format='pdf')

    plt.draw()
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
if __name__== '__main__':

    # -----------------------------
    # GRAB SOME IMPORTANT FUNCTIONS
    # -----------------------------
    # parameter function (this sits in the current local directory)
    from parameters import parameters_init;
    # set path to main library for DG code
    import sys; sys.path.insert(0,'../../lib');

    # --------------
    # SET PARAMETERS
    # --------------
    output_dir = "output";
    AppParams,DimParams,MeshParams,TimeParams,PredParams \
        = parameters_init(output_dir);

    # -------------
    # PLOT SOLUTION
    # -------------
    import Plot
    Plot.plotFrames(output_dir,AppParams,plot);

#-------------------------------------------------------------------------------
