#-------------------------------------------------------------------------------
def exact_solution(x,t):
    """
    Exact solution to sine-to-shock Burgers equation problem.
    """
    return qcompute(t,x,qinit(x))
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def qcompute(t,xi_long,qinit_at_xi_long):

    from numpy import zeros,copy,min,max;

    numxi = len(xi_long);
    xout  = zeros(numxi);
    qout  = copy(qinit_at_xi_long);

    for i in range(0,numxi):
        if xi_long[i]<0.5:
            xout[i] = min([0.5,xi_long[i] + t*qinit_at_xi_long[i]]);
            if xout[i]==0.5:
                qout[i] = 0.0;
        else:
            xout[i] = max([0.5,xi_long[i] + t*qinit_at_xi_long[i]]);
            if xout[i]==0.5:
                qout[i] = 0.0;

    return xout,qout
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def qinit(x):
    from numpy import sin,pi;
    return sin(2.0*pi*x);
#-------------------------------------------------------------------------------
