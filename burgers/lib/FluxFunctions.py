#------------------- Convert from Conservative to Primitive Variables ----------
def GetPrimFromCons(Q_at_QuadPts,
                    numEqns,
                    numQuadPts,
                    AppParams):

    from numpy import zeros,max;
    Prim_at_QuadPts = zeros([numEqns,
                             numQuadPts]);

    for nq in range(0,numQuadPts):
        q = Q_at_QuadPts[0,nq];

        Prim_at_QuadPts[0,nq] = q;

    return Prim_at_QuadPts;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def FluxFunction(W,
                 i,
                 tauIndex,
                 xiIndex,
                 numPredBasis,
                 psiAtQuadP,
                 AppParams):

    from basis_data import FindPrimitiveVar;
    from numpy import array;

    q = FindPrimitiveVar(W[:numPredBasis,:], tauIndex, xiIndex, i,
                         numPredBasis, psiAtQuadP);

    return 0.5*q**2;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def EvalPrimJacobian(numEqns,
                     numOrder,
                     AppParams,
                     primvars):

    from numpy import zeros;

    primJac  = zeros([numEqns,numEqns,numOrder,numOrder]);

    for a in range(0,numOrder):
        for b in range(0,numOrder):
            q = primvars[0,a,b];

            primJac[0,0,a,b] = q;

    return primJac;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def NumericalFlux(i,
                  W,
                  tauIndex,
                  numGridCells,
                  numEqns,
                  numPredBasis,
                  boundary,
                  AppParams,
                  QuadData):

    # here W1 and W2 are 2 dimensional because they store over all
    # the grid cells this returns the unitegrated form of flux

    from numpy import abs,zeros,max;

    if boundary == 'periodic':
        # these are the periodic boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1));
        iMod = i*(i>=0)+(i<0)*(numGridCells-1);
    elif boundary == 'extrapolation':
        # these are the 0 derivative boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1))+(i==(numGridCells-1))*i;
        iMod = i*(i>=0)+(i<0)*(0);

    W1 = W[:numPredBasis,:];

    NumFlux = zeros(numEqns);
    ql = 0.0; # density of left state
    qr = 0.0; # density of right state

    for k in range(0,numPredBasis):
        # k is the basis function index
        ql += QuadData['psiAtQuadRight'][k,tauIndex]*W1[k,iMod]; # concentration of left state
        qr += QuadData['psiAtQuadLeft' ][k,tauIndex]*W1[k,ip1];  # concentration of right state

    lambdaMax = max([abs(ql),\
                     abs(qr),\
                     0.5*abs(ql+qr)]);

    # quadLeft refers to the left most point of the i+1 cell
    # and quad right refers to the right point of the i cell

    f_r = 0.5*qr**2 - lambdaMax*qr;
    f_l = 0.5*ql**2 + lambdaMax*ql;
    NumFlux[0] = 0.5 * ( f_r + f_l );

    return NumFlux,lambdaMax;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def NumericalFlux_LxF(i,
                      Q,
                      numGridCells,
                      numEqns,
                      boundary,
                      AppParams):

    from numpy import sqrt,zeros,max;

    if boundary == 'periodic':
        # these are the periodic boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1));
        iMod = i*(i>=0)+(i<0)*(numGridCells-1);
    elif boundary == 'extrapolation':
        # these are the 0 derivative boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1))+(i==(numGridCells-1))*i;
        iMod = i*(i>=0)+(i<0)*(0);

    ql = Q[0,iMod,0];
    qr = Q[0,ip1,0];

    NumFlux = zeros(numEqns);

    lambdaMax = max([abs(ql),\
                     abs(qr),\
                     0.5*abs(ql+qr)]);

    f_r = 0.5*qr**2 - lambdaMax*qr;
    f_l = 0.5*ql**2 + lambdaMax*ql;
    NumFlux[0] = 0.5 * ( f_r + f_l );

    return NumFlux;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def SourceFunction(t_i,
                   x_i,
                   timestep,
                   spacestep,
                   tauIndex,
                   xiIndex,
                   xi,
                   s,
                   AppParams):

    from numpy import array;

    return array([s[0](t_i+0.5*timestep*xi[tauIndex],
                       x_i+0.5*spacestep*xi[xiIndex],
                       AppParams)]);
#-------------------------------------------------------------------------------
