#-------------------------------------------------------------------------------
def LimitAverageFlux(nu,
                     Q_LxF,
                     dFlux_lft,
                     dFlux_rgt,
                     NumFlux_lft,
                     NumFlux_rgt,
                     epsilon):

    # NOT IMPLEMENTED

    Lambda_lft = 1.0;
    Lambda_rgt = 1.0;

    return Lambda_lft,Lambda_rgt;
#-------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def PredictionStep_Positivity_Limiter(Win,
                                      numGridCells,
                                      numOrder,
                                      numPredBasis,
                                      numEqns,
                                      epsilon,
                                      AppParams,
                                      QuadData):

    num_psiAtPositivityPts = QuadData['num_psiAtPositivityPts'];
    psiAtPositivityPts     = QuadData['psiAtPositivityPts'    ];
    varepsilon = 1.1*epsilon;

    from numpy import min,copy;

    Wout = copy(Win);

    return Wout;
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def CorrectionStep_Positivity_Limiter(Qin,
                                      numGridCells,
                                      numOrder,
                                      numEqns,
                                      epsilon,
                                      AppParams,
                                      num_phiAtPosPts,
                                      phiAtPosPts):

    from numpy import isnan,min,copy;
    varepsilon = 2.0*epsilon;

    Qout = copy(Qin);

    return Qout;
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def CorrectionStep_Moment_Limiter(Q,
                                  numOrder,
                                  numEqns,
                                  MeshParams,
                                  AppParams,
                                  epsilon,
                                  num_phiAtLimPts,
                                  phiAtLimPts):

    from numpy import array,sqrt,dot,zeros,copy;
    from numpy.linalg import norm;

    # convert to characteristic variables
    dC_right,dC_left,C_cent = convert_to_char_vars(Q,
                                                   MeshParams['numGridCells'],
                                                   numOrder,
                                                   numEqns,
                                                   MeshParams['boundary'],
                                                   AppParams);

    # hierarchical minmod limiter on each characteristic variable
    for i in range(0,MeshParams['numGridCells']):

        for m in range(0,numEqns):
            mstop = 0;
            ell = numOrder-2;

            while (mstop==0):

                C_unlimited = C_cent[m,i,ell];
                C_limited = minmod(  C_unlimited,
                                     dC_left[m,i,ell],
                                    dC_right[m,i,ell] );
                C_cent[m,i,ell] = C_limited;

                if abs(C_limited-C_unlimited)>epsilon or \
                   abs(C_limited)<=epsilon:
                    ell -= 1;
                    if ell == -1:
                        mstop = 1;
                else:
                    mstop = 1;

    # convert back to conservative variables
    Qout = convert_to_cons_vars(C_cent,
                                Q,
                                MeshParams['numGridCells'],
                                numOrder,
                                numEqns,
                                AppParams);

    return Qout;
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def convert_to_char_vars(Q,
                         numGridCells,
                         numOrder,
                         numEqns,
                         boundary,
                         AppParams):

    from numpy import zeros,arange,sqrt,array,dot,abs,max;

    # Characteristic variable storage
    C_cent   = zeros([numEqns,numGridCells,numOrder-1]);
    dC_right = zeros([numEqns,numGridCells,numOrder-1]);
    dC_left  = zeros([numEqns,numGridCells,numOrder-1]);

    ip1_vec = arange(numGridCells)+1;
    im1_vec = arange(numGridCells)-1;
    if boundary=='periodic':
        ip1_vec[numGridCells-1] = 0;
        im1_vec[0] = numGridCells-1;
    elif boundary=='extrapolation':
        ip1_vec[numGridCells-1] = numGridCells-1;
        im1_vec[0] = 0;

    sc = zeros(numOrder-1);
    for ell in range(0,numOrder-1):
        sc[ell] = sqrt((2.0*(ell+2.0)-3.0)/(2.0*(ell+2.0)-1.0));

    #W1 = W[:numPredBasis,:];
    #W2 = W[numPredBasis:,:];

    for i in range(0,numGridCells):

        ip1 = ip1_vec[i];
        im1 = im1_vec[i];

        q_ave = Q[0,i,0];
        Rinv  = 1.0;

        for ell in range(0,numOrder-1):
            C_cent[:,i,ell]   =         dot(Rinv, Q[:,i,ell+1]                );
            dC_left[:,i,ell]  = sc[ell]*dot(Rinv, Q[:,i,  ell] - Q[:,im1,ell] );
            dC_right[:,i,ell] = sc[ell]*dot(Rinv, Q[:,ip1,ell] - Q[:,i,  ell] );

    return dC_right,dC_left,C_cent
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def convert_to_cons_vars(Cin,
                         Qin,
                         numGridCells,
                         numOrder,
                         numEqns,
                         AppParams):

    from numpy import copy,array,dot,sqrt,max;

    # Conservative variable storage
    Qout = copy(Qin);

    #W1 = Win[:numPredBasis,:];
    #W2 = Win[numPredBasis:,:];

    for i in range(0,numGridCells):

        q_ave = Qin[0,i,0];
        R     = 1.0;

        for ell in range(1,numOrder):
            Qout[:,i,ell] = dot(R, Cin[:,i,ell-1]);

    return Qout;
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
def minmod(a,b,c):
    from numpy import sign,min;

    if a*b<0.0 or b*c<0.0 or a*c<0.0:
        return 0.0

    return sign(a)*min([abs(a),abs(b),abs(c)]);
#------------------------------------------------------------------------------
