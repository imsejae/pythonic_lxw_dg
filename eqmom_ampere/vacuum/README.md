# Quadrature-based moment-closure using 3 delta functions: shock tube with vacuum #

### Files:
* `run_example.py`   - (main) runs Lax-Wendroff DG method
* `plot_example.py`  - (main) plots solution from Lax-Wendroff DG method
* `parameters.py`    - (main) contains parameter values for simulation

### Use the Makefile to run code:
* `make help`     : this will list all possible targets
* `make run`      : this will execute the DG code
* `make plot`     : this will plot the DG solution
* `make clean`    : this will remove all `*.pyc` and `output/*` files
* `make cleanrun` : this will clean and run
* `make all`      : this will clean, run, and plot

### Initial conditions:
* `rho(t=0,x)   = (x<xshock)*rhol   + (x>=xshock)*rhor`
* `u(t=0,x)     = (x<xshock)*ul     + (x>=xshock)*ur`
* `p(t=0,x)     = (x<xshock)*pl     + (x>=xshock)*pr`
* `q(t=0,x)     = (x<xshock)*ql     + (x>=xshock)*qr`
* `rstar(t=0,x) = (x<xshock)*rstarl + (x>=xshock)*rstarr`
