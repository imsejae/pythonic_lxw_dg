
#-------------------------------------------------------------------------------
def speed_max(u,T,dels): #dels

    # WARNING: this function takes as input "rstar" and NOT "r":
    #
    #         rstar = r - p^2/rho - q^2/p
    #

    from numpy import sqrt,abs,max,array,size;
    from eqmom_closure import m_star

    roots = array([[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [1.0,-1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 1.73205080756887729, -1.73205080756887729, 0.0, 0.0, \
                      0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.741963784302725858, -0.741963784302725858, 2.33441421833897724, \
                      -2.33441421833897724, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.0, 1.35562617997426587, -1.35562617997426587, 2.85697001387280565, \
                      -2.85697001387280565, 0.0, 0.0, 0.0, 0.0, 0.0],
                    [0.616706590192594152, -0.616706590192594152, 1.88917587775371068, \
                      -1.88917587775371068, 3.32425743355211895, -3.32425743355211895, 0.0, \
                      0.0, 0.0, 0.0],
                    [0.0, 1.154405394739968127, -1.154405394739968127, 2.36675941073454129, \
                      -2.36675941073454129, 3.75043971772574226, -3.75043971772574226, 0.0, 0.0, 0.0],
                    [0.539079811351375108, -0.539079811351375108, 1.63651904243510800, \
                      -1.63651904243510800, 2.80248586128754170, -2.80248586128754170, \
                      4.14454718612589433, -4.14454718612589433, 0.0, 0.0],
                    [0.0, 1.023255663789132525, -1.023255663789132525, 2.07684797867783011, \
                      -2.07684797867783011, 3.20542900285646994, -3.20542900285646994, \
                      4.51274586339978267, -4.51274586339978267, 0.0],
                    [0.484935707515497653, -0.484935707515497653, 1.46598909439115818, \
                      -1.46598909439115818, 2.48432584163895458, -2.48432584163895458, \
                      3.58182348355192692, -3.58182348355192692, 4.85946282833231215, \
                      -4.85946282833231215]]);

    #print("roots is", roots[9,:])

    mu_max = max(roots[dels,:]); #dels
    #print("T is", T);
    s = abs(u)+sqrt(T)*mu_max;
    #print("s is", s);

    #tmp1 = (p**2)*(rho**3)*(4.0*(p**3) + (q**2)*rho + 4.0*p*rho*rstar);
    #tmp2 = (p**6)*(rho**7)*rstar*(p**2 + rho*rstar);

    #s = abs(u) + abs(q)/(2.0*p) + sqrt(tmp1+4.0*sqrt(tmp2))/(2.0*(p**2)*(rho**2));




    return s;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------

#------------------- Convert from Conservative to Primitive Variables ----------
def GetPrimFromCons(Q_at_QuadPts,
                    numEqns,
                    numQuadPts,
                    AppParams):

    return Q_at_QuadPts


#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def FluxFunction(W,
                 i,
                 tauIndex,
                 xiIndex,
                 numPredBasis,
                 psiAtQuadP,
                 AppParams,
                 numEqns,
                 dels,
                 t,
                 numGridCells,
                 xloc,
                 tloc): #take out t and numGridCells!

    from basis_data import FindPrimitiveVar;
    from numpy import array,zeros,pi;
    from eqmom_closure import m_star,m_starExact;

    Q1 = zeros(numEqns);
    # get primitive variables from prediction step
    for j in range(0,numEqns):
        Q1[j] =FindPrimitiveVar(W[j*numPredBasis:(j+1)*numPredBasis,:],
                               tauIndex,
                               xiIndex,
                               i,
                               numPredBasis,
                               psiAtQuadP);

    #Q1 = FindPrimitiveVar(W[:numPredBasis,:],
    #                       tauIndex,
    #                       xiIndex,
    #                       i,
    #                       numPredBasis,
    #                       psiAtQuadP);
    #Q2   = FindPrimitiveVar(W[numPredBasis:2*numPredBasis,:],
    #                       tauIndex,
    #                       xiIndex,
    #                       i,
    #                       numPredBasis,
    #                       psiAtQuadP);
    #Q3   = FindPrimitiveVar(W[2*numPredBasis:3*numPredBasis,:],
    #                       tauIndex,
    #                       xiIndex,
    #                       i,
    #                       numPredBasis,
    #                       psiAtQuadP);
    #Q4   = FindPrimitiveVar(W[3*numPredBasis:4*numPredBasis,:],
    #                        tauIndex,
    #                        xiIndex,
    #                        i,
    #                        numPredBasis,
    #                        psiAtQuadP);
    #Q5 = FindPrimitiveVar(W[4*numPredBasis:5*numPredBasis,:],
    #                        tauIndex,
    #                        xiIndex,
    #                        i,
    #                        numPredBasis,
    #                        psiAtQuadP);

    # also need "r" (not "rstar" for call to flux)
    #r = rstar + p**2/rho + q**2/p;

    #cons = consvars(rho,u,p,q,r);
    #cons = array([Q1,Q2,Q3,Q4,Q5]);
    #return flux(rho,u,p,q,r);


    #print("x is", -pi+i*spacestep);
    #print("t is", t);

    return m_star(Q1,xloc,tloc);#dels); #dels

#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def EvalPrimJacobian(numEqns,
                     numOrder,
                     AppParams,
                     primvars):

    from numpy import zeros;

    primJac  = zeros([numEqns,numEqns,numOrder,numOrder]);

    for a in range(0,numOrder):
        for b in range(0,numOrder):
            rho   = primvars[0,a,b];
            u     = primvars[1,a,b];
            p     = primvars[2,a,b];
            q     = primvars[3,a,b];
            rstar = primvars[4,a,b];

            primJac[0,0,a,b] = u;
            primJac[0,1,a,b] = rho;
            primJac[0,2,a,b] = 0.0;
            primJac[0,3,a,b] = 0.0;
            primJac[0,4,a,b] = 0.0;

            primJac[1,0,a,b] = 0.0;
            primJac[1,1,a,b] = u;
            primJac[1,2,a,b] = 1.0/rho;
            primJac[1,3,a,b] = 0.0;
            primJac[1,4,a,b] = 0.0;

            primJac[2,0,a,b] = 0.0;
            primJac[2,1,a,b] = 3.0*p;
            primJac[2,2,a,b] = u;
            primJac[2,3,a,b] = 1.0;
            primJac[2,4,a,b] = 0.0;

            primJac[3,0,a,b] = -(p**2)/(rho**2);
            primJac[3,1,a,b] = 4.0*q;
            primJac[3,2,a,b] = -((q**2)/(p**2))-(p/rho);
            primJac[3,3,a,b] = ((2.0*q)/p)+u;
            primJac[3,4,a,b] = 1.0;

            primJac[4,0,a,b] = 0.0;
            primJac[4,1,a,b] = 5.0*rstar;
            primJac[4,2,a,b] = -(2.0*q*rstar)/(p**2);
            primJac[4,3,a,b] = (2.0*rstar)/p;
            primJac[4,4,a,b] = u;

    return primJac;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def NumericalFlux(i,
                  W,
                  tauIndex,
                  numGridCells,
                  numEqns,
                  numPredBasis,
                  boundary,
                  AppParams,
                  QuadData,
                  dels,
                  t,
                  xloc,
                  tloc): #dels
    # here W1, W2, and W3 are 2 dimensional because they store over all
    # the grid cells this returns the unitegrated form of flux

    from numpy import sqrt,abs,max,zeros,isnan,array,pi,shape;
    from eqmom_closure import m_star,m_starExact;

    if boundary == 'periodic':
        # these are the periodic boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1))
        iMod = i*(i>=0)+(i<0)*(numGridCells-1)
    elif boundary == 'extrapolation':
        # these are the 0 derivative boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1))+(i==(numGridCells-1))*i
        iMod = i*(i>=0)+(i<0)*(0)

    #W1 = W[0*numPredBasis:1*numPredBasis,:]
    #W2 = W[1*numPredBasis:2*numPredBasis,:]
    #W3 = W[2*numPredBasis:3*numPredBasis,:]
    #W4 = W[3*numPredBasis:4*numPredBasis,:]
    #W5 = W[4*numPredBasis:5*numPredBasis,:]

    #print("i is", i);
    W1 = zeros([numEqns,numPredBasis,numGridCells])

    for j in range(0,numEqns):
        W1[j,:,:] = W[j*numPredBasis:(j+1)*numPredBasis,:];

    consL = zeros(numEqns);
    consR = zeros(numEqns);


    NumFlux = zeros(numEqns);
    #rhoL   = 0.0; # density of left state
    #uL     = 0.0; # velocity of left state
    #pL     = 0.0; # pressure of left state
    #qL     = 0.0; #
    #rstarL = 0.0;
    #rhoR   = 0.0; # density of right state
    #uR     = 0.0; # velocity of right state
    #pR     = 0.0; # pressure of right state
    #qR     = 0.0;
    #rstarR = 0.0;

    for j in range(0,numEqns):
        # k is the basis function index
        for k in range(0,numPredBasis):
            consL[j] += QuadData['psiAtQuadRight'][k,tauIndex]*W1[j,k,iMod];
            consR[j] += QuadData['psiAtQuadLeft'][k,tauIndex]* W1[j,k,ip1];

        #consL[0]   += QuadData['psiAtQuadRight'][k,tauIndex]*W1[k,iMod];
        #consL[1]     += QuadData['psiAtQuadRight'][k,tauIndex]*W2[k,iMod];
        #consL[2]     += QuadData['psiAtQuadRight'][k,tauIndex]*W3[k,iMod];
        #consL[3]     += QuadData['psiAtQuadRight'][k,tauIndex]*W4[k,iMod];
        #consL[4] += QuadData['psiAtQuadRight'][k,tauIndex]*W5[k,iMod];
        #consR[0]   += QuadData['psiAtQuadLeft'][k,tauIndex]* W1[k,ip1];
        #consR[1]     += QuadData['psiAtQuadLeft'][k,tauIndex]* W2[k,ip1];
        #consR[2]     += QuadData['psiAtQuadLeft'][k,tauIndex]* W3[k,ip1];
        #consR[3]     += QuadData['psiAtQuadLeft'][k,tauIndex]* W4[k,ip1];
        #consR[4] += QuadData['psiAtQuadLeft'][k,tauIndex]* W5[k,ip1];

    #rL = rstarL + pL**2/rhoL + qL**2/pL;
    #rR = rstarR + pR**2/rhoR + qR**2/pR;

    #consL = consvars(rhoL,uL,pL,qL,rL);
    #consR = consvars(rhoR,uR,pR,qR,rR);

#Flux for QMOM
    #fL = flux(rhoL,uL,pL,qL,rL);
    #fR = flux(rhoR,uR,pR,qR,rR);

    #print("x is", -pi+i*spacestep);
    #print("t is", t);
    #Flux for eqmom_closure
    fL = m_star(consL,xloc,tloc);#dels); #dels
    fR = m_star(consR,xloc,tloc);#dels); #dels

    #rhoL_mod   = max([rhoL,1.0e-10]);   # desingularize for wave speed only
    #rhoR_mod   = max([rhoR,1.0e-10]);   # desingularize for wave speed only
    #pL_mod     = max([pL,1.0e-10]);     # desingularize for wave speed only
    #pR_mod     = max([pR,1.0e-10]);     # desingularize for wave speed only
    #rstarL_mod = max([rstarL,1.0e-10]); # desingularize for wave speed only
    #rstarR_mod = max([rstarR,1.0e-10]); # desingularize for wave speed only

    #rho_ave   = 0.5*(rhoL_mod+rhoR_mod);
    #u_ave     = 0.5*(uL+uR);
    #q_ave     = 0.5*(qL+qR);
    #p_ave     = 0.5*(pL_mod+pR_mod);
    #rstar_ave = 0.5*(rstarL_mod+rstarR_mod);
    uL = consL[1]/consL[0];
    uR = consR[1]/consR[0];
    u_ave = 0.5*(uL+uR);
    TL = consL[2]/consL[0]-(consL[1]/consL[0])**2;
    TR = consR[2]/consR[0]-(consR[1]/consR[0])**2;
    T_ave = 0.5*(TL+TR);

    #print("p is", consR[2]-consR[0]*(consR[1]/consR[0])**2)

    sL   = speed_max(uL,TL,dels); #dels
    sR   = speed_max(uR,TR,dels); #dels
    #sAve = speed_max(rho_ave,u_ave,p_ave,q_ave,rstar_ave);
    sAve = speed_max(u_ave,T_ave,dels); #dels

    #if (isnan(sL) or isnan(sR) or isnan(sAve)):
    #    print(" ")
    #    print("ERROR: NumericalFlux in FluxFunctions.py")
    #    print("    iMod = ",iMod,"ip1 = ",ip1)
    #    print("    (rhoL_mod,uL,pL_mod,qL,rstarL_mod) = ",[rhoL_mod,uL,pL_mod,qL,rstarL_mod])
    #    print("    (rhoR_mod,uR,pR_mod,qR,rstarR_mod) = ",[rhoR_mod,uR,pR_mod,qR,rstarR_mod])
    #    print("    (sL,sR,sAve) = ",[sL,sR,sAve])
    #    print("     Left coeffs: W1[:,iMod] = ",W1[:,iMod])
    #    print("     Left coeffs: W2[:,iMod] = ",W2[:,iMod])
    #    print("     Left coeffs: W3[:,iMod] = ",W3[:,iMod])
    #    print("     Left coeffs: W4[:,iMod] = ",W4[:,iMod])
    #    print("     Left coeffs: W5[:,iMod] = ",W5[:,iMod])
    #    print("    Right coeffs: W1[:,ip1]  = ",W1[:,ip1])
    #    print("    Right coeffs: W2[:,ip1]  = ",W2[:,ip1])
    #    print("    Right coeffs: W3[:,ip1]  = ",W3[:,ip1])
    #    print("    Right coeffs: W4[:,ip1]  = ",W4[:,ip1])
    #    print("    Right coeffs: W5[:,ip1]  = ",W5[:,ip1])
    #    print(" ")
    #    raise

    lambdaMax = max([sL,sR,sAve]);

    for m in range(0,numEqns):
        NumFlux[m] = 0.5*(fR[m]+fL[m] - lambdaMax*(consR[m]-consL[m]));


    return NumFlux,lambdaMax
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
def NumericalFlux_LxF(i,
                      Q,
                      numGridCells,
                      numEqns,
                      boundary,
                      AppParams,
                      dels): #dels

    from numpy import sqrt,abs,max,zeros,isnan,array;
    from eqmom_closure import m_star;

    if boundary == 'periodic':
        # these are the periodic boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1))
        iMod = i*(i>=0)+(i<0)*(numGridCells-1)
    elif boundary == 'extrapolation':
        # these are the 0 derivative boundary conditions
        ip1 = (i+1)*(i<(numGridCells-1))+(i==(numGridCells-1))*i
        iMod = i*(i>=0)+(i<0)*(0)

    primL = primvars(Q[0,iMod,0],Q[1,iMod,0],Q[2,iMod,0],\
                     Q[3,iMod,0],Q[4,iMod,0]);
    primR = primvars(Q[0,ip1,0],Q[1,ip1,0],Q[2,ip1,0],\
                     Q[3,ip1,0],Q[4,ip1,0]);

    rhoL   = primL[0];
    uL     = primL[1];
    pL     = primL[2];
    qL     = primL[3];
    rstarL = primL[4];

    rhoR   = primR[0];
    uR     = primR[1];
    pR     = primR[2];
    qR     = primR[3];
    rstarR = primR[4];

    rL = rstarL + pL**2/rhoL + qL**2/pL;
    rR = rstarR + pR**2/rhoR + qR**2/pR;

    #fL = flux(rhoL,uL,pL,qL,rL);
    #fR = flux(rhoR,uR,pR,qR,rR);

    #Flux for eqmom_closure
    fL = m_star(primL,dels); #dels
    fR = m_star(primR,dels); #dels

    rhoL_mod   = rhoL #max([rhoL,1.0e-10]);   # desingularize for wave speed only
    rhoR_mod   = rhoR #max([rhoR,1.0e-10]);   # desingularize for wave speed only
    pL_mod     = pL #max([pL,1.0e-10]);     # desingularize for wave speed only
    pR_mod     = pR #max([pR,1.0e-10]);     # desingularize for wave speed only
    rstarL_mod = rstarL #max([rstarL,1.0e-10]); # desingularize for wave speed only
    rstarR_mod = rstarR #max([rstarR,1.0e-10]); # desingularize for wave speed only

    rho_ave   = 0.5*(rhoL_mod+rhoR_mod);
    u_ave     = 0.5*(uL+uR);
    q_ave     = 0.5*(qL+qR);
    p_ave     = 0.5*(pL_mod+pR_mod);
    rstar_ave = 0.5*(rstarL_mod+rstarR_mod);

    sL   = speed_max(rhoL_mod,uL,pL_mod,qL,rstarL_mod); #dels
    sR   = speed_max(rhoR_mod,uR,pR_mod,qR,rstarR_mod); #dels
    sAve = speed_max(rho_ave,u_ave,p_ave,q_ave,rstar_ave); #dels

    if (isnan(sL) or isnan(sR) or isnan(sAve)):
        print(" ")
        print("ERROR: NumericalFlux in FluxFunctions.py")
        print("    iMod = ",iMod,"ip1 = ",ip1)
        print("    (rhoL_mod,uL,pL_mod,qL,rstarL_mod) = ",[rhoL_mod,uL,pL_mod,qL,rstarL_mod])
        print("    (rhoR_mod,uR,pR_mod,qR,rstarR_mod) = ",[rhoR_mod,uR,pR_mod,qR,rstarR_mod])
        print("    (sL,sR,sAve) = ",[sL,sR,sAve])
        print("     Left coeffs: W1[:,iMod] = ",W1[:,iMod])
        print("     Left coeffs: W2[:,iMod] = ",W2[:,iMod])
        print("     Left coeffs: W3[:,iMod] = ",W3[:,iMod])
        print("     Left coeffs: W4[:,iMod] = ",W4[:,iMod])
        print("     Left coeffs: W5[:,iMod] = ",W5[:,iMod])
        print("    Right coeffs: W1[:,ip1]  = ",W1[:,ip1])
        print("    Right coeffs: W2[:,ip1]  = ",W2[:,ip1])
        print("    Right coeffs: W3[:,ip1]  = ",W3[:,ip1])
        print("    Right coeffs: W4[:,ip1]  = ",W4[:,ip1])
        print("    Right coeffs: W5[:,ip1]  = ",W5[:,ip1])
        print(" ")
        raise

    lambdaMax = max([sL,sR,sAve]);

    NumFlux = zeros(numEqns);
    for m in range(0,numEqns):
        NumFlux[m] = 0.5*(fR[m]+fL[m] - lambdaMax*(Q[m,ip1,0]-Q[m,iMod,0]));

    return NumFlux;
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
#No source function
def SourceFunction(t_i,
                   x_i,
                   i,
                   timestep,
                   spacestep,
                   tauIndex,
                   xiIndex,
                   xi,
                   s,
                   W,
                   psiAtQuadP,
                   E_field,
                   AppParams,
                   numEqns,
                   numPredBasis,
                   numOrder):



    from numpy import array,zeros,shape,dot;

    tmp = zeros(numEqns);
    vars = zeros(numEqns);

    for m in range(0,numEqns):
        vars[m] = dot(psiAtQuadP[:,xiIndex,tauIndex],W[m*numPredBasis:(m+1)*numPredBasis,i]);

    #E_vars = dot(psiAtQuadP[:,xiIndex,tauIndex],E_field[:,i]);

    for j in range(0,numEqns):
        #print("t_i", t_i)
        #print("x_i", x_i)
        #print("xi[0]", xi[0])
        #print("tauIndex", tauIndex)
        #print("xiIndex", xiIndex)
        tmp[j] = s[j](t_i+0.5*timestep*xi[tauIndex],
                           x_i+0.5*spacestep*xi[xiIndex],
                           vars,
                           #E_vars,
                           AppParams);

    return tmp



    #return array([s[0](t_i+0.5*timestep*xi[tauIndex],
    #                   x_i+0.5*spacestep*xi[xiIndex],
    #                   AppParams),
    #              s[1](t_i+0.5*timestep*xi[tauIndex],
    #                   x_i+0.5*spacestep*xi[xiIndex],
    #                   AppParams),
    #              s[2](t_i+0.5*timestep*xi[tauIndex],
    #                   x_i+0.5*spacestep*xi[xiIndex],
    #                   AppParams),
    #              s[3](t_i+0.5*timestep*xi[tauIndex],
    #                   x_i+0.5*spacestep*xi[xiIndex],
    #                   AppParams),
    #              s[4](t_i+0.5*timestep*xi[tauIndex],
    #                   x_i+0.5*spacestep*xi[xiIndex],
    #                   AppParams)]);
#-------------------------------------------------------------------------------
