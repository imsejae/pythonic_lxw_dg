#Find Legandre coefficients of a product

def LegProduct(A,B,numGridCells,numOrder):
    from numpy import zeros,sqrt

    C = zeros((numGridCells,numOrder))
    if numOrder == 1:
        C[:,0] = A[:,0]*B[:,0];

    if numOrder == 2:
        C[:,0] = A[:,0]*B[:,0] + A[:,1]*B[:,1];
        C[:,1] = A[:,0]*B[:,1] + A[:,1]*B[:,0];

    if numOrder == 3:
        C[:,0] = A[:,0]*B[:,0] + A[:,1]*B[:,1] + A[:,2]*B[:,2];
        C[:,1] = A[:,0]*B[:,1] + 2.0*A[:,2]*B[:,1]/sqrt(5.0) \
                + A[:,1]*(B[:,0]+2.0*B[:,2]/sqrt(5)) ;
        C[:,2] = 2.0*A[:,1]*B[:,1]/sqrt(5.0) + A[:,0]*B[:,2] \
                + A[:,2]*(B[:,0]+2.0*sqrt(5.0)*B[:,2]/7.0) ;

    if numOrder == 4:
        C[:,0] = A[:,0]*B[:,0] + A[:,1]*B[:,1] + A[:,2]*B[:,2] + A[:,3]*B[:,3];
        C[:,1] = A[:,0]*B[:,1] + 2.0*A[:,2]*B[:,1]/sqrt(5.0) \
                + 3.0*sqrt(3.0/35.0)*A[:,3]*B[:,2] + A[:,1]*(B[:,0]+2.0*B[:,2]/sqrt(5)) \
                + 3.0*sqrt(3.0/35.0)*A[:,2]*B[:,3];
        C[:,2] = 2.0*A[:,1]*B[:,1]/sqrt(5.0) + 3.0*sqrt(3.0/35.0)*A[:,3]*B[:,1] \
                + A[:,0]*B[:,2] + A[:,2]*(B[:,0]+2.0*sqrt(5.0)*B[:,2]/7.0) \
                + 3.0*sqrt(3.0/35.0)*A[:,1]*B[:,3] + 4.0*A[:,3]*B[:,3]/(3.0*sqrt(5.0));
        C[:,3] = 3.0*sqrt(3.0/35.0)*A[:,2]*B[:,1] + 3.0*sqrt(3.0/35.0)*A[:,1]*B[:,2] \
                + A[:,3]*(B[:,1]+4.0*B[:,2]/(3.0*sqrt(5.0))) + 4.0*A[:,2]*B[:,3]/(3.0*sqrt(5.0));

    return C

################################################################################

def deriv(spacestep,F,numGridCells,numOrder):
    from numpy import sqrt,zeros,shape
    der = zeros((numGridCells,numOrder));

    if numOrder == 1:
        for i in range(0,numGridCells):
            im1 = (i>0)*(i-1)+(i==0)*(numGridCells-1)
            ip1 = (i<numGridCells-1)*(i+1)+(i==(numGridCells-1))*(0)
            der[i,0] = F[ip1,0]-F[im1,0]

    elif numOrder == 2:
        for i in range(0,numGridCells):
            im1 = (i>0)*(i-1)+(i==0)*(numGridCells-1)
            ip1 = (i<numGridCells-1)*(i+1)+(i==(numGridCells-1))*(0)
            der[i,0] = F[ip1,0]-F[im1,0]
            der[i,1] = F[ip1,1]-F[im1,1]

    elif numOrder == 3:
        for i in range(0,numGridCells):
            im1 = (i>0)*(i-1)+(i==0)*(numGridCells-1)
            ip1 = (i<numGridCells-1)*(i+1)+(i==(numGridCells-1))*(0)
            der[i,0] = F[ip1,0]-F[im1,0]-2*sqrt(5)*(F[ip1,2]-F[im1,2])
            der[i,1] = F[ip1,1]-F[im1,1]
            der[i,2] = F[ip1,2]-F[im1,2]

    elif numOrder == 4:
        for i in range(0,numGridCells):
            im1 = (i>0)*(i-1)+(i==0)*(numGridCells-1)
            ip1 = (i<numGridCells-1)*(i+1)+(i==(numGridCells-1))*(0)
            der[i,0] = F[ip1,0]-F[im1,0]-2*sqrt(5)*(F[ip1,2]-F[im1,2])
            der[i,1] = F[ip1,1]-F[im1,1] - (10/3)*sqrt(3)*sqrt(7)*(F[ip1,3]-F[im1,3])
            der[i,2] = F[ip1,2]-F[im1,2]
            der[i,3] = F[ip1,3]-F[im1,3]



    return((0.5/spacestep)*der)

################################################################################

def second_deriv(spacestep,F,numGridCells,numOrder):
    from numpy import sqrt,zeros
    sec_der = zeros((numGridCells,numOrder));

    if numOrder == 1:
        for i in range(0,numGridCells):
            im1 = (i>0)*(i-1)+(i==0)*(numGridCells-1)
            ip1 = (i<numGridCells-1)*(i+1)+(i==(numGridCells-1))*(0)
            sec_der[i,0] = F[ip1,0]-2*F[i,0]+F[im1,0]

    elif numOrder == 2:
        for i in range(0,numGridCells):
            im1 = (i>0)*(i-1)+(i==0)*(numGridCells-1)
            ip1 = (i<numGridCells-1)*(i+1)+(i==(numGridCells-1))*(0)
            sec_der[i,0] = F[ip1,0]-2*F[i,0]+F[im1,0]
            sec_der[i,1] = F[ip1,1]-2*F[i,1]+F[im1,1]

    elif numOrder == 3:
        for i in range(0,numGridCells):
            im1 = (i>0)*(i-1)+(i==0)*(numGridCells-1)
            ip1 = (i<numGridCells-1)*(i+1)+(i==(numGridCells-1))*(0)
            sec_der[i,0] = F[ip1,0]-2*F[i,0]+F[im1,0]-sqrt(5)*(F[ip1,2]-2*F[i,2]+F[im1,2])
            sec_der[i,1] = F[ip1,1]-2*F[i,1]+F[im1,1]
            sec_der[i,2] = F[ip1,2]-2*F[i,2]+F[im1,2]

    elif numOrder == 4:
        for i in range(0,numGridCells):
            im1 = (i>0)*(i-1)+(i==0)*(numGridCells-1)
            ip1 = (i<numGridCells-1)*(i+1)+(i==(numGridCells-1))*(0)
            sec_der[i,0] = F[ip1,0]-2*F[i,0]+F[im1,0]-sqrt(5)*(F[ip1,2]-2*F[i,2]+F[im1,2])
            sec_der[i,1] = F[ip1,1]-2*F[i,1]+F[im1,1]-(5/3)*sqrt(3)*sqrt(7)*(F[ip1,3]-2*F[i,3]+F[im1,3])
            sec_der[i,2] = F[ip1,2]-2*F[i,2]+F[im1,2]
            sec_der[i,3] = F[ip1,3]-2*F[i,3]+F[im1,3]


    return(1/spacestep**2*sec_der)

################################################################################

def deriv_electricField(numGridCells,E,Q,spacestep,numOrder,xlow,xhigh,t,AppParams):

    from numpy import zeros,dot,shape,sqrt,pi,cos,sin,linspace;
    import math;
    import sys; sys.path.insert(0,'../../lib');
    from basis_data import computeCoefficients;

    x = linspace(xlow+0.5*spacestep, xhigh-0.5*spacestep, numGridCells);

    dE = zeros((numGridCells,numOrder));
    ddE = zeros((numGridCells,numOrder));
    dddE = zeros((numGridCells,numOrder));

    d_energy = deriv(spacestep,Q[2,:,:],numGridCells,numOrder);
    #tmp1 = deriv(spacestep,Q[1,:,:]*E,numGridCells,numOrder);
    momtimesE = LegProduct(Q[1,:,:],E,numGridCells,numOrder)
    tmp1 = deriv(spacestep,momtimesE,numGridCells,numOrder);
    tmp2 = second_deriv(spacestep,Q[3,:,:],numGridCells,numOrder);
    tmp3 = deriv(spacestep,Q[1,:,:],numGridCells,numOrder);

    J0 = 0.25*sqrt(pi)

    C1 = zeros((numGridCells,numOrder));
    C2 = zeros((numGridCells,numOrder));
    C3 = zeros((numGridCells,numOrder));

    if numOrder == 1:
        for i in range(0,numGridCells):
            C1[i,0] = (sqrt(pi)*(2.0*spacestep + (-1.0 + 4.0*pi)*cos(2.0*pi*t - 2.0*x[i]) \
                    *sin(spacestep)))/(8.0*spacestep)

            C2[i,0] = (-(pi*sin(2.0*spacestep)*sin(4.0*pi*t - 4.0*x[i])) \
                + 2.0*sqrt(pi)*(3.0 + 4.0*sqrt(pi) - 16.0*pi**2)\
                *sin(spacestep)*sin(2.0*pi*t - 2.0*x[i]))/(32.0*spacestep)

            C3[i,0] = (-8.0*spacestep*pi + sqrt(pi)*(7.0 + 16.0*sqrt(pi) \
                    - 64.0*pi**3)*cos(2.0*pi*t - 2.0*x[i])*sin(spacestep) \
                    - 3*pi*cos(4.0*pi*t - 4.0*x[i])*sin(2.0*spacestep))/(32.0*spacestep)


    if numOrder == 2:
        for i in range(0,numGridCells):
            C1[i,0] = (sqrt(pi)*(2.0*spacestep + (-1.0 + 4.0*pi)*cos(2.0*pi*t - 2.0*x[i]) \
                    *sin(spacestep)))/(8.0*spacestep)
            C1[i,1] = -(sqrt(3.0*pi)*(-1.0 + 4.0*pi)*(spacestep*cos(spacestep) \
                    - sin(spacestep))*sin(2.0*pi*t - 2.0*x[i]))/(8.0*spacestep**2)

            C2[i,0] = (-(pi*sin(2.0*spacestep)*sin(4.0*pi*t - 4.0*x[i])) \
                    + 2.0*sqrt(pi)*(3.0 + 4.0*sqrt(pi) - 16.0*pi**2)\
                    *sin(spacestep)*sin(2.0*pi*t - 2.0*x[i]))/(32.0*spacestep)
            C2[i,1] = (sqrt(3.0)*(-2.0*spacestep*pi*cos(2.0*spacestep)*\
                    cos(4.0*pi*t - 4.0*x[i]) - 4.0*(-3.0*sqrt(pi) - 4.0*pi \
                    + 16.0*pi**2.5)*cos(2.0*pi*t - 2.0*x[i])*(spacestep*cos(spacestep) \
                    - sin(spacestep)) + pi*cos(4.0*pi*t - 4.0*x[i])*sin(2.0*spacestep)))/(64.0*spacestep**2)

            C3[i,0] = (-8.0*spacestep*pi + sqrt(pi)*(7.0 + 16.0*sqrt(pi) \
                    - 64.0*pi**3)*cos(2.0*pi*t - 2.0*x[i])*sin(spacestep) \
                    - 3*pi*cos(4.0*pi*t - 4.0*x[i])*sin(2.0*spacestep))/(32.0*spacestep)
            C3[i,1] = (sqrt(3.0*pi)*(3.0*sqrt(pi)*(2.0*spacestep*cos(2.0*spacestep) \
                    - sin(2.0*spacestep))*sin(4.0*pi*t - 4.0*x[i]) \
                    + 2.0*(-7.0 - 16.0*sqrt(pi) + 64.0*pi**3)*(spacestep*cos(spacestep) \
                    - sin(spacestep))*sin(2.0*pi*t - 2.0*x[i])))/(64.0*spacestep**2)


    if numOrder == 3:
        for i in range(0,numGridCells):
            C1[i,0] = (sqrt(pi)*(2.0*spacestep + (-1.0 + 4.0*pi)*cos(2.0*pi*t - 2.0*x[i]) \
                    *sin(spacestep)))/(8.0*spacestep)
            C1[i,1] = -(sqrt(3.0*pi)*(-1.0 + 4.0*pi)*(spacestep*cos(spacestep) \
                    - sin(spacestep))*sin(2.0*pi*t - 2.0*x[i]))/(8.0*spacestep**2)
            C1[i,2] = (sqrt(5.0*pi)*(-1.0 + 4.0*pi)*cos(2.0*pi*t \
                    - 2.0*x[i])*(3.0*spacestep*cos(spacestep) + (-3.0 \
                    + spacestep**2)*sin(spacestep)))/(8.0*spacestep**3)

            C2[i,0] = (-(pi*sin(2.0*spacestep)*sin(4.0*pi*t - 4.0*x[i])) \
                    + 2.0*sqrt(pi)*(3.0 + 4.0*sqrt(pi) - 16.0*pi**2)\
                    *sin(spacestep)*sin(2.0*pi*t - 2.0*x[i]))/(32.0*spacestep)
            C2[i,1] = (sqrt(3.0)*(-2.0*spacestep*pi*cos(2.0*spacestep)*\
                    cos(4.0*pi*t - 4.0*x[i]) - 4.0*(-3.0*sqrt(pi) - 4.0*pi \
                    + 16.0*pi**2.5)*cos(2.0*pi*t - 2.0*x[i])*(spacestep*cos(spacestep) \
                    - sin(spacestep)) + pi*cos(4.0*pi*t - 4.0*x[i])*sin(2.0*spacestep)))/(64.0*spacestep**2)
            C2[i,2] = (sqrt(5.0*pi)*(sqrt(pi)*(-6.0*spacestep*cos(2.0*spacestep) \
                    + (3.0 - 4.0*spacestep**2)*sin(2.0*spacestep))\
                    *sin(4.0*pi*t - 4.0*x[i]) - 8.0*(-3.0 - 4.0*sqrt(pi) \
                    + 16.0*pi**2)*(3.0*spacestep*cos(spacestep) \
                    + (-3.0 + spacestep**2)*sin(spacestep))*sin(2.0*pi*t - 2.0*x[i])))/(128.0*spacestep**3)

            C3[i,0] = (-8.0*spacestep*pi + sqrt(pi)*(7.0 + 16.0*sqrt(pi) \
                    - 64.0*pi**3)*cos(2.0*pi*t - 2.0*x[i])*sin(spacestep) \
                    - 3*pi*cos(4.0*pi*t - 4.0*x[i])*sin(2.0*spacestep))/(32.0*spacestep)
            C3[i,1] = (sqrt(3.0*pi)*(3.0*sqrt(pi)*(2.0*spacestep*cos(2.0*spacestep) \
                    - sin(2.0*spacestep))*sin(4.0*pi*t - 4.0*x[i]) \
                    + 2.0*(-7.0 - 16.0*sqrt(pi) + 64.0*pi**3)*(spacestep*cos(spacestep) \
                    - sin(spacestep))*sin(2.0*pi*t - 2.0*x[i])))/(64.0*spacestep**2)
            C3[i,2] = (sqrt(5.0)*(-18.0*spacestep*pi*cos(2.0*spacestep)*cos(4.0*pi*t - 4.0*x[i]) \
                    - 4.0*(-7.0*sqrt(pi) - 16.0*pi + 64.0*pi**3.5)*cos(2.0*pi*t - 2.0*x[i])\
                    *(3.0*spacestep*cos(spacestep) + (-3.0 + spacestep**2)*sin(spacestep)) \
                    + 3.0*(3.0 - 4.0*spacestep**2)*pi*cos(4.0*pi*t - 4.0*x[i])\
                    *sin(2.0*spacestep)))/(128.0*spacestep**3)



    if numOrder == 4:
        for i in range(0,numGridCells):
            C1[i,0] = (sqrt(pi)*(2.0*spacestep + (-1.0 + 4.0*pi)*cos(2.0*pi*t - 2.0*x[i]) \
                        *sin(spacestep)))/(8.0*spacestep)
            C1[i,1] = -(sqrt(3.0*pi)*(-1.0 + 4.0*pi)*(spacestep*cos(spacestep) \
                        - sin(spacestep))*sin(2.0*pi*t - 2.0*x[i]))/(8.0*spacestep**2)
            C1[i,2] = (sqrt(5.0*pi)*(-1.0 + 4.0*pi)*cos(2.0*pi*t \
                        - 2.0*x[i])*(3.0*spacestep*cos(spacestep) + (-3.0 \
                        + spacestep**2)*sin(spacestep)))/(8.0*spacestep**3)
            C1[i,3] = -(sqrt(7.0*pi)*(-1.0 + 4.0*pi)*(spacestep*(-15.0 \
                        + spacestep**2)*cos(spacestep) + 3.0*(5.0 - 2.0*spacestep**2) \
                        *sin(spacestep))*sin(2.0*pi*t - 2.0*x[i]))/(8.0*spacestep**4)

            C2[i,0] = (-(pi*sin(2.0*spacestep)*sin(4.0*pi*t - 4.0*x[i])) \
                        + 2.0*sqrt(pi)*(3.0 + 4.0*sqrt(pi) - 16.0*pi**2)\
                        *sin(spacestep)*sin(2.0*pi*t - 2.0*x[i]))/(32.0*spacestep)
            C2[i,1] = (sqrt(3.0)*(-2.0*spacestep*pi*cos(2.0*spacestep)*\
                        cos(4.0*pi*t - 4.0*x[i]) - 4.0*(-3.0*sqrt(pi) - 4.0*pi \
                        + 16.0*pi**2.5)*cos(2.0*pi*t - 2.0*x[i])*(spacestep*cos(spacestep) \
                        - sin(spacestep)) + pi*cos(4.0*pi*t - 4.0*x[i])*sin(2.0*spacestep)))/(64.0*spacestep**2)
            C2[i,2] = (sqrt(5.0*pi)*(sqrt(pi)*(-6.0*spacestep*cos(2.0*spacestep) \
                        + (3.0 - 4.0*spacestep**2)*sin(2.0*spacestep))\
                        *sin(4.0*pi*t - 4.0*x[i]) - 8.0*(-3.0 - 4.0*sqrt(pi) \
                        + 16.0*pi**2)*(3.0*spacestep*cos(spacestep) \
                        + (-3.0 + spacestep**2)*sin(spacestep))*sin(2.0*pi*t - 2.0*x[i])))/(128.0*spacestep**3)
            C2[i,3] = (sqrt(7.0)*(2.0*spacestep*(15.0 - 4.0*spacestep**2)*pi\
                        *cos(2.0*spacestep)*cos(4.0*pi*t - 4.0*x[i]) \
                        - 16.0*(-3.0*sqrt(pi) - 4.0*pi + 16.0*pi**2.5)\
                        *cos(2.0*pi*t - 2.0*x[i])*(spacestep*(-15.0 + spacestep**2)\
                        *cos(spacestep) + 3.0*(5.0 - 2.0*spacestep**2)*sin(spacestep)) \
                        + 3.0*(-5.0 + 8.0*spacestep**2)*pi*cos(4.0*pi*t - 4.0*x[i])\
                        *sin(2.0*spacestep)))/(256.0*spacestep**4)

            C3[i,0] = (-8.0*spacestep*pi + sqrt(pi)*(7.0 + 16.0*sqrt(pi) \
                        - 64.0*pi**3)*cos(2.0*pi*t - 2.0*x[i])*sin(spacestep) \
                        - 3*pi*cos(4.0*pi*t - 4.0*x[i])*sin(2.0*spacestep))/(32.0*spacestep)
            C3[i,1] = (sqrt(3.0*pi)*(3.0*sqrt(pi)*(2.0*spacestep*cos(2.0*spacestep) \
                        - sin(2.0*spacestep))*sin(4.0*pi*t - 4.0*x[i]) \
                        + 2.0*(-7.0 - 16.0*sqrt(pi) + 64.0*pi**3)*(spacestep*cos(spacestep) \
                        - sin(spacestep))*sin(2.0*pi*t - 2.0*x[i])))/(64.0*spacestep**2)
            C3[i,2] = (sqrt(5.0)*(-18.0*spacestep*pi*cos(2.0*spacestep)*cos(4.0*pi*t - 4.0*x[i]) \
                        - 4.0*(-7.0*sqrt(pi) - 16.0*pi + 64.0*pi**3.5)*cos(2.0*pi*t - 2.0*x[i])\
                        *(3.0*spacestep*cos(spacestep) + (-3.0 + spacestep**2)*sin(spacestep)) \
                        + 3.0*(3.0 - 4.0*spacestep**2)*pi*cos(4.0*pi*t - 4.0*x[i])\
                        *sin(2.0*spacestep)))/(128.0*spacestep**3)
            C3[i,3] = (sqrt(7.0*pi)*(3.0*sqrt(pi)*(2.0*spacestep*(-15.0 + 4.0*spacestep**2)\
                        *cos(2.0*spacestep) + 3.0*(5.0 - 8.0*spacestep**2)*sin(2.0*spacestep))\
                        *sin(4.0*pi*t - 4.0*x[i]) + 8.0*(-7.0 - 16.0*sqrt(pi) \
                        + 64.0*pi**3)*(spacestep*(-15.0 + spacestep**2)*cos(spacestep) \
                        + 3.0*(5.0 - 2.0*spacestep**2)*sin(spacestep))*sin(2.0*pi*t - 2.0*x[i])))\
                        /(256.0*spacestep**4)

    rhotimesE = LegProduct(Q[0,:,:],E,numGridCells,numOrder);
    Etimestmp3 = LegProduct(E,tmp3,numGridCells,numOrder);
    rhotimesmom = LegProduct(Q[0,:,:],Q[1,:,:],numGridCells,numOrder);
    #dE[:,0] += J0;
    dE = -Q[1,:,:] + C1;
    ddE = d_energy - rhotimesE + C2
    dddE = 2*tmp1 - tmp2 + Etimestmp3 + rhotimesmom + C3

    return(dE,ddE,dddE)
