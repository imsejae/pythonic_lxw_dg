def mynormal(x,xcenter,std):
    from numpy import sqrt,exp,pi
    return (1.0/sqrt(2.0*pi*std))*exp(-((x-xcenter)**2)/(2.0*std));

if __name__ == "__main__":

    import numpy as np
    import matplotlib
    import matplotlib.pylab as plt
    matplotlib.rcParams.update({'font.size': 16, 'font.family': 'serif'})


    x = np.linspace(-78.0,78.0,1001);

    y = 1.0 + 4.7*mynormal(x,0.0,122.7);

    plt.figure(1)
    plt.clf()
    plt.plot(x,y,'b-')
    plt.show()
