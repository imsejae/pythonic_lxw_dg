#Sovle the Poisson Equation

###################################################

def poisson_solve(numGridCells,order,xlow,xhigh,P,t,timestep=0.0):

    from numpy import zeros,array,exp,dot,diag,transpose,shape,pi,linspace,sqrt,sin,cos;
    from numpy.linalg import inv,norm;
    import math;
    import sys; sys.path.insert(0,'../../lib');
    from basis_data import computeCoefficients;

    spacestep = (xhigh-xlow)/numGridCells;
    #x = linspace(xlow, xhigh, numGridCells);
    x = zeros(numGridCells);#linspace(xlow+0.5*spacestep, xhigh-0.5*spacestep, numGridCells);
    for i in range(0,numGridCells):
        x[i] = xlow+(i+0.5)*spacestep;

    #print("P shape is", shape(P))
    #print(P[1])

    tmp1 = 0;
    tmp2 = 0;
    for i in range(0,numGridCells):
        tmp1 += (x[i])*P[i,0]; ###(x[i]-xhigh)*P[i,0] for Dirichlet boundary conditions
        ###x[i]*P[i,0] for Periodic boundary conditions
    if order > 1:
        for i in range(0,numGridCells):
            tmp2 += P[i,1];
    gamma = -(spacestep/(xlow-xhigh)*tmp1+(spacestep**2/(2*sqrt(3)*(xlow-xhigh)))*tmp2);
    #print("gamma is", gamma)
    beta = 0;

    sq3=sqrt(3.0);
    sq5=sqrt(5.0);
    sq7=sqrt(7.0);
    S = array([[0.0,0.0,0.0,0.0,0.0],
               [2.0*sq3,0.0,0.0,0.0,0.0],
               [0.0,2.0*sq3*sq5,0.0,0.0,0.0],
               [2.0*sq7,0.0,2.0*sq5*sq7,0.0,0.0],
               [0.0,6.0*sq3,0.0,6.0*sq7,0.0]]);

    A = zeros((order,order));
    B = zeros((order,order));
    #C = zeros((order,order));
    #D = zeros((order,order));
    for ell in range(1,order+1):
        for k in range(1,order+1):
            A[ell-1,k-1] = sqrt(2*k-1)*sqrt(2*ell-1)-S[ell-1,k-1];
            B[ell-1,k-1] = (-1)**ell*sqrt(2*k-1)*sqrt(2*ell-1);
            #C[ell-1,k-1] = (-1)**(k+ell)*sqrt(2*k-1)*sqrt(2*ell-1)+S[ell-1,k-1];
            #D[ell-1,k-1] = (-1)**k*sqrt(2*k-1)*sqrt(2*ell-1);

    Ainv = inv(A);
    #Cinv = inv(C);

    #print("Ainv is", Ainv);
    #print("B is", B);
    #print("spacestep is", spacestep);
    #print("gamma is", gamma);

    E = zeros((numGridCells,order));
    #Phi = zeros((numGridCells,order));
    rhs1 = zeros(order);
    #rhs2 = zeros(order);

    for ell in range(0,order):
        rhs1[ell] = P[0,ell]-(-1)**(ell+1)*sqrt(2*(ell+1)-1)*gamma*(1/spacestep);

    E[0,:] = spacestep*dot(Ainv,rhs1);
    #print("E1 is", E[0,:])
    #print("shape of E1", shape(E[0,:]))

    for k in range(1,numGridCells):
        E[k,:] = spacestep*dot(Ainv,(P[k,:]-(1/spacestep)*dot(B,E[k-1,:])));

    #for k in range(0,numGridCells):
    #    if k == 0:
    #        print("xlow is", xlow);
    #        print("spacestep is", spacestep);
    #        print("x[k] is", x[k])
    #        print("t+0.5*timestep is", t+0.5*timestep)
    #    E[k,0] = -0.25*sqrt(pi)*sin(2*x[k] - 2*pi*(t+(0.5*timestep)));
    #    E[k,0] = (sqrt(pi)*sin(spacestep)*sin(2.0*pi*t - 2.0*x[k]))/(4.0*spacestep);
    #    E[k,1] = (sqrt(3.0*pi)*cos(2.0*pi*t - 2.0*x[k])*(spacestep*cos(spacestep) \
    #            - sin(spacestep)))/(4.0*spacestep**2);
    #    E[k,2] = (sqrt(5.0*pi)*(3.0*spacestep*cos(spacestep) \
    #            + (-3.0 + spacestep**2)*sin(spacestep))*sin(2.0*pi*t - 2.0*x[k]))/(2.0*spacestep**3);
        #E[k,3] = (sqrt(7*pi)*cos(2.0*pi*t - 2.0*x[k])*(spacestep*(-15.0 + spacestep**2)*cos(spacestep) \
        #        + 3.0*(5.0 - 2.0*spacestep**2)*sin(spacestep)))/(2.0*spacestep**4);
    #print("E2 is", E[1,:])

    #for ell in range(0,order):
    #    rhs2[ell] = E[numGridCells-1,ell]+sqrt(2*(ell+1)-1)*beta*spacestep**(-1)
    #Phi[numGridCells-1,:] = spacestep*dot(Cinv,rhs2);

    #for k in reversed(range(0,numGridCells-1)):
        #Phi[k,:] = spacestep*dot(Cinv,(E[k,:]-(1/spacestep)*dot(D,Phi[k+1,:])));

    return(E) #Can add Phi
